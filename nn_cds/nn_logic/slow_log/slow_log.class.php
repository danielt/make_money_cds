<?php
/**
 * 全局失败日志管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
$nns_project_dir = dirname(dirname(dirname(__FILE__)));
require_once $nns_project_dir . DIRECTORY_SEPARATOR ."nn_cms_db".DIRECTORY_SEPARATOR. "nns_common" . DIRECTORY_SEPARATOR . "nns_db_excel_class.php";
class nl_slow_log extends nl_public
{
	static $base_table='mysql.slow_log';
	static $arr_filed = array(
			'start_time',
			'user_host',
			'query_time',
			'lock_time',
			'rows_sent',
			'rows_examined',
			'db',
			'last_insert_id',
			'insert_id',
			'server_id',
			'sql_text',
	);
	
	/**
	 * 查询全局错误日志列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query($dc,$params,$page_info=null)
	{
	    if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_val == '--')
				{
					self::$str_where.=" ( isnull({$where_key}) or {$where_key} = '' ) and ";
				}
				else if($where_key == 'create_begin_time')
				{
					self::$str_where.=" start_time >='$where_val' and ";
				}
				else if($where_key == 'create_end_time')
				{
					self::$str_where.=" start_time <='{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} = '{$where_val}' and ";
				}
			}	
		}
		if(isset($params['other']) && !empty($params['other']))
		{
		    foreach ($params['other'] as $other_val)
		    {
		        if(strlen($other_val) <1)
		        {
		            continue;
		        }
		        self::$str_where.=" {$other_val} and ";
		    }
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		$str_order = (isset($params['order']) && !empty($params['order']) && is_array($params['order'])) ? " order by ".implode(",", $params['order']) : " order by start_time desc ";
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select TIME_TO_SEC(query_time) as s_query_time,TIME_TO_SEC(lock_time) as s_lock_time," . self::$base_table . ".* from " . self::$base_table . " " . self::$str_where . " {$str_order} {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	/**
	 * 依据时间删除日志
	 * @param unknown $dc
	 * @param unknown $date
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	public static function del_date($dc,$date=null)
	{
	    $sql = "truncate " . self::$base_table . " ;";
// 	    $sql="delete from " . self::$base_table . " where start_time <'{$date}' ";
	    $result = nl_execute_by_db($sql, $dc->db());
	    return $result ? self::return_data(0,"ok".$sql) : self::return_data(1,"查询数据失败".$sql);
	}
	
	
	/**
	 * 查询数据库列表
	 * @param unknown $dc
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	public static function query_exsist_database($dc)
	{
	    $result = nl_query_by_db("show DATABASES", $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败show DATABASES");
	    }
	    $last_data = null;
	    if(is_array($result))
	    {
	        foreach ($result as $key=>$val)
	        {
	            if($val['Database'] == 'information_schema' || $val['Database']  == 'performance_schema')
	            {
	               continue;
	            }
	            $last_data[] = $val['Database'];
	        }
	    }
	    unset($result);
	    $last_data = (is_array($last_data) && !empty($last_data)) ? $last_data : true;
	    return self::return_data(0,"查询数据成功",$last_data);
	}
	
	public static function show_state($dc)
	{
	    $sql="show variables like '%slow%'";
	    $result = nl_query_by_db($sql,  $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功",$result);
	}
	
	public static function opration_log($dc,$op_flag)
	{
	    $sql="SET global slow_query_log ={$op_flag}";
	    $result = nl_execute_by_db($sql,  $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"执行数据失败".$sql);
	    }
	    return self::return_data(0,"执行数据成功",$result);
	}
}
