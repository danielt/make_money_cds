<?php
/*
 * Created on 2012-11-19
 * @author yxt
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once "topic_item_image.class.php";
include_once "topic_item.class.php";
/**
 * 、信息内容组合业务
 */
class nl_topic_item_bo{
	/**
	 *从DC模块获取信息包内容列表
	 * @param dc DC模块
	 * @param string 信息ID
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_AUTO
	 * @return *
	 * 			Array 单条信息内容结果
				ARRAY(
					'info'=>ARRAY 信息内容数组
					'images'=>ARRAY 信息相册图片数组
				)
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
  static public function get_topic_item_info_with_image($dc,$id,$policy){
	switch($policy){
		case NL_DC_DB: $data=self::_get_topic_item_info_with_image_db($dc->db(),$id); break;
		default : $data=self::_get_topic_item_info_with_image_cache($dc,$id,$policy);
	}
	return $data;
  }
 static private function _get_topic_item_info_with_image_db($db,$id){
	$sql="select item.*,images.* from nns_topic_item as item,nns_topic_item_images as images where item.nns_id=images.nns_topic_item_id and tiem.nns_id='{$id}'";
	$data=nl_query_by_db($sql,$db);
	return $data;
 }
 static private function _get_topic_item_info_with_image_cache($dc,$id,$policy){
	$info=nl_topic_item::_get_cache_by_ids ($dc,$ids=array($id),null,$policy);
	$images=nl_topic_item_image::_get_imgcache_by_item_id ($dc,$id,null,null,null,NL_ORDER_NUM_DESC,NL_DC_AUTO);
	return array('info'=>$info,'images'=>$images);
 }
}
?>