<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
 <HEAD>
  <TITLE> 论坛前台接口测试页面 </TITLE>
  <meta http-equiv='Content-Type' content='text/html; charset=utf-8' />
  <META NAME="Generator" CONTENT="EditPlus">
  <META NAME="Author" CONTENT="">
  <META NAME="Keywords" CONTENT="">
  <META NAME="Description" CONTENT="">
  <HEAD>
  <BODY>
  <table border=1>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>获取栏目下信息包列表</td>
      <td>
      	<table>
      	<tr><td>信息包id：</td><td><input type="text" name="topic_id"/></td></tr>
      	<tr><td>信息包栏目id：</td><td><input type="text" name="category_id" /></td></tr>
      	<tr><td>epg输出标示:</td><td>
      		<select  name="tag">
      			<option value="26">26</option>
      			<option value="27">27</option>
      			<option value="28">28</option>
      			<option value="29">29</option>
      			<option value="30">30</option>
      			<option value="31">31</option>
      		</select></td></tr>
      	<tr><td>起始位置</td><td><input type="text" name="since" /></td></tr>
      	<tr><td>记录条数:</td><td><input type="text" name="num" /></td></tr>
      	<tr><td>排序规则：</td><td>
			<select name="order">
      			<option value="NL_ORDER_NUM_ASC">NL_ORDER_NUM_ASC</option>
      			<option value="NL_ORDER_NUM_DESC">NL_ORDER_NUM_DESC</option>
      			<option value="NL_ORDER_TIME_ASC">NL_ORDER_TIME_ASC</option>
      			<option value="NL_ORDER_TIME_DESC">NL_ORDER_TIME_DESC</option>
      			<option value="NL_ORDER_CLICK_ASC">NL_ORDER_CLICK_ASC</option>
      			<option value="NL_ORDER_CLICK_ASC">NL_ORDER_CLICK_ASC</option>
      		</select></td></tr>
      <tr><td>读取策略：</td><td>
      		<select name="policy">
      			<option value="NL_DC_AUTO">NL_DC_AUTO</option>
      			<option value="NL_DC_DB">NL_DC_DB</option>
      			<option value="NL_DC_CACHE">NL_DC_CACHE</option>
      		</select></td></tr>
      		</table>
      </td><td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list_by_child" />
    <tr>
      <td>获取栏目及子栏目内容列表</td>
      <td>
      <table>
      	<tr><td>信息包id：</td><td><input type="text" name="topic_id"/></td></tr>
      	<tr><td>信息包栏目id：</td><td><input type="text" name="category_id" value=1000 /></td></tr>
      	<tr><td>epg输出标示:</td><td>
      		<select  name="tag">
      			<option value="26">26</option>
      			<option value="27">27</option>
      			<option value="28">28</option>
      			<option value="29">29</option>
      			<option value="30">30</option>
      			<option value="31">31</option>
      		</select></td></tr>
      	<tr><td>起始位置</td><td><input type="text" name="since" /></td></tr>
      	<tr><td>记录条数:</td><td><input type="text" name="num" /></td></tr>
      <tr><td>排序规则：</td><td>
			<select name="order">
      			<option value="NL_ORDER_NUM_ASC">NL_ORDER_NUM_ASC</option>
      			<option value="NL_ORDER_NUM_DESC">NL_ORDER_NUM_DESC</option>
      			<option value="NL_ORDER_TIME_ASC">NL_ORDER_TIME_ASC</option>
      			<option value="NL_ORDER_TIME_DESC">NL_ORDER_TIME_DESC</option>
      			<option value="NL_ORDER_CLICK_ASC">NL_ORDER_CLICK_ASC</option>
      			<option value="NL_ORDER_CLICK_ASC">NL_ORDER_CLICK_ASC</option>
      		</select></td></tr>
      	<tr><td>读取策略：</td><td>
      		<select name="policy">
      			<option value="NL_DC_AUTO">NL_DC_AUTO</option>
      			<option value="NL_DC_DB">NL_DC_DB</option>
      			<option value="NL_DC_CACHE">NL_DC_CACHE</option>
      		</select></td></tr>
      		</table>
      </td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>查询信息内容列表</td>
      <td><table>
      	<tr><td>信息包id：</td><td><input type="text" name="topic_id" value=a#b|c /></td></tr>
      	<tr><td>信息包栏目id：</td><td><input type="text" name="category_id" value=a#b|c /></td></tr>
      	<tr><td>epg输出标示:</td><td>
      		<select  name="tag">
      			<option value="26">26</option>
      			<option value="27">27</option>
      			<option value="28">28</option>
      			<option value="29">29</option>
      			<option value="30">30</option>
      			<option value="31">31</option>
      		</select></td></tr>
      	<tr><td>标题:</td><td><input type="text" name="title" /></td></tr>
      	<tr><td>副标题：</td><td><input type="text" name="subtitle" /></td></tr>
      	<tr><td>关键字:</td><td><input type="text" name="keyword" value="a#b" /></td></tr>
      	<tr><td>信息摘要:</td><td><input type="text" name="summary" /></td></tr>
      	<tr><td>是否外链</td><td><input type="radio" name="link_type" value=1 />是<input type="radio" name="link_type" value=0 />否</td></tr>
      	<tr><td>信息来源：</td><td><input type="text"  name="source" /></td></tr>
      	<tr><td>信息内容：</td><td><input type="text" name="content" /></td></tr>
      <tr><td>起始位置</td><td><input type="text" name="since" /></td></tr>
      	<tr><td>记录条数:</td><td><input type="text" name="num" /></td></tr>
      	<tr><td>排序规则：</td><td>
			<select name="order">
      			<option value="NL_ORDER_NUM_ASC">NL_ORDER_NUM_ASC</option>
      			<option value="NL_ORDER_NUM_DESC">NL_ORDER_NUM_DESC</option>
      			<option value="NL_ORDER_TIME_ASC">NL_ORDER_TIME_ASC</option>
      			<option value="NL_ORDER_TIME_DESC">NL_ORDER_TIME_DESC</option>
      			<option value="NL_ORDER_CLICK_ASC">NL_ORDER_CLICK_ASC</option>
      			<option value="NL_ORDER_CLICK_ASC">NL_ORDER_CLICK_ASC</option>
      		</select></td></tr>
      	<tr><td>读取策略：</td><td>
      		<select name="policy">
      			<option value="NL_DC_AUTO">NL_DC_AUTO</option>
      			<option value="NL_DC_DB">NL_DC_DB</option>
      			<option value="NL_DC_CACHE">NL_DC_CACHE</option>
      		</select></td></tr>
      		</table>
      </td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>获取信息内容列表数量</td>
      <td>
			<table>
				<tr><td>信息包id</td><td><input type="text" name="topic_id" value="可以多个"/></td></tr>
				<tr><td>信息栏目id</td><td><input type="text" name="category_id" value="可以多个"/></td></tr>
				<tr><td>信息标题</td><td><input type="text" name="title" value=""/></td></tr>
				<tr><td>信息副标题</td><td><input type="text" name="subtitle" value=""/></td></tr>
				<tr><td>关键字</td><td><input type="text" name="keyword" value="#and |or"/></td></tr>
				<tr><td>epg标识</td><td><input type="text" name="tag" value="#and |or"/></td></tr>
				<tr><td>信息摘要</td><td><input type="text" name="summary" value=""/></td></tr>
				<tr><td>信息是否外链</td><td><input type="radio" name="link_type" value=1 />是<input type="radio" name="link_type" value=0 />否</td></tr>
				<tr><td>信息来源</td><td><input type="text" name="source" value="" /></td></tr>
				<tr><td>信息内容</td><td><input type="text" name="content" value="" /></td></tr>
			</table>
	 </td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  <form action="topic_text.submit.php" method="post" target="_blank">
    <input type="hidden" name="method" value="epg_get_topic_item_list" />
    <tr>
      <td>t10</td>
      <td>t11</td>
      <td><input type="submit" value="提交" /></td>
    </tr>
  </form>
  </table>
  </BODY>
  </HTML>