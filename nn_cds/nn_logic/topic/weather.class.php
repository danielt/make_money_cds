<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_weather {
	/**
	 * 根据标题获取信息内容
	 */
	static function get_weather_by_city($dc, $title, $policy = NL_DC_AUTO){
		$result = "";
		switch ($policy) {
			case NL_DC_AUTO :
				$result = self :: _get_weather_by_city_by_db($dc->db(),$title,  $policy);
				break;
			case NL_DC_DB :
				$result = self :: _get_weather_by_city_by_db($dc->db(), $title, $policy);
				break;
			case NL_DC_CACHE :

				break;
		}
		return $result;
	}
	static private function _get_weather_by_city_by_db($db,$title, $policy)
	{
		$sql = "select nns_content from nns_topic_item where nns_subtitle='{$title}'";

		return nl_db_get_one($sql,$db);
	}
 /**
  * 解析天气xml数据
  *
 * @return array(
 * 'city'=>,
 *今天
 * 'today'=>array(
 *     'condition'=>,
 *     'temp_low'=>,
 *     'temp_height'=>,
 * ),
 *明天
 * 'tomorrow'=>array(
 *     'condition'=>,
 *     'temp_low'=>,
 *     'temp_height'=>,
 * ),
 *后天
 * 'after_tomorrow'=>array(
 *     'condition'=>,
 *     'temp_low'=>,
 *     'temp_height'=>,
 * )
 *)
  */
 static public function parse_weather_data($topic_content){
//echo $topic_content;



 	//$topic_content = htmlspecialchars_decode(htmlspecialchars_decode($topic_content));
    //解析天气xml数据
    //$weather_data = array();

	$dom = new DOMDocument('1.0', 'utf-8');
	//echo $topic_content;
	//$topic_content = '<?xml version="1.0" encoding="utf-8" ?//> <weather> <position> <name>成都</name> <latitude></latitude> <longitude></longitude> <current_time>2012-12-14</current_time> <current_week>星期五</current_week> </position> <today><temp_low>6</temp_low><temp_high>8</temp_high><condition>小雨转小到中雨</condition><humidity></humidity></today> <tomorrow><temp_low>6</temp_low><temp_high>9</temp_high><condition>小雨</condition><humidity></humidity></tomorrow> <the_day_after_tomorrow><temp_low>5</temp_low><temp_high>9</temp_high><condition>阴</condition><humidity></humidity></the_day_after_tomorrow> </weather>';

	$dom->loadXML($topic_content);
	$xpath = new DOMXPath($dom);

    //var_dump($topic_content);exit;
	$city = isset($topic_content['city'])?$topic_content['city']:'';
    $today_temp_low  = $xpath->query('/weather/today/temp_low')->item(0)->nodeValue;
    $today_temp_high = $xpath->query('/weather/today/temp_high')->item(0)->nodeValue;
    $today_condition = $xpath->query('/weather/today/condition')->item(0)->nodeValue;
    $today_wind = $xpath->query('/weather/today/wind')->item(0)->nodeValue;
    $today_index_cy = $xpath->query('/weather/today/index_cy')->item(0)->nodeValue;
    $today_index_zwx = $xpath->query('/weather/today/index_zwx')->item(0)->nodeValue;
    $today_index_wr = $xpath->query('/weather/today/index_wr')->item(0)->nodeValue;

    $tomorrow_temp_low  = $xpath->query('/weather/tomorrow/temp_low')->item(0)->nodeValue;
    $tomorrow_temp_high = $xpath->query('/weather/tomorrow/temp_high')->item(0)->nodeValue;
    $tomorrow_condition = $xpath->query('/weather/tomorrow/condition')->item(0)->nodeValue;
    $tomorrow_wind = $xpath->query('/weather/tomorrow/wind')->item(0)->nodeValue;
    $tomorrow_index_cy = $xpath->query('/weather/tomorrow/index_cy')->item(0)->nodeValue;
    $tomorrow_index_zwx = $xpath->query('/weather/tomorrow/index_zwx')->item(0)->nodeValue;
    $tomorrow_index_wr = $xpath->query('/weather/tomorrow/index_wr')->item(0)->nodeValue;

    $after_tomorrow_temp_low = $xpath->query('/weather/the_day_after_tomorrow/temp_low')->item(0)->nodeValue;
    $after_tomorrow_temp_high = $xpath->query('/weather/the_day_after_tomorrow/temp_high')->item(0)->nodeValue;
    $after_tomorrow_condition = $xpath->query('/weather/the_day_after_tomorrow/condition')->item(0)->nodeValue;
    $after_tomorrow_wind = $xpath->query('/weather/the_day_after_tomorrow/wind')->item(0)->nodeValue;
    $after_tomorrow_index_cy = $xpath->query('/weather/the_day_after_tomorrow/index_cy')->item(0)->nodeValue;
    $after_tomorrow_index_zwx = $xpath->query('/weather/the_day_after_tomorrow/index_zwx')->item(0)->nodeValue;
    $after_tomorrow_index_wr = $xpath->query('/weather/the_day_after_tomorrow/index_wr')->item(0)->nodeValue;

    $weather_data['city'] = $city;
    //今天
    $weatherinfo['weather1'] = $today_condition;
    $weatherinfo['temp1'] = $today_temp_low .'℃~'.$today_temp_high.'℃';
    $weatherinfo['wind1'] = $today_wind;
    $weatherinfo['index_cy1'] = $tomorrow_index_cy;
    $weatherinfo['index_zwx1'] = $today_index_zwx;
    $weatherinfo['index_wr1'] = $today_index_wr;
    //明天
    $weatherinfo['weather2'] = $tomorrow_condition;
    $weatherinfo['temp2'] = $tomorrow_temp_low .'℃~'.$tomorrow_temp_high.'℃';
    $weatherinfo['wind2'] = $tomorrow_wind;
    $weatherinfo['index_cy2'] = $today_index_cy;
    $weatherinfo['index_zwx2'] = $tomorrow_index_zwx;
    $weatherinfo['index_wr2'] = $tomorrow_index_wr;
    //后天
    $weatherinfo['weather3'] = $after_tomorrow_condition;
    $weatherinfo['temp3'] = $after_tomorrow_temp_low .'℃~'.$after_tomorrow_temp_high.'℃';
    $weatherinfo['wind3'] = $after_tomorrow_wind;
    $weatherinfo['index_cy3'] = $after_tomorrow_index_cy;
    $weatherinfo['index_zwx3'] = $after_tomorrow_index_zwx;
    $weatherinfo['index_wr3'] = $after_tomorrow_index_wr;

    for($i=1;;$i++){
    	if($i>4 || !isset($weatherinfo['weather'.$i])) break;
    	$weatherinfo['weather_image'.$i]  = self::get_weather_style($weatherinfo['weather'.$i]);
    	$weatherinfo['weather_en'.$i]  = self::get_weather_en($weatherinfo['weather'.$i]);
    }

    return $weatherinfo;
 }
 static private function get_weather_style($w_condition)
 {
 	$weather_style= array();
 	$w_list = array();
 	if(false !== strpos($w_condition,'转')){
 		//
 		$w_list = explode('转',$w_condition);
 	}else{
 		$w_list[] = $w_condition;
 	}
 	//foreach($w_list as $w){
 	for($i=0;$i<2;$i++){
 		if(!isset($w_list[$i])) break;
 		$w = $w_list[$i];
 		if(false !== strpos($w,'晴')){
            $weather_style[] = 'weather_qin';
 		}else if(false !== strpos($w,'阴')){
            $weather_style[] = 'weather_yin';
 		}else if(false !== strpos($w,'多云')){
            $weather_style[] = 'weather_duoyun';
 		}else if(false !== strpos($w,'小雨')){
            $weather_style[] = 'weather_xiaoyu';
 		}else if(false !== strpos($w,'中雨')){
            $weather_style[] = 'weather_zhongyu';
 		}else if(false !== strpos($w,'大雨')){
            $weather_style[] = 'weather_dayu';
 		}else if(false !== strpos($w,'雷阵雨')){
            $weather_style[] = 'weather_leizhenyu';
 		}else if(false !== strpos($w,'阵雨')){
             $weather_style[] = 'weather_zhenyu';
 		}else if(false !== strpos($w,'小雪')){
            $weather_style[] = 'weather_xiaoxue';
 		}else if(false !== strpos($w,'大雪')){
            $weather_style[] = 'weather_daxue';
 		}else if(false !== strpos($w,'雾')){
            $weather_style[] = 'weather_wu';
 		}else if(false !== strpos($w,'沙尘暴')){
            $weather_style[] = 'weather_shachenbao';
 		}else if(false !== strpos($w,'雨夹雪')){
            $weather_style[] = 'weather_yujiaxue';
 		}else if(false !== strpos($w,'雨')){
            $weather_style[] = 'weather_xiaoyu';
 		}else if(false !== strpos($w,'雪')){
            $weather_style[] = 'weather_xiaoxue';
 		}

 	}
 	return $weather_style;
 }
 static private function get_weather_en($w_condition)
 {
 	$weather_style= array();
 	$w_list = array();
 	if(false !== strpos($w_condition,'转')){
 		//
 		$w_list = explode('转',$w_condition);
 	}else{
 		$w_list[] = $w_condition;
 	}
 	//foreach($w_list as $w){
 	for($i=0;$i<2;$i++){
 		if(!isset($w_list[$i])) break;
 		$w = $w_list[$i];
 		if(false !== strpos($w,'晴')){
            $weather_style[] = 'Sunny';
 		}else if(false !== strpos($w,'阴')){
            $weather_style[] = 'Overcast';
 		}else if(false !== strpos($w,'多云')){
            $weather_style[] = 'Cloudy';
 		}else if(false !== strpos($w,'小雨')){
            $weather_style[] = 'Light rain';
 		}else if(false !== strpos($w,'中雨')){
            $weather_style[] = 'Moderate rain';
 		}else if(false !== strpos($w,'大雨')){
            $weather_style[] = ' Heavy rainfall';
 		}else if(false !== strpos($w,'雷阵雨')){
            $weather_style[] = 'Thunder shower';
 		}else if(false !== strpos($w,'阵雨')){
             $weather_style[] = 'Shower';
 		}else if(false !== strpos($w,'小雪')){
            $weather_style[] = 'Light Snow';
 		}else if(false !== strpos($w,'大雪')){
            $weather_style[] = 'Heavy Snow';
 		}else if(false !== strpos($w,'雾')){
            $weather_style[] = 'Fog';
 		}else if(false !== strpos($w,'沙尘暴')){
            $weather_style[] = 'Sandstorm';
 		}else if(false !== strpos($w,'雨夹雪')){
            $weather_style[] = 'Sleet';
 		}else if(false !== strpos($w,'Rain')){
            $weather_style[] = 'weather_xiaoyu';
 		}else if(false !== strpos($w,'雪')){
            $weather_style[] = 'Snow';
 		}

 	}
 	$weather_en = implode(' to ',$weather_style);
 	return $weather_en;
 }
}
?>