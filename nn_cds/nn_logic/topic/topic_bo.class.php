<?php

/*
 * Created on 2012-11-19
 * @author yxt
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';

/**
 * 信息包组合业务
 */
class nl_topic_bo {
	const CACHE_TIME_OUT = 300;
	/**
	 * 从dc根据用户ID获取隶属信息包列表
	 * @param dc DC模块
	 * @param string 用户ID 可为空。
	 * @param array	$fields:需要获取的信息包字段信息,如果不传入则全部返回，注意要传完整的字段名
	 * @return *
				ARRAY 查询列表
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功但无数据
	 */
	static function get_topic_list_by_user_id($dc, $user_id, $fields = array ()) {
		$db = $dc->db();
		if (empty ($user_id)) {
			include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'system/system.class.php';
			$carrier_info = nl_system :: get_carrier_id($db);
			$user_id = $carrier_info['0']['nns_id'];
		}
		$sql = "select nns_category_id from nns_user_collect where nns_user_id='{$user_id}'";
		$info = nl_query_by_db($sql, $db);
		if (empty ($fields)) {
			$field = "*";
		} else {
			$field = implode(',', $fields);
		}
		$sql = "select {$field} from nns_topic where nns_org_od='{$category_id}'";
		$topic_info = nl_query_by_db($sql, $db);
		return $topic_info;
	}
}
?>