<?php
/**
 * 全局失败日志管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
$nns_project_dir = dirname(dirname(dirname(__FILE__)));
require_once $nns_project_dir . DIRECTORY_SEPARATOR ."nn_cms_db".DIRECTORY_SEPARATOR. "nns_common" . DIRECTORY_SEPARATOR . "nns_db_excel_class.php";
class nl_global_error_log extends nl_public
{
	static $base_table='nns_global_error_log';
	static $arr_filed = array(
			'nns_id',
			'nns_name',
			'nns_desc',
			'nns_type',
			'nns_action',
			'nns_link_file',
			'nns_model',
			'nns_cp_id',
			'nns_sp_id',
			'nns_bind_id',
			'nns_message_id',
			'nns_create_time',
			'nns_video_import_id',
			'nns_index_import_id',
			'nns_media_import_id',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function add($dc=null,$params,$db=null)
	{
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$params['nns_create_time'] = date('Y-m-d H:i:s');
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		if(is_object($dc))
		{
			$db = $dc->db();
		}
		if(!is_object($db))
		{
			return self::return_data(1,'DB IS NULL');
		}
		$result = nl_execute_by_db($sql, $db);
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'全局错误日志_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		// 		$params['nns_id'] = np_guid_rand();
// 		$nns_id = $params['nns_id'];
// 		unset($params['nns_id']);
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	
	/**
	 * 查询全局错误日志列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_val == '--')
				{
					self::$str_where.=" ( isnull({$where_key}) or {$where_key} = '' ) and ";
				}
				else if($where_key == 'create_begin_time')
				{
					self::$str_where.=" nns_create_time >='$where_val' and ";
				}
				else if($where_key == 'create_end_time')
				{
					self::$str_where.=" nns_create_time <='{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} = '{$where_val}' and ";
				}
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_exsist($dc)
	{
		$sql = "select count(*) as count from nns_cp ";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = isset($result[0]['count']) ? $result[0]['count'] : 0;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询所有
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_all($dc)
	{
		$sql="select * from " . self::$base_table;
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete($dc,$nns_id)
	{
		if(strlen($nns_id) <1)
		{
			return self::return_data(1,'全局错误日志_id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}
	
	
	
	/**
	 * 通过时间删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete_by_time($dc,$date)
	{
		if(strlen($date) <1)
		{
			return self::return_data(1,'传入时间为空为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_create_time < '{$date}' ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}
	
	
	/**
	 * 查询多个全局错误日志列表
	 * @param object $dc 数据库对象
	 * @param array $cp_id_arr array(id1,id2)
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_more_id($dc,$cp_id_arr)
	{
		if(!is_array($cp_id_arr) || empty($cp_id_arr))
		{
			return self::return_data(1,'传参不是数组或为空');
		}
		$ids_arr = implode("','", $cp_id_arr);
		$ids_arr = "'".$ids_arr."'";
		$sql = "select nns_id,nns_name from " . self::$base_table . " where nns_id in (" . $ids_arr . ")";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK',$result);
	}

	public static function global_error_log_export_excel($data_list,$count){
		//组装成数组
		$num = 0;
		$arr = array(
			array(cms_get_lang('total'),$count),
			array('序号','消息ID','主媒资注入ID','分集注入ID','片源注入ID','绑定ID','名称','类型','动作','CP_ID','SP_ID','创建时间','描述','日志模板')
		);
		if (!empty($data_list)) {
			foreach ($data_list['data_info'] as $k1=>$v1) {
				$num++;
				$arr[] = array($num,$v1['nns_message_id'],$v1['nns_video_import_id'],$v1['nns_index_import_id'],$v1['nns_media_import_id'],
					$v1['nns_bind_id'],$v1['nns_name'],$v1['nns_type'],$v1['nns_action'],$v1['nns_cp_id'],$v1['nns_sp_id'],$v1['nns_create_time'],
					$v1['nns_desc'],$v1['nns_model']);
			}
		}
		//引入excel.class.php
		$xls = new Excel_XML('UTF-8',false,cms_get_lang('count'));
		$xls->addArray($arr);
		$xls->generateXML('global_error_log');
		die;
	}
}
