<?php
/**
 * CDN直播频道队列管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_live_media_ex extends nl_public
{
	static $base_table='nns_live_media_ex';
	static $arr_filed = array(
			'nns_live_media_id',
			'nns_key',
			'nns_value',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_media_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_live_media_id='{$nns_id}'";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result) && !empty($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 添加CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function add($dc,$params)
	{
		$params = self::make_nns_pre($params);
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改直播片源
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @param string $nns_id GUID
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function edit($dc,$params,$nns_media_id,$nns_key)
	{
		$params = self::make_nns_pre($params);
		if(strlen($nns_media_id)<0)
		{
			return self::return_data(1,'直播片源guid为空');
		}
		if(strlen($nns_key)<0)
		{
		    return self::return_data(1,'直播片源KEY为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_live_media_id'=>$nns_media_id,'nns_key'=>$nns_key));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	/**
	 * 查询单个片源扩展信息
	 * @param unknown $dc
	 * @param unknown $nns_media_id
	 * @param unknown $nns_key
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function query_key_exsist($dc,$nns_media_id,$nns_key)
	{
	    if(strlen($nns_media_id)<0)
	    {
	        return self::return_data(1,'直播片源guid为空');
	    }
	    if(strlen($nns_key)<0)
	    {
	        return self::return_data(1,'直播片源KEY为空');
	    }
	    $sql="select * from " . self::$base_table . " where nns_live_media_id='{$nns_media_id}' and nns_key='{$nns_key}' limit 1";
	    $result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    $result = (isset($result[0]) && is_array($result[0]) && !empty($result[0])) ? $result[0] : null;
	    return self::return_data(0,'ok'.$sql,$result);
	}
	
	
	/**
	 * 查询单个片源扩展信息
	 * @param unknown $dc
	 * @param unknown $nns_media_id
	 * @param unknown $nns_key
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 */
	static public function query_keys_exsist($dc,$nns_media_id,$nns_keys)
	{
	    if(strlen($nns_media_id)<0)
	    {
	        return self::return_data(1,'直播片源guid为空');
	    }
	    $nns_keys = is_array($nns_keys) ? $nns_keys : array($nns_keys);
	    if(empty($nns_keys)<0)
	    {
	        return self::return_data(1,'直播片源KEY为空');
	    }
	    $sql="select * from " . self::$base_table . " where nns_live_media_id='{$nns_media_id}' and nns_key in('" . implode("','", $nns_keys) . "')";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'ok'.$sql,$result);
	}
	
	/**
	 * 查询直播片源列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_key == 'create_begin_time')
				{
					self::$str_where.=" nns_create_time >= '{$where_val}' and ";
				}
				else if($where_key == 'create_begin_time')
				{
					self::$str_where.=" create_end_time <= '{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} = '{$where_val}' and ";
				}
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
}
