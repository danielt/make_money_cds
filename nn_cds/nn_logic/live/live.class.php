<?php
/**
 * CDN直播频道队列管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_live extends nl_public
{
	static $base_table='nns_live';
	static $base_table_ex='nns_live_ex';
	static $arr_filed = array(
			'nns_id',
			'nns_name',
			'nns_state',
			'nns_check',
			'nns_view_type',
			'nns_org_type',
			'nns_org_id',
			'nns_tag',
			'nns_director',
			'nns_show_time',
			'nns_view_len',
			'nns_all_index',
			'nns_new_index',
			'nns_area',
			'nns_image0',
			'nns_image1',
			'nns_image2',
			'nns_image3',
			'nns_image4',
			'nns_image5',
			'nns_summary',
			'nns_remark',
			'nns_create_time',
			'nns_modify_time',
			'nns_deleted',
			'nns_category_id',
			'nns_play_count',
			'nns_score',
			'nns_score_count',
			'nns_point',
			'nns_copyright_date',
			'nns_pinyin',
			'nns_pinyin_length',
			'nns_eng_name',
			'nns_alias_name',
			'nns_language',
			'nns_cp_id',
			'nns_import_id',
	        'nns_import_source',
	        'nns_producer',
	        'nns_image_v',
	        'nns_image_s',
	        'nns_image_h',
	        'nns_image_logo',
            'nns_depot_id',
	);

    /**
     * 扩展信息表字段
     * @var array
     */
	static $arr_filed_ex = array(
	    'nns_live_id',
        'nns_key',
        'nns_value',
        'nns_cp_id'
    );
	/**
	 * 检查数据是否存在通过内容提供商和上游cms标示
	 * @param unknown $dc
	 * @param unknown $cp_id
	 * @param unknown $import_source
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2017-02-27
	 */
	static public function check_cp_producer_exsist($dc,$cp_id,$import_source)
	{
	    $sql="select * from " . self::$base_table . " where nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_ex_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table_ex . " where nns_live_id='{$nns_id}'";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (empty($result)) ? null : $result;
		return self::return_data(0,'OK',$result);
	}
	/**
	 * 添加CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function add($dc,$params)
	{
		$params = self::make_nns_pre($params);
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_create_time']) || strlen($params['nns_create_time'])<1)
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
			$params['nns_modify_time'] = $date_time;
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改直播片源
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @param string $nns_id GUID
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function edit($dc,$params,$nns_id)
	{
        if (isset($params['nns_id']))
        {
            unset($params['nns_id']);
        }

		$params = self::make_nns_pre($params);
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'CDN直播频道队列guid为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
			$params['nns_modify_time'] = date("Y-m-d H:i:s");
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	
	/**
	 * 查询直播片源列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_key == 'create_begin_time')
				{
					self::$str_where.=" nns_create_time >= '{$where_val}' and ";
				}
				else if($where_key == 'create_begin_time')
				{
					self::$str_where.=" create_end_time <= '{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} = '{$where_val}' and ";
				}
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @param array|string $mix_id GUID
	 * @return  array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function delete($dc,$mix_id)
	{
		if(empty($mix_id) || !is_array($mix_id) || !is_string($mix_id))
		{
			return self::return_data(1,'CDN直播频道队列_id为空,或则参数错误：'.var_export($mix_id));
		}
		$str_id= (is_array($mix_id)) ? implode("','", $mix_id) : $mix_id;
		$sql = "update " . self::$base_table . " set nns_deleted=1 where nns_id in ('{$str_id}') ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}

	static public function query_all_filter_tstv($dc)
	{
		$sql = "select a.* from ". self::$base_table ." as a join ". self::$base_table_ex ." as b on a.nns_id = b.nns_live_id where b.nns_key = 'live_type'and find_in_set('PLAYBACK', b.nns_value)";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result);
	}

    /**
     * 依据条件查询直播频道信息
     * @param $dc
     * @param $params
     * @return array
     *
     */
    static public function query_by_condition($dc,$params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(!is_array($params) || empty($params))
        {
            return self::return_data(1,'查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table,$params);
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0,'OK'.$sql,$result);
    }
    
    /**
     * 依据条件查询直播频道信息
     * @param $dc
     * @param $params
     * @return array
     *
     */
    static public function query_ex_by_condition($dc,$params)
    {
        $params = self::except_useless_params(self::$arr_filed_ex, $params);
        if(!is_array($params) || empty($params))
        {
            return self::return_data(1,'查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table_ex,$params);
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0,'OK'.$sql,$result);
    }

    /**
     * 获取扩展信息
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del_ex_by_condition($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table_ex,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }


    /**
     * @description:更新扩展信息
     * @author:xinxin.deng
     * @date: 2017/12/5 15:01
     * @param $dc
     * @param $params
     * @return array
     */
    static public function update_ex_info($dc,$params, $where)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_update_sql(self::$base_table_ex,$params, $where);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

    /**
     * @description:向扩展表添加数据
     * @author:xinxin.deng
     * @date: 2017/12/5 15:18
     * @param $dc
     * @param $params
     * @return array
     */
    static public function add_ex($dc,$params)
    {
        $params = self::except_useless_params(self::$arr_filed_ex, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table_ex, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok');
    }

    /**
     * @description:删除直播
     * @author:xinxin.deng
     * @date: 2017/12/5 14:07
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

    /**
     * @description:删除直播扩展表
     * @author:xinxin.deng
     * @date: 2017/12/5 14:07
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del_ex($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table_ex,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

    /**
     * 根据直播ID获取直播信息
     * @param $dc
     * @param array //$arr_id 直播ID一维数组
     * @param bool //$is_query_ex 是否查询直播扩展信息
     * @return array
     */
    static public function query_live_by_id($dc,$arr_id,$is_query_ex=true)
    {
        if(empty($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);
        $sql = "select * from " . self::$base_table . " where nns_id in ('{$str_ids}')";
        $live_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($live_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $live_ex_re = array();
        if($is_query_ex)
        {
            $ex_sql = "select ex.*,v.nns_id from " . self::$base_table_ex . " as ex left join " . self::$base_table . " as v on v.nns_asset_import_id=ex.nns_vod_id and v.nns_cp_id=ex.nns_cp_id where v.nns_id in ('{$str_ids}')";
            $live_ex_re = nl_query_by_db($ex_sql,$dc->db());
            if(!is_array($live_ex_re))
            {
                $live_ex_re = array();
            }
            if(!empty($live_ex_re))
            {
                foreach ($live_ex_re as $ex_info)
                {
                    $vod_ex[$ex_info['nns_id']][$ex_info['nns_key']] = $ex_info['nns_value'];
                }
            }
        }
        $result = array();
        foreach ($live_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
            if(isset($live_ex_re[$info['nns_id']]))
            {
                $result[$info['nns_id']]['ex_info'] = $live_ex_re[$info['nns_id']];
            }
            else
            {
                $result[$info['nns_id']]['ex_info'] = array();
            }
        }
        if(empty($result))
        {
            return self::return_data(1,'live数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }
}
