<?php
/**
 * CDN直播频道队列管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_live_index extends nl_public
{
	static $base_table='nns_live_index';
    static $base_table_ex='nns_live_index_ex';
	static $arr_filed = array(
			'nns_id',
			'nns_name',
			'nns_live_id',
			'nns_index',
			'nns_time_len',
			'nns_summary',
			'nns_create_time',
			'nns_modify_time',
			'nns_image',
			'nns_play_count',
			'nns_score',
			'nns_score_count',
			'nns_cp_id',
	        'nns_import_source',
            'nns_deleted',
            'nns_import_id'
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_ids($dc,$nns_id)
	{
	    $sql = is_string($nns_id) ? "select * from " . self::$base_table . " where nns_id='{$nns_id}' " : "select * from " . self::$base_table . " where nns_id in('" . implode("','", $nns_id) . "') ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (!empty($result[0]) && is_array($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_live_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_live_id='{$nns_id}'";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (!empty($result) && is_array($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function add($dc,$params)
	{
		$params = self::make_nns_pre($params);
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_create_time']) || strlen($params['nns_create_time'])<1)
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
			$params['nns_modify_time'] = $date_time;
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改直播片源
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @param array|string $nns_id GUID
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function edit($dc,$params,$nns_id)
	{
		$params = self::make_nns_pre($params);
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'CDN直播频道队列guid为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
			$params['nns_modify_time'] = date("Y-m-d H:i:s");
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	
	/**
	 * 查询直播片源列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_key == 'create_begin_time')
				{
					self::$str_where.=" nns_create_time >= '{$where_val}' and ";
				}
				else if($where_key == 'create_begin_time')
				{
					self::$str_where.=" create_end_time <= '{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} = '{$where_val}' and ";
				}
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @param string $nns_id GUID
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function delete($dc,$mix_id)
	{
		if(empty($mix_id) || !is_array($mix_id) || !is_string($mix_id))
		{
			return self::return_data(1,'CDN直播频道队列_id为空,或则参数错误：'.var_export($mix_id));
		}
		$str_id= (is_array($mix_id)) ? implode("','", $mix_id) : $mix_id;
		$sql = "update " . self::$base_table . " set nns_deleted=1 where nns_id in ('{$str_id}') ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}

    /**
     * @description:根据条件查询分集
     * @author:xinxin.deng
     * @date: 2017/12/7 14:40
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (! is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && ! empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }
    /**
     * 根据分集ID获取媒资分集信息
     * @param $dc
     * @param $arr_id 直播分集ID一维数组
     * @param $is_query_ex 是否查询直播分集扩展信息
     * @return array
     */
    static public function query_live_index_by_id($dc,$arr_id,$is_query_ex=true)
    {
        if(empty($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);
        $sql = "select * from " . self::$base_table . " where nns_id in ('{$str_ids}')";
        $vod_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($vod_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $vod_ex = array();
        if($is_query_ex)
        {
            $ex_sql = "select ex.*,v.nns_id from " . self::$base_table_ex . " as ex left join " . self::$base_table . " as v on v.nns_import_id=ex.nns_vod_index_id and v.nns_cp_id=ex.nns_cp_id where v.nns_id in ('{$str_ids}')";
            $vod_ex_re = nl_query_by_db($ex_sql,$dc->db());
            if(!is_array($vod_ex_re))
            {
                $vod_ex_re = array();
            }
            if(!empty($vod_ex_re))
            {
                foreach ($vod_ex_re as $ex_info)
                {
                    $vod_ex[$ex_info['nns_id']][$ex_info['nns_key']] = $ex_info['nns_value'];
                }
            }
        }
        $result = array();
        foreach ($vod_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
            if(isset($vod_ex[$info['nns_id']]))
            {
                $result[$info['nns_id']]['ex_info'] = $vod_ex[$info['nns_id']];
            }
            else
            {
                $result[$info['nns_id']]['ex_info'] = array();
            }
        }
        if(empty($result))
        {
            return self::return_data(1,'live_index数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }
}
