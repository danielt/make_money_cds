<?php
/**
 * CDN直播频道队列管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_third_live extends nl_public
{
	static $live = "nns_live";
	static $live_ex='nns_live_ex';
	static $base_table = 'nns_live_bind_third_platform';
	static $base_live_table = 'nns_live';
	static $arr_filed = array(
		'nns_id',
		'nns_live_id',
		'nns_third_live_id',
		'nns_cp_id',
		'nns_third_live_name',
		'nns_create_time',
		'nns_modify_time'
	);

	/**
	 * 检查数据是否存在通过内容提供商和上游cms标示
	 * @param unknown $dc
	 * @param unknown $cp_id
	 * @param unknown $import_source
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2017-02-27
	 */
	static public function check_cp_producer_exsist($dc, $cp_id, $import_source)
	{
		$sql = "select * from " . self::$base_table . " where nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if (!$result)
		{
			return self::return_data(1, '数据库查询失败,sql:' . $sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0, 'OK', $result);
	}

	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_id($dc, $nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if (!$result)
		{
			return self::return_data(1, '数据库查询失败,sql:' . $sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0, 'OK', $result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_all($dc)
	{
	    $sql = "select * from " . self::$base_table;
	    $result = nl_query_by_db($sql, $dc->db());
	    if (!$result)
	    {
	        return self::return_data(1, '数据库查询失败,sql:' . $sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0, 'OK', $result);
	}

	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_ex_by_id($dc, $nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_live_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if (!$result)
		{
			return self::return_data(1, '数据库查询失败,sql:' . $sql);
		}
		$result = (empty($result)) ? null : $result;
		return self::return_data(0, 'OK', $result);
	}

	/**
	 * 添加CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function add($dc, $params)
	{
		$params = self::make_nns_pre($params);
		if (!isset($params['nns_id']) || strlen($params['nns_id']) < 1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$date_time = date("Y-m-d H:i:s");
		if (!isset($params['nns_create_time']) || strlen($params['nns_create_time']) < 1)
		{
			$params['nns_create_time'] = $date_time;
		}
		if (!isset($params['nns_modify_time']) || strlen($params['nns_modify_time']) < 1)
		{
			$params['nns_modify_time'] = $date_time;
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if (empty($params))
		{
			return self::return_data(1, '参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if (!$result)
		{
			return self::return_data(1, '数据库执行失败' . $sql);
		}
		return self::return_data(0, 'ok');
	}

	/**
	 * 修改直播片源
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @param string $nns_id GUID
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function edit($dc, $params, $nns_id)
	{
		$params = self::make_nns_pre($params);
		if (strlen($nns_id) < 0)
		{
			return self::return_data(1, 'CDN直播频道队列guid为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if (empty($params))
		{
			return self::return_data(1, '参数为空');
		}
		if (!isset($params['nns_modify_time']) || strlen($params['nns_modify_time']) < 1)
		{
			$params['nns_modify_time'] = date("Y-m-d H:i:s");
		}
		$sql = self::make_update_sql(self::$base_table, $params, array('nns_id' => $nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if (!$result)
		{
			return self::return_data(1, '数据库执行失败' . $sql);
		}
		return self::return_data(0, 'ok');
	}


	/**
	 * 查询直播片源列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query($dc, $params, $page_info = null)
	{
		if (isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key => $where_val)
			{
				if ($where_key == 'create_begin_time')
				{
					self::$str_where .= " base.nns_create_time >= '{$where_val}' and ";
				} 
				else if ($where_key == 'create_begin_time')
				{
					self::$str_where .= " base.create_end_time <= '{$where_val}' and ";
				} 
				else if ($where_key == 'nns_cp_id')
				{
				    self::$str_where .= " base.nns_cp_id = '{$where_val}' and ";
				}
				else
				{
					self::$str_where .= " base.{$where_key} = '{$where_val}' and ";
				}
			}
		}
		if (isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key => $like_val)
			{
				self::$str_where .= " base.{$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where, 'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql = "select base.* from " . self::$base_table . " as base " . self::$str_where . " order by base.nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) from " . self::$base_table . " as base " . self::$str_where;
		$result_count = nl_query_by_db($sql_count, $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if (!$result || !$result_count)
		{
			return self::return_data(1, "查询数据失败" . $sql);
		}
		return self::return_data(0, "查询数据成功", $result, $result_count);
	}


	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @param string $nns_id GUID
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function delete($dc, $str_id)
	{
		$sql = "delete from " . self::$base_table . " where nns_id in ('{$str_id}') ";
		$result = nl_execute_by_db($sql, $dc->db());
		if (!$result)
		{
			return self::return_data(1, '数据库查询失败,sql:' . $sql);
		}
		return self::return_data(0, 'OK');
	}

	static public function check_exist($dc, $param)
	{
		if (isset($param['nns_live_id']) && !empty($param['nns_live_id']))
		{
			$sql = "select * from ". self::$base_table ." where nns_live_id = '{$param['nns_live_id']}'";
			$result = nl_query_by_db($sql, $dc->db());
			if (is_array($result))
			{
				return self::return_data(1, "频道已被绑定");
			}
		}
		if (isset($param['nns_third_live_id']) && !empty($param['nns_third_live_id']))
		{
			$sql = "select * from ". self::$base_table ." where nns_third_live_id = '{$param['nns_third_live_id']}' and nns_cp_id='{$param['nns_cp_id']}'";
			$result = nl_query_by_db($sql, $dc->db());
			if (is_array($result))
			{
				return self::return_data(1, "第三方频道已被绑定");
			}
		}
		return self::return_data(0, "可以进行绑定", $result);
	}

	/**
	 * 根据频道类型获取绑定的三方频道id
	 * @author <feijian.gao@starcor>
	 * @date   2017年7月20日10:01:38
	 * @param object $dc 			数据库操作对象
	 * @param string $str_live_type 频道类型
	 * @return array array('ret'=>'','reason'=>'','data_info'=>array())
	 */
	static public function get_id_filter_live_type($dc,$str_live_type = 'PLAYBACK')
	{
		if(empty($str_live_type))
		{
			return self::return_data(1, "频道类型为空");
		}
		$sql = "SELECT live_external.nns_live_id,live_external.nns_third_live_id FROM nns_live AS live LEFT JOIN nns_live_ex AS live_ex ON live.nns_id = live_ex.nns_live_id RIGHT JOIN nns_live_bind_third_platform as live_external ON live.nns_id = live_external.nns_live_id WHERE live_ex.nns_key = 'live_type' AND FIND_IN_SET('{$str_live_type}',live_ex.nns_value)";
		$result = nl_query_by_db($sql, $dc->db());
		if (count($result)==0)
		{
			return self::return_data(1, "没有回看类型频道绑定第三方频道");
		}
		return self::return_data(0, "成功", $result);
	}
}
