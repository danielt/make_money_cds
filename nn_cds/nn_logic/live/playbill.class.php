<?php
/**
 * CDN直播频道队列管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_playbill extends nl_public
{
	static $base_table='nns_live_playbill_item';
	static $arr_filed = array(
			'nns_id',
			'nns_name',
			'nns_begin_time',
			'nns_time_len',
			'nns_live_id',
			'nns_create_time',
			'nns_state',
			'nns_summary',
			'nns_image1',
			'nns_image2',
			'nns_image3',
			'nns_image4',
			'nns_image5',
			'nns_image0',
			'nns_pinyin',
			'nns_pinyin_length',
			'nns_eng_name',
			'nns_keyword',
			'nns_kind',
			'nns_image6',
			'nns_actor',
			'nns_director',
			'nns_live_media_id',
			'nns_playbill_import_id',
			'nns_domain',
			'nns_hot_dgree',
			'nns_message_id',
			'nns_timeshift_status',
			'nns_timeshift_delay',
			'nns_storage_status',
			'nns_storage_delay',
			'nns_cp_id',
	        'nns_import_source',
	        'nns_domain',
            'nns_deleted',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_ids($dc,$nns_id)
	{
	    if(!is_array($nns_id) || empty($nns_id))
	    {
	        return self::return_data(0,'OK');
	    }
	    $sql = "select * from " . self::$base_table . " where nns_id in('".implode("','", $nns_id)."')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_exsist_by_live_id($dc,$nns_live_id)
	{
		$sql = "select count(*) as total from " . self::$base_table . " where nns_live_id='{$nns_live_id}'";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]['total']) && $result[0]['total'] >0) ? $result[0]['total'] : 0;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query_by_live_id($dc,$nns_live_id,$start_time)
	{
		$sql = "select * from " . self::$base_table . " where nns_live_id='{$nns_live_id}' and nns_begin_time >='{$start_time}'";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result) && !empty($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
	}

	static public function query_by_live_id_field($dc,$nns_live_id)
	{
		$sql = "select nns_name,nns_begin_time,nns_time_len,nns_live_id,nns_summary,nns_image0,nns_image1,nns_image2,nns_image3,nns_image4,nns_image5,nns_actor,nns_director,nns_playbill_import_id from " . self::$base_table . " where nns_live_id='{$nns_live_id}' and nns_state = 0";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result) && !empty($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 添加CDN直播频道队列
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function add($dc,$params)
	{
		$params = self::make_nns_pre($params);
		if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
		{
			$params['nns_id'] = np_guid_rand();
		}
		$date_time = date("Y-m-d H:i:s");
		if(!isset($params['nns_create_time']) || strlen($params['nns_create_time'])<1)
		{
			$params['nns_create_time'] = $date_time;
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
			$params['nns_modify_time'] = $date_time;
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改直播片源
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @param string $nns_id GUID
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function edit($dc,$params,$nns_id)
	{
		$params = self::make_nns_pre($params);
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'CDN直播频道队列guid为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
		{
			$params['nns_modify_time'] = date("Y-m-d H:i:s");
		}
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	
	/**
	 * 查询直播片源列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
				if($where_key == 'create_begin_time')
				{
					self::$str_where.=" nns_create_time >= '{$where_val}' and ";
				}
				else if($where_key == 'create_begin_time')
				{
					self::$str_where.=" create_end_time <= '{$where_val}' and ";
				}
				else
				{
					self::$str_where.=" {$where_key} = '{$where_val}' and ";
				}
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql);
		}
		return self::return_data(0,"查询数据成功",$result,$result_count);
	}
	
	
	/**
	 * 查询唯一的队列数据
	 * @param unknown $dc
	 * @param unknown $live_id
	 * @param unknown $live_index_id
	 * @param unknown $live_media_id
	 * @param unknown $cp_id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'), multitype:NULL number string , number, mixed, multitype:NULL multitype:a r y s t i n g  array multitype:number  >
	 */
	static public function query_playbill_is_exsist($dc,$live_id,$live_media_id,$cp_id)
	{
		if(strlen($live_id) <1)
		{
			return self::return_data(1,"直播ID为空：".$live_id);
		}
		if(strlen($live_media_id) <1)
		{
			return self::return_data(1,"直播片源ID为空：".$live_media_id);
		}
		if(strlen($cp_id) <1)
		{
			return self::return_data(1,"直播分集ID为空：".$cp_id);
		}
		$sql="select * from " . self::$base_table . " where nns_cp_id='{$cp_id}' and nns_live_id='{$live_id}' and nns_live_media_id='{$live_media_id}' ";
		$result = nl_query_by_db($sql, $dc->db());
		$result = (isset($result) && !empty($result) && is_array($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @param string $nns_id GUID
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-09-24
	 */
	static public function delete($dc,$mix_id)
	{
		if(empty($mix_id) || !is_array($mix_id) || !is_string($mix_id))
		{
			return self::return_data(1,'CDN直播频道队列_id为空,或则参数错误：'.var_export($mix_id));
		}
		$str_id= (is_array($mix_id)) ? implode("','", $mix_id) : $mix_id;
		$sql = "update " . self::$base_table . " set nns_deleted=1 where nns_id in ('{$str_id}') ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}

    /**
     * 根据频道id查询某一天的节目单
     * 说明：0表示当天，-1表示当天的前一天，+1表示当天的后一天，依次类推
     * @author <feijian.gao@starcor.cn>
     * @date    2017年7月20日14:15:19
     * @param object $dc 数据库操作对象
     * @param string $str_live_id 频道id
     * @param null $str_date
     * @return array array('ret'=>'','reason'=>'','data_info'=>array())
     */
	static public function get_playbill_by_day($dc, $str_live_id, $str_date=null)
	{
		$str_time_tmp = date("Ymd",time());
		//当天的开始时间
		$str_now_time_begin = strtotime($str_time_tmp);
		//当天的开始时间
		$str_now_time_end = strtotime($str_time_tmp."235959");
		$str_where_time = '';

		//对$int_day参数的值进行处理
		if(isset($str_date) && strlen($str_date) > 0)
		{
		    $str_date_format = date("Y-m-d",strtotime($str_date));
            if(!$str_date_format)
            {
			    return self::return_data(1,"时间参数不正确");
            }
            $str_now_time_begin = date("Y-m-d H:i:s",strtotime($str_date_format));
            $str_now_time_end = date("Y-m-d H:i:s",strtotime($str_date_format."23:59:59"));
            $str_where_time = "nns_begin_time >= '{$str_now_time_begin}' and nns_begin_time <= '{$str_now_time_end}'";
		}
		else
        {
            return self::return_data(1,"时间参数不正确");
        }
		$sql = "select nns_name,nns_begin_time,nns_time_len,nns_live_id,nns_summary,nns_image0,nns_image1,nns_image2,nns_image3,nns_image4,nns_image5,nns_actor,nns_director,nns_playbill_import_id from " . self::$base_table . " where nns_live_id='{$str_live_id}' and nns_state = 0 and " . $str_where_time ." order by nns_begin_time asc";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result) && !empty($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 根据频道id查询某一天的节目单
	 * 说明：0表示当天，-1表示当天的前一天，+1表示当天的后一天，依次类推
	 * @author <feijian.gao@starcor.cn>
	 * @date    2017年7月20日14:15:19
	 * @param object $dc 数据库操作对象
	 * @param string $str_live_id 频道id
	 * @param null $str_date
	 * @return array array('ret'=>'','reason'=>'','data_info'=>array())
	 */
	static public function get_playbill_by_del_day($dc, $str_live_id, $str_date)
	{
	    $sql = "select nns_id from " . self::$base_table . " where nns_live_id='{$str_live_id}' and nns_state = '0' and nns_begin_time<='{$str_date}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 根据频道id查询某一天的节目单
	 * 说明：0表示当天，-1表示当天的前一天，+1表示当天的后一天，依次类推
	 * @author <feijian.gao@starcor.cn>
	 * @date    2017年7月20日14:15:19
	 * @param object $dc 数据库操作对象
	 * @param string $str_live_id 频道id
	 * @param null $str_date
	 * @return array array('ret'=>'','reason'=>'','data_info'=>array())
	 */
	static public function playbill_by_del_day($dc, $str_live_id, $str_date)
	{
	    $sql = "delete from " . self::$base_table . " where nns_live_id='{$str_live_id}' and nns_state = '1' and nns_begin_time<='{$str_date}'";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 修改全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit_v2($dc,$params,$where_condition)
	{
	    if(!is_array($where_condition))
	    {
	        return self::return_data(1,'条件查询必须为数组');
	    }
	    $where_condition = self::except_useless_params(self::$arr_filed, $where_condition);
	    if(empty($where_condition) || !is_array($where_condition))
	    {
	        return self::return_data(1,'全局错误日志_id为空');
	    }
	    $params = self::except_useless_params(self::$arr_filed, $params);
	    if(empty($params))
	    {
	        return self::return_data(1,'参数为空');
	    }
	    $params['nns_modify_time'] = date("Y-m-d H:i:s");
	    $sql = self::make_update_sql(self::$base_table, $params,$where_condition);
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok');
	}
    /**
     * 根据节目单ID获取媒资信息
     * @param $dc
     * @param array //$arr_id //节目单ID一维数组
     * @return array
     */
    static public function query_playbill_by_id($dc,$arr_id)
    {
        if(empty($arr_id) || !is_array($arr_id))
        {
            return self::return_data(1,'参数错误');
        }
        $str_ids = implode("','",$arr_id);

        $sql = "select * from " . self::$base_table . " where nns_id in ('{$str_ids}')";

        $palybill_re = nl_query_by_db($sql,$dc->db());
        if(!is_array($palybill_re))
        {
            return self::return_data(1,'数据库执行失败或数据查询为空：'.$sql);
        }
        $result = array();
        foreach ($palybill_re as $info)
        {
            $result[$info['nns_id']]['base_info'] = $info;
        }
        if(empty($result))
        {
            return self::return_data(1,'live数据查询结果为空');
        }
        return self::return_data(0,'ok'.$sql,$result);
    }

}
