<?php

/**
 * 影片资源分类标签绑定表基础业务
 * @author yxt 2013年1月16日12:16:59
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_video_label_bind_content{
	/**
	 * 增加绑定
	 * @param $dc
	 * @param string $video_id影片id
	 * @param string $video_type影片类型 0为点播 1为直播 2为节目
	 * @param string $label_id标签id
	 * @param string $label_type_id:标签类型id
	 * @return bool 成功返回true，失败返回false
	 */
	static public function add_video_label_content ($dc,$video_id,$video_type,$label_id,$label_type_id){
		if(empty($video_id) || empty($label_id)){
			return false;
		}
		$sql="select count(*) as n from  nns_video_label_bind_".$label_type_id." where nns_video_id='".$video_id."' and nns_label_id='".$label_id."'";
		 
		$res=nl_query_by_db($sql, $dc->db());
		  
		if($res[0]['n']>=1)
		{
			return 'back';
		}
		else
		{
		//更新影片数量
		
		$sql="insert into nns_video_label_bind_{$label_type_id}(nns_video_id,nns_video_type,nns_label_id,nns_label_type_id) values('{$video_id}','{$video_type}','{$label_id}','{$label_type_id}')";
	
		$ret=nl_execute_by_db($sql, $dc->db());
		if($ret)
		{
			$sql="update nns_video_label set nns_count=nns_count+1 where nns_id='{$label_id}'";
			nl_execute_by_db($sql, $dc->db());
			 
		}
		return 'ok';
	 }
  
	}
	/**获取绑定表数据
	 * @param unknown $dc
	 * @param unknown $video_id
	 * @param unknown $video_type
	 * @param unknown $label_id
	 * @param unknown $label_type_id
	 * @return boolean|Ambigous <*, boolean, unknown>
	 */
	static public function get_label_bind_video_list($dc,$nns_id,$label_id){
		if(empty($nns_id)){
			return false;
		}
		 
		$sql="select * from  nns_video_label_bind_".$nns_id." where nns_label_id='".$label_id."'";
		 
		$res=nl_query_by_db($sql, $dc->db());
		//$count = count($res);
		//$res[0]['nums'] = $count;
		 
		return $res;
	}
	/**
	 * 解除绑定
	 * @param $dc
	 * @param string $video_type影片类型 0为点播 1为直播 2为节目
	 * @param string $video_id影片id
	 */
	static public function del_video_label_content ($dc,$video_id,$video_type){
		//遍历绑定表
		$sql="show tables like 'nns_video_label_bind_%'";
		$tables=nl_query_by_db($sql, $dc->db());
		if(is_bool($tables)){
			return $tables;
		}
		//循环接触绑定
		foreach ($tables as $table){
			$sql="delete from {$table["Tables_in_".g_db_name." (nns_video_label_bind_%)"]} where nns_video_type='{$video_type}' and nns_video_id='{$video_id}'";
			$ret=nl_execute_by_db($sql, $dc->db());
		}
		return $ret;
	}
	//解除绑定( 标签绑定视频)
    static public function del_video_label_bind($dc,$video_id,$label_id){
		//遍历绑定表
		$video_id1 = stripcslashes(trim($video_id,','));

		$sql="show tables like 'nns_video_label_bind_%'";
		$tables=nl_query_by_db($sql, $dc->db());

		if(is_bool($tables)){
			return $tables;
		}
		//循环解除绑定
		foreach ($tables as $table){

			$key = key($table);
			$sql="delete from {$table[$key]} where nns_video_id in({$video_id1}) and nns_label_id={$label_id}";

			$ret=nl_execute_by_db($sql, $dc->db());
		}

		if($ret)
		{
			$video_arr = explode(',',$video_id1);
			$del_num = count($video_arr);
			
			$sql="update nns_video_label set nns_count=nns_count-{$del_num} where nns_id='{$label_id}'";

			nl_execute_by_db($sql, $dc->db());

		}
		
		return $ret;
	}
	/**
	 * 根据影片id影片type获取标签id
	 * @param $dc
	 * @param string $video_id
	 * @param string $video_type
	 * @return bool：true(结果为空)，false(执行失败)
	 * @return array：结果数组
	 */
	static public function get_label_by_video_id($dc,$video_id,$video_type){
		//遍历绑定表
		$sql="show tables like 'nns_video_label_bind_%'";
		$tables=nl_query_by_db($sql, $dc->db());
		if(is_bool($tables)){
			return $tables;
		}

		$data=array();
		//循环接触绑定
		foreach ($tables as $table){
			//    2013-05-17 Jacen Edit Tables_in_nn_cms to Tables_in_'.g_db_name.' 以适应数据库名称变化的情况
			$sql='select nns_label_id from '.$table['Tables_in_'.g_db_name.' (nns_video_label_bind_%)'].' where nns_video_type='.$video_type.' and nns_video_id=\''.$video_id.'\'';
			
			$ret=nl_query_by_db($sql, $dc->db());
			if(!is_bool($ret)){
				foreach ($ret as $row){
					$data[]=$row["nns_label_id"];
				}
			}
		}
		if(empty($data)){
			return true;
		}
		return $data;
	}
	/**
	 * 通过标签组获取标签组对应的影片信息
	 * @param obj $dc
	 * @param array() $label_id标签id
	 * @return bool,执行错误或者结果为空
	 * array,返回标签包含通过审核影片的全部信息
	 */
	static public function get_video_info_by_label_id($dc,$label_id){
		if(empty($label_id)){
			return true;
		}
		//先根据标签id获取标签的类型，然后联合绑定表和媒资包去查询
		$sql="select * from nns_video_label where nns_id ='{$label_id}'";
		$res1=nl_query_by_db($sql, $dc->db());
		if(is_bool($res1)){
			return $res1;
		}
		$sql="select assists.* from nns_assists_item as assists,nns_video_label_bind_{$res1[0]['nns_type']} as bind where assists.nns_check=1 and assists.nns_video_type=bind.nns_video_type and assists.nns_video_id=bind.nns_video_id";
//		var_dump($sql);
		$res2=nl_query_by_db($sql, $dc->db());
		return $res2;
	}
	/**
	 * 根据标签类和媒资包id，获取该媒资包下的这种类型下的标签信息
	 * @param $dc
	 * @param string $type 1000/2000...
	 * @param string $asset_id movie....
	 * @param string $order
	 */
	static public function get_label_by_type_asset($dc,$type,$asset_id,$sina,$num,$order="times"){
		$sql="select label.* from nns_video_label as label,nns_video_label_bind_{$type} as bind,nns_assists_item as asset where asset.nns_video_id=bind.nns_video_id and bind.nns_label_id=label.nns_id and label.nns_state='1' GROUP BY label.nns_name order by label.nns_{$order} desc limit {$sina},{$num}";
		return nl_query_by_db($sql, $dc->db());
	}
        
        
    /**
     * 解除绑定
     * @param LMS加
     * @param string $video_type影片类型 0为点播 1为直播 2为节目
     * @param string $video_id影片id
     */
    static public function del_video_label($dc, $video_id, $label_id, $label_type) {
            if(empty($video_id) || empty($label_id) || empty($label_type)){
                return ;
            }
        $sql = "delete from  nns_video_label_bind_$label_type where nns_video_id='{$video_id}' and nns_label_id='{$label_id}'";
        $ret = nl_execute_by_db($sql, $dc->db());
        return $ret;
    }
    
}

//$dc=nl_get_dc(array("db_policy"=>NL_DB_WRITE,"cache_policy"=>NP_KV_CACHE_TYPE_MEMCACHE));
//$dc->open();
////$r=nl_video_label_bind_content::add_video_label_content ($dc,"015f2601999bc05e3c082c4c65a52130",1,"001");
////$r=nl_video_label_bind_content::del_video_label_content($dc,"fdasfdsafdsa",1);
//$r=nl_video_label_bind_content::get_video_info_by_label_id($dc,"00000016");
//var_dump($r);