<?php
/*
 * Created on 2013-1-20
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 include_once 'label_type.class.php';
 include_once 'video_label.class.php';
 include_once 'video_label_bind_content.class.php';
 class nl_video_label_bo{
 	// 	    类型
// 		'kind'=>'类型',
//// 		地区
// 		'area'=>'地区',
//// 		年份
// 		'year'=>'年份',
//// 		语种
// 		'language'=>'语种',
//// 		导演
// 		'director'=>'导演',
//// 		演员
// 		'actor'=>'演员',
//// 		出品人
// 		'producer'=>'出品人',
//// 		编剧
// 		'screenwriter'=>'编剧',
//// 		字幕语种
// 		'text_lang'=>'字幕语种'

 	static $type_arr=array(
// 	    类型
 		'kind'=>array(
			'id'=>'1000',
			'name'=>'类型',
			),
// 		地区
 		'area'=>array(
			'id'=>'1002',
			'name'=>'地区',
			),
// 		年份
 		'year'=>array(
			'id'=>'1003',
			'name'=>'年份',
			),
// 		语种
 		'language'=>array(
			'id'=>'1004',
			'name'=>'语种',
			),
// 		导演
 		'director'=>array(
			'id'=>'1005',
			'name'=>'导演',
			),
// 		演员
 		'actor'=>array(
			'id'=>'1006',
			'name'=>'演员',
			),
// 		出品人
 		'producer'=>array(
			'id'=>'1007',
			'name'=>'出品商',
			),
// 		编剧
 		'screenwriter'=>array(
			'id'=>'1008',
			'name'=>'编剧',
			),
// 		字幕语种
 		'text_lang'=>array(
			'id'=>'1009',
			'name'=>'字幕语种',
			),
//       首字母
		'initial'=>array(
			'id'=>'1001',
			'name'=>'首字母',
		),
		
		//       首字母
		'copyright'=>array(
			'id'=>'1010',
			'name'=>'版权',
		),
 	);
 static public function get_label_types_with_default($dc){
 	$label_types=nl_label_type::get_label_types($dc);
 	if(is_array($label_types)){
	 	foreach ($label_types as $label_type){
	 		if ($label_type['nns_field'] && !array_key_exists($label_type['nns_field'],self::$type_arr)){
	 			self::$type_arr[$label_type['nns_field']]=array(
	 					'id'=>$label_type['nns_id'],
	 					'name'=>$label_type['nns_name']
	 				);
	 		}
	 	}
 	}
 	return self::$type_arr;
 }

  /**
  * 为绑定主类型标签
  * @param 	"video_id"=>视频ID
  *	@param	"video_type"=>视频类型 0点播 | 1直播 | 2节目
  * @param array(
  * 	"type_id"=>类型ID
  * 	"type_name"=>类型名
  *		 "name"=>标签名,/分割

  * )
  * @return String 类型ID
  *
  */
 static public function bind_video_label_by_video($dc,$video_id,$video_type,$params){
 	$params['source']='main';
 	$label_names=$params['name'];


 	$label_types=nl_label_type::get_label_types($dc);
 	if(is_array($label_types)){
	 	foreach ($label_types as $label_type){
	 		if (!array_key_exists($label_type['nns_field'],self::$type_arr)){
	 			self::$type_arr[$label_type['nns_field']]=array(
	 					'id'=>$label_type['nns_id'],
	 					'name'=>$label_type['nns_name']
	 				);
	 		}
	 	}
 	}


// 	$label_names=np_build_split_string($params['name']);
	if (empty($label_names)) return FALSE;
	$label_names=explode('/',$label_names);
// 	SETP1 添加标签
foreach ($label_names as $label_name){
	$params['name']=trim($label_name,' ');
	$label_id=self::add_video_label_by_type($dc,$params);
//	echo('id:'.$label_id);
	if ($label_id===FALSE) continue;
	
	$bool=nl_video_label_bind_content::add_video_label_content(
		$dc,
		$video_id,
		$video_type,
		$label_id,
		self::$type_arr[$params['type_id']]['id']
	);
}
	return $bool;
 }
  /**
  * 添加标签
  * @param array(
  * 	"type_id"=>类型ID
  *		 "name"=>标签名,
  *		"source"=>标签来源，
  * )
  * @return String 类型ID
  *
  */
 static public function add_video_label_by_type($dc,$params){
// 	var_dump($params);
 	if (empty($params['name'])) return FALSE;
 	if (!array_key_exists($params['type_id'],self::$type_arr)) {
 		return FALSE;
 		}



 	$result=nl_label_type::get_label_type_by_id($dc,self::$type_arr[$params['type_id']]['id']);
 	if ($result===TRUE){

 	//	 	STEP1  添加标签类型
	 	$result=nl_label_type::add_label_type(
	 	$dc,
	 	self::$type_arr[$params['type_id']]['id'],
	 	self::$type_arr[$params['type_id']]['name'],
	 	'video',
	 	(int)self::$type_arr[$params['type_id']]['id'],
	 	$params['type_id'],
	 	$params['type_id']
	 	);

 	}

 	//var_dump($result);
	 if ($result===FALSE) return FALSE;

//    STEP2 添加标签信息
		$result=nl_video_label::add_video_label(
	 	$dc,
	 	$params['name'],
	 	array(
	 		'type'=>self::$type_arr[$params['type_id']]['id'],
	 		'source'=>$params['source']
	 	)
	 	);
	 	return $result;

	 }
 }
?>
