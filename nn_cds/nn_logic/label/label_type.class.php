<?php
/**
 * 影片资源分类标签类型表操作
 * @author yxt 2013年1月15日14:06:33
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
//include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'label'.DIRECTORY_SEPARATOR.'asset_item_bo.class.php';
class nl_label_type{
	//可以添加的字段
	static $add_fields=array("name"=>"","type"=>"","weight"=>"","summary"=>"");
	//可以修改的字段
	static $modify_fields=array("weight"=>"","summary"=>"");
	/**
	 * 添加标签类型，前三个参数必须传
	* @param $dc DC模块
	* @param string $id标签类型id
	* @param string $name:标签名称
	* @param string $type标签类型种类,默认为video
	* @param string $weight标签权重
	* @param string $summary标签类型描述
	* @return bool $ret
	*/

	static public function add_label_type($dc,$id,$name,$type,$weight=1,$summary="",$field_name=""){
		//检查标签影片绑定表是否存在
		$check_table_exis=nl_query_by_db("show tables like 'nns_video_label_bind_{$id}'", $dc->db());
		//如果不存在则创建
//		var_dump($check_table_exis);
		if($check_table_exis===true){
			$sql="CREATE TABLE nns_video_label_bind_{$id} (
				  nns_video_id varchar(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
				  nns_video_type int(1) NOT NULL DEFAULT 0,
				  nns_label_id char(32) NOT NULL ,
				  nns_label_type_id char(4) NOT NULL DEFAULT '0000',
				  KEY nns_video_id (nns_video_id,nns_label_id)
				) ENGINE=InnoDB DEFAULT CHARSET=utf8;";
			$create_talble=nl_execute_by_db($sql, $dc->db());
		}
		//var_dump($create_talble);die();
		//如果没有传递id则返回false
		$params=array(
			"nns_id"=>$id,
			"nns_name"=>$name,
			"nns_type"=>$type,
			"nns_weight"=>$weight,
			"nns_create_time"=>date("Y-m-d H:i:s",time()),
			"nns_summary"=>$summary,
			"nns_field"=>$field_name
		);
		if(empty($id)||empty($name)||empty($type)){
			return false;
		}
		$fields="";
		$values="";
		foreach ($params as $key=>$val){
			$fields.="{$key},";
			$values.="'{$val}',";
		}
		$fields=trim($fields,',');
		$values=trim($values,',');
		//组织sql
		$sql="insert into nns_label_type({$fields}) values({$values})";
		$ret=nl_execute_by_db($sql, $dc->db());
		return $ret;
	}
	/**
	 * 修改类型信息
	 * @param $dc,
	 * @param string $id:标签类型id
	 * @param array $params:需要修改的字段一维数组array("name"=>标签名称","type"=>标签类型,"weight"=>权重,"summary"=>标签描述)
	 * @return bool
	 */
	static public function modify_label_type($dc,$id,$params=array()){
		if(empty($id)){
			return false;
		}
		$modify_sql	= '';
		foreach($params as $key=>$val){
			$modify_sql.=" nns_{$key}='{$val}',";
		}
		$modify_sql=trim($modify_sql,',');
		$sql="update nns_label_type set {$modify_sql} where nns_id='{$id}'";
		$ret=nl_execute_by_db($sql, $dc->db());
		return $ret;
	}
	/**
	 * 根据标签类型名获取标签类型信息
	 * @param $dc
	 * @param string $name:标签类型名称
	 * @return array():返回一维数组的字段=>值，键值对[没有结果返回TRUE]
	 */
	static public function get_label_type_by_id($dc,$id){
		if(empty($id)){
			return true;
		}
		$sql="select * from nns_label_type where nns_id='{$id}'";
		//echo $sql;
		$ret=nl_query_by_db($sql, $dc->db());
		if(!is_bool($ret)){
			$ret=$ret[0];
		}
		return $ret;
	}

	/**
	 * 获取标签类型列表
	 *
	 * @access public
	 * @param mixed $dc
	 * @param mixed $since LIMIT 第1个参数
	 * @param mixed $length LIMIT 第2个参数
	 * @param string $where 条件查询
	 * @return void
	 */
	static public function get_label_types($dc, $since = null, $length = null,$where=""){
		$limit	= '';
		if (isset($since) && $length)
		{
			$limit	= " LIMIT {$since}, {$length}";
		}
		if(!empty($where)){
			$where=" where {$where}";
		}
		$sql="select * from nns_label_type {$where}{$limit}";
		$ret=nl_query_by_db($sql, $dc->db());
		return $ret;
	}
	/**
	 * 删除标签类型，没有删除关联数据，待完善---by wuha 2013-03-26
	 *
	 * @access public
	 * @param object $dc
	 * @param mixed $id_or_ids 要删除的标签类型ID int or array
	 * @return boolean 删除是否成功
	 */
	static public function del_label_type($dc, $id_or_ids)
	{
		if ($dc -> is_db_open() && !empty($id_or_ids))
		{
			if (is_array($id_or_ids))
			{
				$sql	= 'DELETE FROM `nns_label_type` WHERE `nns_id` in (' . implode(', ', $id_or_ids) . ')';
			}
			else if (intval($id_or_ids))
			{
				$sql 	= "DELETE FROM `nns_label_type` WHERE `nns_id`={$id_or_ids}";
			}
			else
			{
				return;
			}
			return nl_execute_by_db($sql, $dc -> db());
		}
	}
	static public function count_label_types($dc, $where = '')
	{
		if(!empty($where))
		{
			$where	= " WHERE {$where}";
		}
		$sql	= "SELECT COUNT(1) AS `count` FROM `nns_label_type`{$where}";
		$result	= nl_query_by_db($sql, $dc -> db());
		if ($result) return $result[0]['count'];
		else return false;
	}
}
//$dc=nl_get_dc(array("db_policy"=>NL_DB_WRITE,"cache_policy"=>NP_KV_CACHE_TYPE_MEMCACHE));
//$dc->open();
//$r=nl_label_type::add_label_type($dc,"1000","name","year","4","summay");
//////$r=nl_label_type::modify_label_type($dc, "0001", array("weight"=>100));
////$r=nl_label_type::get_label_type_by_id($dc,"0001");
//var_dump($r);
//echo "<pre>";
//print_r($r);