<?php
/**
 * 绑定包列表
 * @author ++ 2013年3月28日15:05:05
 * 
 */
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
//include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'label'.DIRECTORY_SEPARATOR.'asset_item_bo.class.php';
class nl_label_bind_video{
	 const CACHE_TIME_OUT = 300;
    /**
     * 读取bind包列表
     */
    public static function get_bind_list($dc,$policy,$start=0,$size=30,$order=NL_ORDER_NUM_DESC)
    {
    	 
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$cache_key = $start.$size.$order;

				$result = self::_get_bind_list_by_cache($dc->cache(),$cache_key);
				if (!$result) {
					$result = self::_get_bind_list_by_db($dc->db(),$start,$size,$_GET,$order);
					self::_set_bind_list_to_cache($dc->cache(),$cache_key,$result);
				}
			}
			else {
				$result = self::_get_bind_list_by_db($dc->db(),$start,$size,$_GET,$order);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_bind_list_by_db($dc->db(),$start,$size,$_GET,$order);
		}elseif($policy == NL_DC_CACHE){
			$cache_key = $start.$size.$order;
			$result = self::_get_bind_list_by_cache($dc->cache(),$cache_key);
		}

		return $result;
    }
    private static function _get_bind_list_by_by_cache($cache,$cache_key)
    {
    	return unserialize($cache->get('bind_list|#'.self::_get_cache_data_version($cache).$cache_key));
    }
    private static function _get_bind_list_by_db($db,$start=0,$size=30,$_GET)
    {
    	$label_id = $_GET['label_id'];
    	$type_id = $_GET['nns_id'];
    	$video_name = '';
    	$type = $_SESSION["nns_manager_type"];
    	switch($type){
    		    case 0:
    		    $type = 'vod';
    			break;
    			case 1:
    			$type = 'live';
    			break;
    	}
    	 
    	$sql = "select * from nns_video_label_bind_$type_id where nns_label_id=$label_id limit $start,$size ";
    	$r_arr = nl_db_get_all($sql,$db);
    	foreach($r_arr as $k=>$v){
    		 
    		$sql1 = "select nns_name from nns_{$type} where nns_id='{$v['nns_video_id']}'";
    		$video_name = nl_db_get_all($sql1,$db);

    		$r_arr[$k]['nns_video_name'] = isset($video_name[0]['nns_name'])?$video_name[0]['nns_name']:'';
    		
    		
    	}
    	return $r_arr;
    }
    
    private static function _set_bind_list_to_cache($cache,$cache_key,$data)
    {
    	return $cache->set('bind_list|#'.self::_get_cache_data_version($cache).$cache_key,serialize($data),self::CACHE_TIME_OUT);
    }
    public static function count_bind($dc,$policy,$_GET)
    {
    	 
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$cache_key = '';
				$result = self::_get_bind_count_by_cache($dc->cache(),'');
				if (!$result) {
					$result = self::_get_bind_count_by_db($dc->db(),$_GET);
					self::_set_bind_count_to_cache($dc->cache(),$cache_key,$result);
				}
			}
			else {
				$result = self::_get_bind_count_by_db($dc->db(),$_GET);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_bind_count_by_db($dc->db(),$_GET);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_bind_count_by_cache($dc->cache(),'');
		}

		return $result;
    }
    private static function _get_bind_count_by_cache($cache,$cache_key='')
    {
    	return $cache->get('label_bind_video|#'.self::_get_cache_data_version($cache).$cache_key);
    }
    private static function _get_bind_count_by_db($db,$_GET)
    {
    	$label_id = $_GET['label_id'];
    	$type_id = $_GET['nns_id'];
    	$sql = "select count(*) from nns_video_label_bind_$type_id where nns_label_id=$label_id";
    	return nl_db_get_col($sql,$db);
    }
    private static function _set_bind_count_to_cache($cache,$cache_key='',$data)
    {
    	return $cache->set('label_bind_video|#'.self::_get_cache_data_version($cache).$cache_key,$data,self::CACHE_TIME_OUT);
    }
    private static function _get_cache_data_version($cache){
    	$v = $cache->get('label_bind_video');
    	if( ! $v)
    	{
    		$v = self::_set_cache_data_version($cache);
    	}
    	return $v;
    }
    private static function _set_cache_data_version($cache){
    	$guid = np_guid_rand();
    	return $cache->set('label_bind_video',$guid,self::CACHE_TIME_OUT *2);
    }
}	
?>
	
	
	
	