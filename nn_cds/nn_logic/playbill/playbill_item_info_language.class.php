<?php
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
class nl_playbill_item_info_language {
	const CACHE_TIME_OUT=300;
    /*
     * 从DC模块获取节目多语言信息
 	 * get_playbill_item_info_by_language
 	 */
 	
	/**
	 * 从DC模块获取节目多语言信息
	 * @param dc DC模块
	 * @param string 节目ID
	* @param string扩展语种 zh_CN | en_US 
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_AUTO
	 * @return *
	 * 			Array 节目单列表结果
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */


	static public function get_playbill_item_info_by_language($dc,$playbill_id,$language ,$policy=NL_DC_AUTO) {
		$result = null;
		if ($policy == NL_DC_AUTO){
			if ($dc->is_cache_enabled()) {
				$result = self::_get_playbill_item_info_by_language_cache($dc->cache(),$playbill_id,$language);
				if ($result === FALSE ) {
					$result = self::_get_playbill_item_info_by_language_db($dc->db(),$playbill_id,$language);
				self::_set_playbill_item_info_by_language_cache($dc->cache(),$playbill_id,$language,$result);
				}
			}
			else {
				$result = self::_get_playbill_item_info_by_language_db($dc->db(),$playbill_id,$language);
			}
		}elseif($policy == NL_DC_DB){
			$result = self::_get_playbill_item_info_by_language_db($dc->db(),$playbill_id,$language);
		}elseif($policy == NL_DC_CACHE){
			$result = self::_get_playbill_item_info_by_language_cache($dc->cache(),$playbill_id,$language);
		}
		
		return $result;
	}
	
	static private function _get_playbill_item_info_by_language_db($db,$playbill_id,$language) {
		$sql = 'select * ' .
				'from nns_live_playbill_item_language ' .
				'where nns_playbill_id=\''.$playbill_id.'\' ' .
				'and nns_language=\''.$language.'\'';
//		echo $sql;
//		die;
		return nl_query_by_db($sql, $db);
	}
	static private function _get_playbill_item_info_by_language_cache($cache,$playbill_id,$language) {
		if ($cache===NULL) return FALSE;
		return unserialize($cache->get('playbill_ex_info|#'.$playbill_id.'|#'.$language));
	}
	static private function _set_playbill_item_info_by_language_cache($cache,$playbill_id,$language,$data) {
		if ($cache===NULL) return FALSE;
		return $cache->set('playbill_ex_info|#'.$playbill_id.'|#'.$language,serialize($data),self::CACHE_TIME_OUT);
	}

	static public function delete_playbill_item_info_by_language_cache($dc,$playbill_id,$language) {
		if ($dc->cache()===NULL) return FALSE;
		return $dc->cache()->delete('playbill_ex_info|#'.$playbill_id.'|#'.$language);
	}
	
	
	
    /* * 根据节目ID批量查询对应语言的信息
 	 * get_live_info_by_language_ids
 	 */
 	
	/**
	 * 根据节目ID批量查询对应语言的信息
	 * @param dc DC模块
     * @param array 节目ID数组
	 * @param string 	信息语言类型。zh_CN|en_US 仅为其中一个
						同全局配置文件get_g_extend_language方法返回的数据。
	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE 
					默认为NL_DC_AUTO
	 * @return *
	 *			ARRAY（
					“节目ID”=>array 节目内容
					“节目ID”=>array 节目内容
					） 查询成功
	 * 			FALSE 查询失败
	 * 			TRUE  操作成功，但无数据
	 */

	static public function get_playbill_item_info_by_language_ids($dc,$ids, $language,$policy=NL_DC_AUTO) {
		$array=array();
		foreach ($ids as $item_id){
			$result = self::get_playbill_item_info_by_language($dc,$item_id,$language,$policy);
			if ($result===FALSE) return FALSE;
			if ($result!==TRUE){
				$array[$item_id]=$result;
			}
		}
		
		if (count($array)==0) return TRUE;
		
		return $array;
	}
	
	/* *modify_playbill_item_info_by_language 
	根据节目ID修改扩展语言信息
	*/
/**
	 * 从DC模块修改节目多语言信息
	 * 若不存在则添加 存在则修改
	 * @param dc DC模块
	 * @param string 节目ID
	* @param string扩展语种 zh_CN | en_US  
	 * @param Array 所需参数组
			Array(
			“name”=>节目名称，
			“summary”=>节目简介，
)

	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 失败
	 * 			TRUE  成功
	 */
 static public function modify_playbill_item_info_by_language ($dc,$id,$language,$params,$policy=NL_DC_DB){
 	$result=self::get_playbill_item_info_by_language($dc,$id,$language,$policy);
 	if ($result===TRUE){
 		$result=self::_add_playbill_item_info_by_language_db($dc,$id,$language,$params,$policy);
 	}else{
 		$result=self::_modify_playbill_item_info_by_language_db($dc,$id,$language,$params,$policy);
 	}
 	return $result;
 }	
	
	
	/* *_add_playbill_item_info_by_language_db 
	根据节目ID添加扩展语言信息
	*/
/**
	 * 从DC模块添加节目多语言信息
	 * @param dc DC模块
	 * @param string 节目ID
	* @param string扩展语种 zh_CN | en_US  
	 * @param Array 所需参数组
			Array(
			“name”=>节目名称，
			“summary”=>节目简介，
)

	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 失败
	 * 			TRUE  成功
	 */
 static private function _add_playbill_item_info_by_language_db ($dc,$id,$language,$params,$policy=NL_DC_DB){
 	$sql = 'insert into  nns_live_playbill_item_language (' .
				'nns_name, ' .
				'nns_summary,' .
				'nns_playbill_id,' .
				'nns_language' .
				') values (' .
				'\''.$params['name'].'\',' .
				'\''.$params['summary'].'\',' .
				'\''.$id.'\',' .
				'\''.$language.'\'' .
				') ' ;
//				echo $sql;
//				die;
		return nl_execute_by_db($sql, $dc->db());
 }
 
 	/* *_modify_playbill_item_info_by_language_db 
	根据节目ID添加扩展语言信息
	*/
/**
	 * 从DC模块添加节目多语言信息
	 * @param dc DC模块
	 * @param string 节目ID
	* @param string扩展语种 zh_CN | en_US  
	 * @param Array 所需参数组
			Array(
			“name”=>节目名称，
			“summary”=>节目简介，
)

	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 失败
	 * 			TRUE  成功
	 */
 static private function _modify_playbill_item_info_by_language_db ($dc,$id,$language,$params,$policy=NL_DC_DB){
 	$sql = 'update  nns_live_playbill_item_language set ' .
				'nns_name=\''.$params['name'].'\', ' .
				'nns_summary=\''.$params['summary'].'\' ' .
				'where nns_playbill_id=\''.$id.'\' ' .
				'and nns_language=\''.$language.'\'' ;
				
				
		return nl_execute_by_db($sql, $dc->db());
 }
 
  	/* *delete_playbill_item_info_by_language 
	根据节目ID删除扩展语言信息
	*/
/**
	 * 从DC模块删除节目多语言信息
	 * @param dc DC模块
	 * @param string 节目ID
)

	 * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
					默认为NL_DC_DB
	 * @return *
	 * 			FALSE 失败
	 * 			TRUE  成功
	 */
 static public function delete_playbill_item_info_by_language ($dc,$id,$policy=NL_DC_DB){
 	$sql = 'delete from nns_live_playbill_item_language  ' .
				'where nns_playbill_id=\''.$id.'\' ' ;
				
		return nl_execute_by_db($sql, $dc->db());
 }
 
 
   /* *epg_search_playbill_item_info_by_language 
	根据节目ID删除扩展语言信息
	*/
/**
	 * 前端从DC模块获取信息包内容列表
		只读取前端列表需要的字段。
	 * @param dc DC模块
	 * @param string 直播ID 为空为忽略条件
	 * @param int 起始查询记录位置
	 * @param int 查询记录条数
* @param array 查询参数组
			Array(
				“begin_time”=>起始时间 ， 
				“end_time”=>结束时间，
				“name”=>节目名称，LIKE查询 ,
				“timezone”=>时区，不传为当前时区
)

	 * @param string扩展语种 zh_CN | en_US 
	 
	*@param string 排序规则， NL_ORDER_TIME_ASC | NL_ORDER_TIME_DESC 
				默认为 NL_ORDER_TIME_ DESC
	
				NL_ORDER_TIME_ASC 按创建时间升序排列
				NL_ORDER_TIME_ DESC 按创建时间降序排列

	 * @return *
	 * 			Array 信息包内容列表结果
					
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
*/
 static public function epg_search_playbill_item_info_by_language ($dc,$live_id,$since,$num,$params,$language,$search_type='pinyin_firstchar',$order=NL_ORDER_TIME_DESC ){
 	$timezone=$params['timezone'];
 	if (!empty($timezone) || $timezone==0){
 		if (!empty($begin_time)){
 			$params['begin_time']=date('Y-m-d H:i:s',strtotime($begin_time.'+0000') - $timezone*3600);
 		}
		if (!empty($end_time)){	
			$params['end_time']=date('Y-m-d H:i:s',strtotime($end_time.'+0000') - $timezone*3600);
		}
	}else{
		if (!empty($begin_time)){
			$params['begin_time']=date('Y-m-d H:i:s',strtotime($begin_time));
		}
		if (!empty($end_time)){
			$params['end_time']=date('Y-m-d H:i:s',strtotime($end_time));
		}
	}
 	
 	$sql = 'select ' .
 			'a.nns_begin_time,' .
 			'a.nns_id,' .
 			'a.nns_image,' .
 			'a.nns_time_len,' .
 			'a.nns_live_id,' .
 			'b.nns_name,' .
 			'b.nns_summary ' .
 			'from nns_live_playbill_item as a ' .
 			'left join nns_live_playbill_item_language as b ' .
 			'on a.nns_id=b.nns_playbill_id ' .
 			'where ' .
 			'a.nns_state=\'0\'  and ';
 			
 			if (!empty($live_id)){
 				$sql.='a.nns_live_id=\''.$live_id.'\' ' ;
 			}
 			
 			
 				$sql_where='';
		
		$search_types=explode('|',$search_type);
		
		$search_sql='';
		foreach ($search_types as $type){
			
			switch ($type){
				case 'name_firstchar':
					if (!empty($params['name']))
					$search_sql.=" b.nns_name like '".$params['name']."%' or";
				break;
				case 'name_likechar':
					if (!empty($params['name']))
					$search_sql.=" b.nns_name like '%".$params['name']."%' or";
				break;
				case 'pinyin_likechar':
				if (!empty($params['pinyin']))
					$search_sql.=" a.nns_pinyin like '%".$params['pinyin']."%' or";
				break;
				case 'en_firstchar':
				if (!empty($params['en_name']))
					$search_sql.=" a.nns_eng_name like '".$params['en_name']."%' or";
				break;
				case 'en_likechar':
				if (!empty($params['en_name']))
					$search_sql.=" a.nns_eng_name like '%".$params['en_name']."%' or";
				break;
				case 'pinyin_firstchar':
					if (!empty($params['pinyin']))
					$search_sql.=" a.nns_pinyin like '".$params['pinyin']."%' or";
				break;
				default:
//					$search_sql.=' ';
				break;
			}

		}
		$search_sql=rtrim($search_sql,'or');
		$search_sql=trim($search_sql);
		if (!empty($search_sql)) $search_sql='('.$search_sql.')';
		$sql_where.= $search_sql;
 			
 			
 			foreach ($params as $key=>$value){
					if ($key=='begin_time' && !empty($value)){
						$sql_where.=' and a.nns_begin_time >= \''.$value.'\'';
						continue;
					}
					if ($key=='end_time' && !empty($value)){
						$sql_where.=' and a.nns_begin_time <= \''.$value.'\'';
						continue;
					}
//					if ($key=='name' && !empty($value)){
//						$sql_where.=' and b.nns_name like \''.$value.'\'';
//						continue;
//					}
//					if ($key=='pinyin' && !empty($value)){
//						$sql_where.=' and b.nns_pinyin like \''.$value.'\'';
//						continue;
//					}
//					if ($key=='en_name' && !empty($value)){
//						$sql_where.=' and b.nns_eng_name like \''.$value.'\'';
//						continue;
//					}
				}
			$sql.=$sql_where;
			
			switch ($order){
				case NL_ORDER_TIME_ASC:
					$sql.=' order by nns_begin_time asc ';
				break;
				default:
					$sql.=' order by nns_begin_time desc ';
				break;
			}
			
			$sql.='limit '.$since.','.$num;
			
			
			$result=nl_query_by_db($sql, $dc->db());
			//		当前时区差
		$time_offset=strtotime(date('Y-m-d').'+0000')-strtotime(date('Y-m-d'));
		if (is_array($result) && (!empty($timezone) || $timezone==0)){
			foreach ($result as $key=>$item){
				$item['nns_begin_time']=date('Y-m-d H:i:s',strtotime($item['nns_begin_time'])-$time_offset+$timezone*3600);
				$result[$key]=$item;
			}
		}
				
		return $result;
 }

   /* *count_search_playbill_item_info_by_language 
	根据节目ID删除扩展语言信息条数
	*/
/**
	 * 前端从DC模块获取信息包内容列表条数
	 * @param dc DC模块
	 * @param string 直播ID
* @param array 查询参数组
			Array(
				“begin_time”=>起始时间 ， 
				“end_time”=>结束时间，
				“name”=>节目名称，LIKE查询 ,
				“timezone”=>时区，不传为当前时区,
				"pinyin"=>拼音首字查询
				"en_name"=>英文首字查询
)

	 * @param string扩展语种 zh_CN | en_US 
	 


	 * @return int 记录条数
*/
 static public function count_search_playbill_item_info_by_language ($dc,$live_id,$params,$language,$search_type='pinyin_firstchar' ){
 	$timezone=$params['timezone'];
 	if (!empty($timezone) || $timezone==0){
 		if (!empty($begin_time)){
 			$params['begin_time']=date('Y-m-d H:i:s',strtotime($begin_time.'+0000') - $timezone*3600);
 		}
		if (!empty($end_time)){	
			$params['end_time']=date('Y-m-d H:i:s',strtotime($end_time.'+0000') - $timezone*3600);
		}
	}else{
		if (!empty($begin_time)){
			$params['begin_time']=date('Y-m-d H:i:s',strtotime($begin_time));
		}
		if (!empty($end_time)){
			$params['end_time']=date('Y-m-d H:i:s',strtotime($end_time));
		}
	}
 	
 	$sql = 'select count (1) as num ' .
 			'from nns_live_playbill_item as a ' .
 			'left join nns_live_playbill_item_language as b ' .
 			'on a.nns_id=b.nns_playbill_id ' .
 			'where ' .
 			'a.nns_state=\'0\'  and ';
 			
 			if (!empty($live_id)){
 				$sql.='a.nns_live_id=\''.$live_id.'\' ' ;
 			}
 			
 			
 			$sql_where='';
		
		$search_types=explode('|',$search_type);
		
		$search_sql='';
		foreach ($search_types as $type){
			
			switch ($type){
				case 'name_firstchar':
					if (!empty($params['name']))
					$search_sql.=" b.nns_name like '".$params['name']."%' or";
				break;
				case 'name_likechar':
					if (!empty($params['name']))
					$search_sql.=" b.nns_name like '%".$params['name']."%' or";
				break;
				case 'pinyin_likechar':
				if (!empty($params['pinyin']))
					$search_sql.=" a.nns_pinyin like '%".$params['pinyin']."%' or";
				break;
				case 'en_firstchar':
				if (!empty($params['en_name']))
					$search_sql.=" a.nns_eng_name like '".$params['en_name']."%' or";
				break;
				case 'en_likechar':
				if (!empty($params['en_name']))
					$search_sql.=" a.nns_eng_name like '%".$params['en_name']."%' or";
				break;
				case 'pinyin_firstchar':
					if (!empty($params['pinyin']))
					$search_sql.=" a.nns_pinyin like '".$params['pinyin']."%' or";
				break;
				default:
//					$search_sql.=' ';
				break;
			}

		}
		$search_sql=rtrim($search_sql,'or');
		$search_sql=trim($search_sql);
		if (!empty($search_sql)) $search_sql='('.$search_sql.')';
		$sql_where.= $search_sql;
 			
 			
 			foreach ($params as $key=>$value){
					if ($key=='begin_time' && !empty($value)){
						$sql_where.=' and a.nns_begin_time >= \''.$value.'\'';
						continue;
					}
					if ($key=='end_time' && !empty($value)){
						$sql_where.=' and a.nns_begin_time <= \''.$value.'\'';
						continue;
					}
//					if ($key=='name' && !empty($value)){
//						$sql_where.=' and b.nns_name like \''.$value.'\'';
//						continue;
//					}
//					if ($key=='pinyin' && !empty($value)){
//						$sql_where.=' and b.nns_pinyin like \''.$value.'\'';
//						continue;
//					}
//					if ($key=='en_name' && !empty($value)){
//						$sql_where.=' and b.nns_eng_name like \''.$value.'\'';
//						continue;
//					}
				}
			$sql.=$sql_where;
			
			$result=nl_query_by_db($sql, $dc->db());
				
		return $result[0]['num'];
 }

}
?>