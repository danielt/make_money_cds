<?php

include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';
include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'label' . DIRECTORY_SEPARATOR . 'video_label_bo.class.php';
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_playbill_item extends nl_public
{

        const CACHE_TIME_OUT = 300;

        static $base_table='nns_live_playbill_item';
        
        static $arr_filed = array(
            'nns_id',
            'nns_name',
            'nns_begin_time',
            'nns_begin_data',
            'nns_end_time',
            'nns_time_len',
            'nns_live_id',
            'nns_create_time',
            'nns_state',
            'nns_summary',
            'nns_image1',
            'nns_image2',
            'nns_image3',
            'nns_image4',
            'nns_image5',
            'nns_image0',
            'nns_pinyin',
            'nns_pinyin_length',
            'nns_eng_name',
            'nns_keyword',
            'nns_kind',
            'nns_image6',
            'nns_actor',
            'nns_director',
            'nns_live_media_id',
            'nns_playbill_import_id',
            'nns_domain',
            'nns_hot_dgree',
            'nns_cp_id',
            'nns_integer_id',
            'nns_modify_time',
            'nns_import_source',
	        'nns_ext_url',
            'nns_deleted',
            'nns_c2_is_deleted',
        );
        /*
         * *********************************************
         * count_playbill_item_list
         * 从DC模块获取节目单列表数量
         * ********************************************
         */

        /**
         * 从DC模块获取节目单列表数量
         * @param dc DC模块
         * @param string 直播ID
         * @param string 起始日期 格式为20121129T133900
         * @param string 结束日期 格式为2012113T0000000
         * @param string 是否有效 NULL 为忽略  0为有效 1为无效


         * @param int 时区 8  默认为系统时区
         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
          默认为NL_DC_AUTO
         * @return int 记录条数
         */
        static public function count_playbill_item_list($dc, $live_id, $begin_time, $end_time, $state, $timezone = NULL) {
                $result = null;
                if (!empty($timezone) || ($timezone != '' && $timezone == 0)) {
                        $begin_time = date('Y-m-d H:i:s', strtotime($begin_time . '+0000') - $timezone * 3600);
                        $end_time = date('Y-m-d H:i:s', strtotime($end_time . '+0000') - $timezone * 3600);
                } else {
                        $begin_time = date('Y-m-d H:i:s', strtotime($begin_time));
                        $end_time = date('Y-m-d H:i:s', strtotime($end_time));
                }

                $sql = 'select count(1) as num from nns_live_playbill_item where ' .
                        'nns_live_id=\'' . $live_id . '\' and ' .
                        'nns_begin_time>=\'' . $begin_time . '\' and ' .
                        'nns_begin_time<=\'' . $end_time . '\' ';
                if ($state !== NULL) {
                        $sql.='and nns_state=\'' . $state . '\' ';
                }


                $result = nl_query_by_db($sql, $dc->db());
                return $result[0]['num'];
        }

        
        /**
         * 查询单个列表
         * @param object $dc 数据库对象
         * @param string $nns_id guid
         * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
         * @author liangpan
         * @date 2016-09-24
         */
        static public function query_by_id($dc,$nns_id)
        {
            $sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
            $result = nl_query_by_db($sql, $dc->db());
            if(!$result)
            {
                return self::return_data(1,'数据库查询失败,sql:'.$sql);
            }
            $result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
            return self::return_data(0,'OK',$result);
        }
        
        
        
        /**
         * 查询单个列表
         * @param object $dc 数据库对象
         * @param string $nns_id guid
         * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
         * @author liangpan
         * @date 2016-09-24
         */
        static public function query_by_liveid_and_time($dc,$nns_live_id,$date)
        {
            $sql = "select *,left(nns_begin_time,10) as bk_date from " . self::$base_table . " where nns_live_id='{$nns_live_id}' and nns_begin_time>='{$date}' and nns_state='0' order by nns_begin_time asc";
            $result = nl_query_by_db($sql, $dc->db());
            if(!$result)
            {
                return self::return_data(1,'数据库查询失败,sql:'.$sql);
            }
            return self::return_data(0,'OK',$result);
        }
        /*
         * *********************************************
         * get_playbill_item_list
         * 从DC模块获取节目单列表
         * ********************************************
         */

        /**
         * 从DC模块获取节目单列表
         * @param dc DC模块
         * @param string 直播ID
         * @param string 起始日期 格式为20121129T133900
         * @param string 结束日期 格式为2012113T0000000
         * @param string 是否有效 NULL 为忽略  0为有效 1为无效
         * @param int 起始查询记录位置
         * @param int 查询记录条数


         * @param int 时区 8  默认为系统时区
         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
          默认为NL_DC_AUTO
         * @return *
         * 			Array 节目单列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function get_playbill_item_list($dc, $live_id, $begin_time, $end_time, $state, $since, $num, $timezone = NULL, $params = NULL, $policy = NL_DC_AUTO) {
                $result = null;
                $num = (int) $num;
                if (!empty($timezone) || ($timezone == 0 && $timezone != '')) {
                        $begin_time = date('Y-m-d H:i:s', strtotime($begin_time . '+0000') - $timezone * 3600);
                        $end_time = date('Y-m-d H:i:s', strtotime($end_time . '+0000') - $timezone * 3600);
                } else {
                        $begin_time = date('Y-m-d H:i:s', strtotime($begin_time));
                        $end_time = date('Y-m-d H:i:s', strtotime($end_time));
                }

                if ($policy == NL_DC_AUTO) {
                        if ($dc->is_cache_enabled()) {
                                $result = self::_get_playbill_item_list_by_cache($dc->cache(), $live_id, $begin_time, $end_time, $state, $since, $num);
                                if ($result === FALSE) {
                                        $result = self::_get_playbill_item_list_by_db($dc->db(), $live_id, $begin_time, $end_time, $state, $since, $num);
                                        self::_set_playbill_item_list_by_cache($dc->cache(), $live_id, $begin_time, $end_time, $state, $since, $num, $result);
                                }
                        } else {
                                $result = self::_get_playbill_item_list_by_db($dc->db(), $live_id, $begin_time, $end_time, $state, $since, $num);
                        }
                } elseif ($policy == NL_DC_DB) {
                        $result = self::_get_playbill_item_list_by_db($dc->db(), $live_id, $begin_time, $end_time, $state, $since, $num);
                } elseif ($policy == NL_DC_CACHE) {
                        $result = self::_get_playbill_item_list_by_cache($dc->cache(), $live_id, $begin_time, $end_time, $state, $since, $num);
                }
//		当前时区差
                $time_offset = strtotime(date('Y-m-d') . '+0000') - strtotime(date('Y-m-d'));
                if (is_array($result) && (!empty($timezone) || ($timezone != '' && $timezone == 0))) {
                        foreach ($result as $key => $item) {
                                $item['nns_begin_time'] = date('Y-m-d H:i:s', strtotime($item['nns_begin_time']) - $time_offset + $timezone * 3600);
                                $result[$key] = $item;
                        }
                }
//		var_dump($result);die;
                return $result;
        }

        static private function _get_playbill_item_list_by_db($db, $live_id, $begin_time, $end_time, $state, $since, $num) {
                //yxt2013年2月25日10:56:53，去掉了'nns_begin_time<=\''.$end_time.'\' ' ;中的”=“，不然第一天的会把第二天的第一个节目查出来
                $sql = 'select * from nns_live_playbill_item where ' .
                        'nns_live_id=\'' . $live_id . '\' and ' .
                        'nns_begin_time>=\'' . $begin_time . '\' and ' .
                        'nns_begin_time<\'' . $end_time . '\' ';
                if ($state !== NULL) {
                        $sql.='and nns_state=\'' . $state . '\' ';
                }
                $sql.='order by nns_begin_time asc ';
                if (!empty($num)) {
                        $sql.='limit ' . $since . ',' . $num;
                }

//		echo $sql;die;
                return nl_query_by_db($sql, $db);
        }

        static private function _get_playbill_item_list_by_cache($cache, $live_id, $begin_time, $end_time, $state, $since, $num) {
                if ($cache === NULL)
                        return FALSE;
//		获取GUID标识
                $comp_key = $cache->get('index_playbill|#' . $live_id);

                return unserialize($cache->get($comp_key . '|#' . $begin_time . '|#' . $end_time . '|#' . $state . '|#' . $since . '|#' . $num));
        }

        static private function _set_playbill_item_list_by_cache($cache, $live_id, $begin_time, $end_time, $state, $since, $num, $data) {
                if ($cache === NULL)
                        return FALSE;

                $comp_key = $cache->get('index_playbill|#' . $live_id);
                $comp_key = ($comp_key === FALSE) ? np_guid_rand() : $comp_key;
                $cache->set('index_playbill|#' . $live_id, $comp_key);
                return $cache->set($comp_key . '|#' . $begin_time . '|#' . $end_time . '|#' . $state . '|#' . $since . '|#' . $num, serialize($data), self::CACHE_TIME_OUT);
        }

        static public function delete_playbill_item_list_by_cache($dc, $live_id) {
                if ($dc->cache() === NULL)
                        return FALSE;
                return $dc->cache()->delete('index_playbill|#' . $live_id);
        }

        static public function get_cache_key($dc, $live_id) {
                if ($dc->cache() === NULL)
                        return FALSE;
                return $dc->cache()->get('index_playbill|#' . $live_id);
        }

        /*
         * *********************************************
         * add_playbill_item
         * 从DC模块添加节目单
         * ********************************************
         */

        /**
         * 从DC模块添加节目单
         * @param dc DC模块
         * @param string 直播ID
         * @param Array 所需参数组
          Array(
          "id"=>节目ID ，若含该参数，则用该参数创建记录，否则自动生成GUID
          “name”=>节目名称，
          “begin_time”=>节目播出时间，
          “time_len”=>节目时长，
          “summary”=>节目简介，
          "kind"=>节目种类,
          "keyword"=>节目关键字
          “image”=>array(
          0=>节目剧照1
          1=>节目剧照2
          2=>节目剧照3
          3=>节目剧照3
          }
          “pinyin”=>拼音
          “eng_name”=>英文
          )

         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
          默认为NL_DC_AUTO
         * @return *
         * 			FALSE 失败
         * 			TRUE  成功
         */
        static public function add_playbill_item($dc, $live_id, $params, $policy = NL_DC_DB) {
        	
                $params['id'] = empty($params['id']) ? np_guid_rand() : $params['id'];
                $result = self::_add_playbill_item_by_db($dc->db(), $live_id, $params);
                 nl_write_error('-------------------add_playbill_item--------------------');
/*
                if ($result == true) {
                        $labels = nl_video_label_bind_content::get_label_by_video_id($dc, $live_id, 1);
                        if (is_array($labels)) {
                                foreach ($labels as $label) {
                                        nl_video_label_bind_content::add_video_label_content(
                                                $dc, $params['id'], 2, $label['nns_label_id']
                                        );
                                }
                        }

                        $label_arr = array();

                        $label_arr['kind'] = $params['kind'];

                        foreach ($label_arr as $label_key => $label_item) {
                                nl_video_label_bo::bind_video_label_by_video(
                                        $dc, $params['id'], 2, array(
                                    'type_id' => $label_key,
                                    'name' => $label_item,
                                        )
                                );
                        }
                        //self::delete_playbill_item_list_by_cache($dc, $live_id);
                }
*/
                return $result;
        }

        static private function _add_playbill_item_by_db($db, $live_id, $params) {

                $sql = 'select count(1) as num from nns_live_playbill_item where ' .
                        'nns_name=\'' . $params['name'] . '\' and ' .
                        'nns_live_id=\'' . $live_id . '\' and ' .
                        'nns_begin_time=\'' . $params['begin_time'] . '\' ';

                //error_log($sql."\n",'3','E:\test.txt');
                $count = nl_query_by_db($sql, $db);

                if ($count[0]['num'] > 0) {
                        return TRUE;
                }
                $params['name'] = str_replace('<', '(', $params['name']);
                $params['name'] = str_replace('>', ')', $params['name']);

               $image_key_str = '';
               $image_value_str = '';
                if (isset($params['image']) && is_array($params['image'])) {
                        foreach ($params['image'] as $key => $image) {
                                if (!empty($image)) {
                                        $image_key_str.='nns_image' . $key . ',';
                                        $image_value_str.='\'' . $image . '\',';
                                }
                        }
                }
               $summary = isset($params['summary'])?$params['summary']:'';
               $keyword = isset($params['keyword'])?$params['keyword']:'';
               $kind = isset($params['kind'])?$params['kind']:'';
               $eng_name = isset($params['eng_name'])?$params['eng_name']:'';
               $pinyin = isset($params['pinyin'])?$params['pinyin']:'';
               $str_date_time = date('Y-m-d H:i:s');
                $sql = 'insert into nns_live_playbill_item ' .
                        '(' .
                        'nns_id,' .
                        'nns_live_id,' .
                        'nns_name,' .
                        'nns_begin_time,' .
                        'nns_time_len,' .
                        'nns_summary,' .
                        'nns_kind,' .
                        'nns_keyword,' .
                        $image_key_str .
                        'nns_pinyin,' .
                        'nns_pinyin_length,'.
                        'nns_eng_name,' .
                        'nns_create_time,' .     
                        'nns_modify_time,' .
                        'nns_live_media_id,' .
                        'nns_cp_id' .
                        ') values (' .
                        '\'' . $params['id'] . '\',' .
                        '\'' . $live_id . '\',' .
                        '\'' . $params['name'] . '\',' .
                        '\'' . $params['begin_time'] . '\',' .
                        '\'' . $params['time_len'] . '\',' .
                        '\'' . $summary. '\',' .
                        '\'' . $kind . '\',' .
                        '\'' . $keyword . '\',' .
                        $image_value_str .
                        '\'' . $pinyin . '\',' .
                         '\'' . strlen($pinyin) . '\',' .
                        '\'' . $eng_name. '\',' .
                        '\'' . $str_date_time. '\',' .
                        '\'' . $str_date_time. '\',' .
                        '\'' . $params['media_id']. '\',' .
                        '\'' . $params['cp_id']. '\'' .
                        ') ';
//		echo $sql;
//		die;

//error_log($sql."\n",'3','E:\test.txt');
                $re =  nl_execute_by_db($sql, $db);
				
				return $re;
        }

        /**
         * 根据ID获取节目信息
         * @param DC模块
         * @param String 节目ID
         * @param 策略 默认NL_DC_AUTO
         * @return TRUE 未找到
         * 			FALSE 查询失败
         * 			ARRAY 节目信息
         */
        static public function get_playbill_item_info($dc, $id, $timezone = NULL, $policy = NL_DC_AUTO) {
                if ($policy == NL_DC_AUTO) {
                        if ($dc->is_cache_enabled()) {
                                $result = self::_get_playbill_item_list_by_cache($dc->cache(), $id);
                                if ($result === FALSE) {
                                        $result = self::_get_playbill_item_info_by_db($dc->db(), $id);
                                        self::_set_playbill_item_list_by_cache($dc->cache(), $id, $result);
                                }
                        } else {
                                $result = self::_get_playbill_item_info_by_db($dc->db(), $id);
                        }
                } elseif ($policy == NL_DC_DB) {
                        $result = self::_get_playbill_item_info_by_db($dc->db(), $id);
                } elseif ($policy == NL_DC_CACHE) {
                        $result = self::_get_playbill_item_list_by_cache($dc->cache(), $id);
                }

                /* 当前时区差 后台减去时区会 把时间设置为前一天。所以注视掉 2012.04.16 LMS
                  $time_offset=strtotime(date('Y-m-d').'+0000')-strtotime(date('Y-m-d'));
                  if (is_array($result) && is_numeric($timezone)){
                  foreach ($result as $key=>$item){
                  $item['nns_begin_time']=date('Y-m-d H:i:s',strtotime($item['nns_begin_time'])-$time_offset+$timezone*3600);
                  $result[$key]=$item;
                  }
                  }
                 * 
                 */

                return $result;
        }

        static private function _get_playbill_item_info_by_db($db, $id) {
                $sql = 'select * from nns_live_playbill_item where nns_id=\'' . $id . '\'';

                $result = nl_query_by_db($sql, $db);
//	 	var_dump($result);die;
                return $result;
        }

        static private function _get_playbill_item_info_by_cache($cache, $id) {
                if ($cache === NULL)
                        return FALSE;
                return unserialize($cache->get('playbill_item|#' . $id));
        }

        static private function _set_playbill_item_info_by_cache($cache, $id, $data) {
                if ($cache === NULL)
                        return FALSE;
                return $cache->set('playbill_item|#' . $id, serialize($data));
        }

        static public function delete_playbill_item_info_by_cache($dc, $id) {
                if ($dc->cache() === NULL)
                        return FALSE;
                return $dc->cache()->delete('playbill_item|#' . $id);
        }

        /**
         * 根据ID组获取节目单
         * @param DC模块
         * @param 节目ID组
         * @return array(
         * 	id1=>节目内容
         * 	id2=>节目内容
         * ) 
         */
        static public function get_playbill_item_info_by_ids($dc, $ids) {
                $ids_str = implode('","', $ids);
                $ids_str = '"' . $ids_str . '"';
                $sql = 'select * from nns_live_playbill_item where nns_id in (' . $ids_str . ')';
                $result = nl_query_by_db($sql, $dc->db());

                if (is_array($result)) {
                        $result = np_array_rekey($result, 'nns_id');
                }

                return $result;
        }

        /*
         * *********************************************
         * modify_playbill_item
         * 从DC模块修改节目单
         * ********************************************
         */

        /**
         * 从DC模块修改节目单
         * @param dc DC模块
         * @param string 节目ID
         * @param Array 所需参数组
          Array(
          “name”=>节目名称，
          “begin_time”=>节目播出时间，
          “time_len”=>节目时长，
          “summary”=>节目简介，
          “image”=>array(
          0=>节目剧照1
          1=>节目剧照2
          2=>节目剧照3
          3=>节目剧照3
          }
          “pinyin”=>拼音
          “eng_name”=>英文
          )

         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
          默认为NL_DC_AUTO
         * @return *
         * 			Array 节目单列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function modify_playbill_item($dc, $id, $params, $policy = NL_DC_DB) {
                $result = self::_modify_playbill_item_by_db($dc->db(), $id, $params);

                $info = self::get_playbill_item_info($dc, $id);
                if ($result === TRUE) {
                        nl_video_label_bind_content::del_video_label_content($dc, $id, 2);
                        $label_arr = array();

                        $label_arr['kind'] = $params['kind'];

                        foreach ($label_arr as $label_key => $label_item) {
                                nl_video_label_bo::bind_video_label_by_video(
                                        $dc, $params['id'], 2, array(
                                    'type_id' => $label_key,
                                    'name' => $label_item,
                                        )
                                );
                        }
                        self::delete_playbill_item_list_by_cache($dc, $info[0]['nns_live_id']);
                        self::delete_playbill_item_info_by_cache($dc, $id);
                }
                return $result;
        }

        static private function _modify_playbill_item_by_db($db, $id, $params) {
                $sql = 'update  nns_live_playbill_item set ';
                $query = '';

                foreach ($params as $key => $value) {
                        if ($key == 'image') {
                                if (is_array($params['image'])) {
                                        foreach ($value as $image_key => $image) {
                                                if (!empty($image)) {
                                                        $image_update_str.='nns_image' . $image_key . '=\'' . $image . '\',';
                                                }
                                        }
                                }
                                $query.=$image_update_str;
                        } else {
                                $query.='nns_' . $key . '=\'' . $value . '\',';
                        }
                }
                $date_time = date("Y-m-d H:i:s");
                $query.='nns_modify_time=\'' . $date_time . '\',';
                $query = rtrim($query, ',');
                $sql.=$query . ' where nns_id=\'' . $id . '\'';

                $re =  nl_execute_by_db($sql, $db);
// 				if($re == true){
// 					$g_inject = g_cms_config::get_g_config_value('g_inject_enabled');
// 							if($g_inject){
// 								//获取对应的栏目ID
// 								$depot = category_model::__get_category_all($id,'playbill');
// 								$depot_info = $depot['depot_info'];//获取映射之后的栏目ID已经SPid
// 								$category_all = category_model::get_category_id_by_content($depot_info,1);
// 								//print_r($category_all);
// 								//die;
// 								if(is_array($category_all)&&count($category_all)>0){
// 									//foreach ($category_all as  $value) {
// 										category_content_model::_task_c2($category_all,$id,'playbill');//将数据添加到任务表
// 									//}
// 								}
// 							}
// 				}
				return $re;
        }

        /*
         * *********************************************
         * delete_playbill_item
         * 从DC模块删除节目单
         * ********************************************
         */

        /**
         * 从DC模块删除节目
         * @param dc DC模块
         * @param string 节目ID
         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
          默认为NL_DC_AUTO
         * @return *
         * 			Array 节目单列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function delete_playbill_item($dc, $id, $policy = NL_DC_DB) {
                $result = self::_delete_playbill_item_by_db($dc->db(), $id);
                $info = self::get_playbill_item_info($dc, $id);
                if ($result === TRUE) {
                        nl_video_label_bind_content::del_video_label_content($dc, $id, 2);
                        self::delete_playbill_item_list_by_cache($dc, $info[0]['nns_live_id']);
                        self::delete_playbill_item_info_by_cache($dc, $id);
                }
                return $result;
        }

        static private function _delete_playbill_item_by_db($db, $id) {
                $sql = 'delete from  nns_live_playbill_item ' .
                        'where nns_id=\'' . $id . '\'';

                return nl_execute_by_db($sql, $db);
        }

        /*
         * *********************************************
         * count_search_playbill_item_list
         * 从DC搜索节目单内容列表数量
         * ********************************************
         */

        /**
         * 从DC模块查询节目单内容列表
         * @param dc DC模块
         * @param string 直播ID
         * @param array 查询参数组
          Array(
          “begin_time”=>起始时间 ，
          “end_time”=>结束时间，
          “name”=>节目名称，LIKE查询 ,
          "pinyin"=>拼音
          "en_name"=>英文名
          “state”=>节目状态  NULL 为忽略  0为有效 1为无效
          "except"=>array(
          'packet_type'=>被排除的节目单所属包类型
          'packet_id'=>被排除的节目单包ID
          'category_id'=>被排除的节目单包栏目ID
          )
          )


         * @return int 记录条数
         */
        static public function count_search_playbill_item_list($dc, $live_id, $params, $search_type = 'pinyin_firstchar') {
                $sql = 'select count(1) as num from  nns_live_playbill_item where ';
                if (!empty($live_id)) {
                        $sql.='nns_live_id=\'' . $live_id . '\' and'; //yxt，2013年3月7日16:51:23这里的这个and是我加的。
                } else {
                        $sql .= ' 1 and ';
                }
                $sql_where = '';
//		echo $search_type;
                $search_types = explode('|', $search_type);

                $search_sql = '';
                foreach ($search_types as $type) {

                        switch ($type) {
                                case 'name_firstchar':
                                        if (!empty($params['name']))
                                                $search_sql.=" nns_name like '" . $params['name'] . "%' or";
                                        break;
                                case 'name_likechar':
                                        if (!empty($params['name']))
                                                $search_sql.=" nns_name like '%" . $params['name'] . "%' or";
                                        break;
                                case 'pinyin_likechar':
                                        if (!empty($params['pinyin']))
                                                $search_sql.=" nns_pinyin like '%" . $params['pinyin'] . "%' or";
                                        break;
                                case 'en_firstchar':
                                        if (!empty($params['en_name']))
                                                $search_sql.=" nns_eng_name like '" . $params['en_name'] . "%' or";
                                        break;
                                case 'en_likechar':
                                        if (!empty($params['en_name']))
                                                $search_sql.=" nns_eng_name like '%" . $params['en_name'] . "%' or";
                                        break;
                                case 'pinyin_firstchar':
                                        if (!empty($params['pinyin']))
                                                $search_sql.=" nns_pinyin like '" . $params['pinyin'] . "%' or";
                                        break;
                                default:
//					$search_sql.=' ';
                                        break;
                        }
                }
                $search_sql = rtrim($search_sql, 'or');
                $search_sql = trim($search_sql);
                if (!empty($search_sql))
                        $search_sql = '(' . $search_sql . ')';
                $sql_where.= $search_sql;

                foreach ($params as $key => $value) {
                        if ($key == 'begin_time' && !empty($value)) {
                                $sql_where.=' and nns_begin_time >= \'' . $value . '\'';
                                continue;
                        }
                        if ($key == 'end_time' && !empty($value)) {
                                $sql_where.=' and nns_begin_time <= \'' . $value . '\'';
                                continue;
                        }
//					if ($key=='name' && !empty($value)){
//						$sql_where.=' and nns_name like \'%'.$value.'%\'';
//						continue;
//					}
                        if ($key == 'state' && (!empty($value) || $value == '0')) {
                                $sql_where.=' and nns_state = \'' . $value . '\'';
                                continue;
                        }
//					if ($key=='pinyin' && !empty($value)){
//						$sql_where.=' and nns_pinyin like \''.$value.'\'';
//						continue;
//					}
//					if ($key=='en_name' && !empty($value)){
//						$sql_where.=' and nns_eng_name like \''.$value.'\'';
//						continue;
//					}
                }
                //yxt下面这2行代码是我加的
                $sql_where = trim($sql_where, ' ');
                $sql_where = trim($sql_where, 'and');
                $sql.=' ' . $sql_where;

                if (array_key_exists('except', $params)) {
                        if (is_array($params['except'])) {
                                $sql.=' and nns_id not in (';
                                switch ($params['except']['packet_type']) {
                                        case 'asset':
                                                $sql.=' select nns_video_id from nns_assists_item where ' .
                                                        'nns_assist_id="' . $params['except']['packet_id'] . '" and ' .
                                                        'nns_category_id="' . $params['except']['category_id'] . '" ' .
                                                        'and nns_video_type="2" ';
                                                break;
                                        case 'service':
                                                $sql.=' select nns_video_id from nns_assists_item where ' .
                                                        'nns_service_id="' . $params['except']['packet_id'] . '" and ' .
                                                        'nns_category_id="' . $params['except']['category_id'] . '" ' .
                                                        'and nns_video_type="2" ';
                                                break;
                                        case 'ad':
                                                $sql.=' select nns_video_id from nns_ad_content_item where ' .
                                                        'nns_ad_id="' . $params['except']['packet_id'] . '" ' .
                                                        'and nns_video_type="2" and nns_type="video"';
                                                break;
                                        case 'ad_policy':
                                                $sql.=' select nns_video_id from nns_ad_postion_policy where ' .
                                                        'nns_ad_postion_id="' . $params['except']['packet_id'] . '" ' .
                                                        'and nns_video_type="playbill" and nns_type="video"';
                                                break;
                                }
                                $sql.=') ';
                        }
                }
//		echo ($sql);	die;
                $result = nl_query_by_db($sql, $dc->db());
//		var_dump($result);die;
                return $result[0]['num'];
        }

        /*
         * *********************************************
         * search_playbill_item_list
         * 从DC搜索节目单内容列表
         * ********************************************
         */

        /**
         * 从DC模块查询节目单内容列表
         * @param dc DC模块
         * @param string 直播ID
         * @param int 起始查询记录位置
         * @param int 查询记录条数
         * @param array 查询参数组
          Array(
          “begin_time”=>起始时间 ，
          “end_time”=>结束时间，
          “name”=>节目名称，LIKE查询 ,
          “state”=>节目状态  NULL 为忽略  0为有效 1为无效
          "pinyin"=>拼音
          "en_name"=>英文名
          "except"=>array(
          'packet_type'=>被排除的节目单所属包类型
          'packet_id'=>被排除的节目单包ID
          'category_id'=>被排除的节目单包栏目ID
          )
          )
          @param search_type 搜索类型
         * @param string 排序规则， NL_ORDER_TIME_ASC | NL_ORDER_TIME_DESC 
          默认为 NL_ORDER_TIME_ASC

          NL_ORDER_TIME_ASC 按创建时间升序排列
          NL_ORDER_TIME_ DESC 按创建时间降序排列

         * @return *
         * 			Array 信息包内容列表结果

         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function search_playbill_item_list($dc, $live_id, $since, $num, $params, $search_type = 'pinyin_firstchar', $order = NL_ORDER_TIME_ASC) {
                $sql = 'select * from  nns_live_playbill_item where ';
                if (!empty($live_id)) {
                        $sql.='nns_live_id=\'' . $live_id . '\' and'; //yxt，2013年3月7日16:51:23这里的这个and是我加的。
                } else {
                        $sql .= ' 1 and ';
                }
//		var_dump($search_type);	die;
                $sql_where = '';

                $search_types = explode('|', $search_type);

                $search_sql = '';
                foreach ($search_types as $type) {

                        switch ($type) {
                                case 'name_firstchar':
                                        if (!empty($params['name']))
                                                $search_sql.=" nns_name like '" . $params['name'] . "%' or";
                                        break;
                                case 'name_likechar':
                                        if (!empty($params['name']))
                                                $search_sql.=" nns_name like '%" . $params['name'] . "%' or";
                                        break;
                                case 'pinyin_likechar':
                                        if (!empty($params['pinyin']))
                                                $search_sql.=" nns_pinyin like '%" . $params['pinyin'] . "%' or";
                                        break;
                                case 'en_firstchar':
                                        if (!empty($params['en_name']))
                                                $search_sql.=" nns_eng_name like '" . $params['en_name'] . "%' or";
                                        break;
                                case 'en_likechar':
                                        if (!empty($params['en_name']))
                                                $search_sql.=" nns_eng_name like '%" . $params['en_name'] . "%' or";
                                        break;
                                case 'pinyin_firstchar':
                                        if (!empty($params['pinyin']))
                                                $search_sql.=" nns_pinyin like '" . $params['pinyin'] . "%' or";
                                        break;
                                default:
//					$search_sql.=' ';
                                        break;
                        }
                }
                $search_sql = rtrim($search_sql, 'or');
                $search_sql = trim($search_sql);
                if (!empty($search_sql))
                        $search_sql = '(' . $search_sql . ')';
                $sql_where.= $search_sql;

                foreach ($params as $key => $value) {
                        if ($key == 'begin_time' && !empty($value)) {
                                $sql_where.=' and nns_begin_time >= \'' . $value . '\'';
                                continue;
                        }
                        if ($key == 'end_time' && !empty($value)) {
                                $sql_where.=' and nns_begin_time <= \'' . $value . '\'';
                                continue;
                        }
//					if ($key=='name' && !empty($value)){
//						switch ($search_type){
//							case 'name_firstchar':
//								$sql_where.=' and nns_name like \''.$value.'%\'';
//							break;
//							default:
//								$sql_where.=' and nns_name like \'%'.$value.'%\'';
//							break;
//						}
//						continue;
//					}
                        if ($key == 'state' && (!empty($value) || $value == '0')) {
                                $sql_where.=' and nns_state = \'' . $value . '\'';
                                continue;
                        }
//					if ($key=='pinyin' && !empty($value)){
//						switch ($search_type){
//							case 'pinyin_likechar':
//								$sql_where.=' and nns_pinyin like \'%'.$value.'%\'';
//							break;
//							default:
//								$sql_where.=' and nns_pinyin like \''.$value.'%\'';
//							break;
//						}
//						continue;
//					}
//					if ($key=='en_name' && !empty($value)){
//						switch ($search_type){
//							case 'en_firstchar':
//								$sql_where.=' and nns_eng_name like \''.$value.'%\'';
//							break;
//							default:
//								$sql_where.=' and nns_eng_name like \'%'.$value.'%\'';
//							break;
//						}
////						$sql_where.=' and nns_eng_name like \''.$value.'\'%';
//						continue;
//					}
                }
                //yxt下面这2行代码是我加的
                $sql_where = trim($sql_where, ' ');
                $sql_where = trim($sql_where, 'and');
                $sql.=' ' . $sql_where;
                $sql.=' and nns_deleted <> 1 ';

                if (array_key_exists('except', $params)) {
                        if (is_array($params['except'])) {
                                $sql.=' and nns_id not in (';
                                switch ($params['except']['packet_type']) {
                                        case 'asset':
                                                $sql.=' select nns_video_id from nns_assists_item where ' .
                                                        'nns_assist_id="' . $params['except']['packet_id'] . '" and ' .
                                                        'nns_category_id="' . $params['except']['category_id'] . '" ' .
                                                        'and nns_video_type="2" ';
                                                break;
                                        case 'service':
                                                $sql.=' select nns_video_id from nns_assists_item where ' .
                                                        'nns_service_id="' . $params['except']['packet_id'] . '" and ' .
                                                        'nns_category_id="' . $params['except']['category_id'] . '" ' .
                                                        'and nns_video_type="2" ';
                                                break;
                                        case 'ad':
                                                $sql.=' select nns_video_id from nns_ad_content_item where ' .
                                                        'nns_ad_id="' . $params['except']['packet_id'] . '" ' .
                                                        'and nns_video_type="2" and nns_type="video"';
                                                break;
                                        case 'ad_policy':
                                                $sql.=' select nns_video_id from nns_ad_postion_policy where ' .
                                                        'nns_ad_postion_id="' . $params['except']['packet_id'] . '" ' .
                                                        'and nns_video_type="playbill" and nns_type="video"';
                                                break;
                                }
                                $sql.=') ';
                        }
                }

                switch ($order) {
                        case NL_ORDER_TIME_DESC:
                                $sql.=' order by nns_begin_time desc ';
                                break;
                        default:
                                $sql.=' order by nns_begin_time asc ';
                                break;
                }

                $sql.='limit ' . $since . ',' . $num;
//		
//echo $sql;die;

                return nl_query_by_db($sql, $dc->db());
        }

        /*
         * *********************************************
         * update_playbill_item_state
         * 从DC搜索节目单内容列表
         * ********************************************
         */

        /**
         * 从DC模块修改节目状态
         * @param dc DC模块
         * @param string 节目ID
         * @param int 状态类型 0为有效 ，1为无效
         * @return *
         * 			FALSE 失败
         * 			TRUE  成功
         */
        static public function update_playbill_item_state($dc, $id, $state) {
                $sql = 'update nns_live_playbill_item set ' .
                        'nns_state=\'' . $state . '\' ' .
                        'where nns_id=\'' . $id . '\'';
//		echo $sql;die;		
                $result = nl_execute_by_db($sql, $dc->db());
                $info = self::get_playbill_item_info($dc, $id);
                if ($result === TRUE) {
                        self::delete_playbill_item_list_by_cache($dc, $info[0]['nns_live_id']);
                }

                return $result;
        }

        /*
         * *********************************************
         * epg_get_playbill_item_list_by_day
         * 从DC模块获取节目单列表(前端用)
         * ********************************************
         */

        /**
         * 从DC模块获取节目单列表（前端用）
         * @param dc DC模块
         * @param string 直播ID
         * @param string日期 格式为20121129
         * @param int 时区  8  默认为当前时区
         * @param String 读取策略 NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE
          默认为NL_DC_AUTO
         * @return *
         * 			Array 节目单列表结果
         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function epg_get_playbill_item_list_by_day($dc, $live_id, $day, $timezone, $policy = NL_DC_AUTO) {
                return self::get_playbill_item_list($dc, $live_id, $day, date('Ymd', strtotime($day) + 3600 * 24), 0, 0, 500, $timezone, $policy);
        }

        /*
         * *********************************************
         * epg_get_playbill_item_list_by_day
         * 从DC模块获取节目单列表(前端用)
         * ********************************************
         */

        /**
         * 前端从DC模块获取信息包内容列表
          只读取前端列表需要的字段。
         * @param dc DC模块
         * @param string 直播ID 为空为忽略条件
         * @param int 起始查询记录位置
         * @param int 查询记录条数
         * @param array 查询参数组
          Array(
          “begin_time”=>起始时间 ，
          “end_time”=>结束时间，
          “name”=>节目名称，LIKE查询 ,
          “timezone”=>时区，不传为当前时区
          )
         * @param string 查询条件 
         * @param string 排序规则， NL_ORDER_TIME_ASC | NL_ORDER_TIME_DESC 
          默认为 NL_ORDER_TIME_ASC

          NL_ORDER_TIME_ASC 按创建时间升序排列
          NL_ORDER_TIME_ DESC 按创建时间降序排列

         * @return *
         * 			Array 信息包内容列表结果

         * 			FALSE 查询失败
         * 			TRUE  查询成功，但无数据
         */
        static public function epg_search_playbill_item_list($dc, $live_id, $since, $num, $params, $search_type = 'pinyin_firstchar', $order = NL_ORDER_TIME_ASC) {
                $timezone = $params['timezone'];
                $begin_time = $params['begin_time'];
                $end_time = $params['end_time'];
                if (!empty($timezone) || ($timezone != '' && $timezone == 0)) {
                        if (!empty($begin_time)) {
                                $params['begin_time'] = date('Y-m-d H:i:s', strtotime($begin_time . '+0000') - $timezone * 3600);
                        }
                        if (!empty($end_time)) {
                                $params['end_time'] = date('Y-m-d H:i:s', strtotime($end_time . '+0000') - $timezone * 3600);
                        }
                } else {
                        if (!empty($begin_time)) {
                                $params['begin_time'] = date('Y-m-d H:i:s', strtotime($begin_time));
                        }
                        if (!empty($end_time)) {
                                $params['end_time'] = date('Y-m-d H:i:s', strtotime($end_time));
                        }
                }
                $params['state'] = 0;

                $result = self::search_playbill_item_list($dc, $live_id, $since, $num, $params, $search_type, $order);
//	var_dump($result);
//		die;
                //		当前时区差
                $time_offset = strtotime(date('Y-m-d') . '+0000') - strtotime(date('Y-m-d'));
                if (is_array($result) && (!empty($timezone) || ($timezone != '' && $timezone == 0))) {
                        foreach ($result as $key => $item) {
                                $item['nns_begin_time'] = date('Y-m-d H:i:s', strtotime($item['nns_begin_time']) - $time_offset + $timezone * 3600);
                                $result[$key] = $item;
                        }
                }

                return $result;
        }

        static public function epg_count_search_playbill_item_list($dc, $live_id, $params, $search_type = 'pinyin_firstchar') {
                $timezone = $params['timezone'];
                $begin_time = $params['begin_time'];
                $end_time = $params['end_time'];
                if (!empty($timezone) || ($timezone != '' && $timezone == 0)) {
                        if (!empty($begin_time)) {
                                $params['begin_time'] = date('Y-m-d H:i:s', strtotime($begin_time . '+0000') - $timezone * 3600);
                        }
                        if (!empty($end_time)) {
                                $params['end_time'] = date('Y-m-d H:i:s', strtotime($end_time . '+0000') - $timezone * 3600);
                        }
                } else {
                        if (!empty($begin_time)) {
                                $params['begin_time'] = date('Y-m-d H:i:s', strtotime($begin_time));
                        }
                        if (!empty($end_time)) {
                                $params['end_time'] = date('Y-m-d H:i:s', strtotime($end_time));
                        }
                }
                $params['state'] = 0;

                $result = self::count_search_playbill_item_list($dc, $live_id, $params, $search_type);
                return $result;
        }

        static public function get_playbill_item_info_by_live_id($dc, $live_id, $begin_time) {
                $sql = 'select * from nns_live_playbill_item where nns_live_id=\'' . $live_id . '\' and nns_begin_time=\'' . $begin_time . '\'';
                $result = nl_query_by_db($sql, $dc->db());

                return $result['0']['nns_id'];
        }

        /**
         * 获取直播频道正在播放节目名称
         */
        static public function get_playbill_name_by_live_id($dc, $live_id, $begin_time, $end_time, $state, $since, $num) {
                $sql = 'select * from nns_live_playbill_item where ' .
                        'nns_live_id=\'' . $live_id . '\' and ' .
                        'nns_begin_time<=\'' . $begin_time . '\' ';
                //'nns_begin_time>=\''.$end_time.'\' ' ;
                if ($state !== NULL) {
                        $sql.='and nns_state=\'' . $state . '\' ';
                }
                $sql.='order by nns_begin_time desc ';
                if (!empty($num)) {
                        $sql.='limit ' . $since . ',' . $num;
                }

//		echo $sql;die;
                return nl_query_by_db($sql, $dc->db());
        }

        /**
         * 获取直播频道即将播放节目名称
         */
        static public function get_next_playbill_name_by_live_id($dc, $live_id, $begin_time, $end_time, $state, $since, $num) {
                $sql = 'select * from nns_live_playbill_item where ' .
                        'nns_live_id=\'' . $live_id . '\' and ' .
                        'nns_begin_time>=\'' . $begin_time . '\' ';
                if ($state !== NULL) {
                        $sql.='and nns_state=\'' . $state . '\' ';
                }
                $sql.='order by nns_begin_time desc ';
                if (!empty($num)) {
                        $sql.='limit ' . $since . ',' . $num;
                }


                return nl_query_by_db($sql, $dc->db());
        }

        /**
         * 删除指定播出时间内的节目单
         */
        static public function del_playbill_item_info_by_time($dc, $statime, $endtime, $video_id) {
                if (empty($statime) || empty($endtime)) {
                        return false;
                }
                $sql = "DELETE FROM `nns_live_playbill_item` WHERE `nns_begin_time` >= date('{$statime}') AND `nns_begin_time` <= date('{$endtime}') AND `nns_live_id` = '{$video_id}'";

                $result = nl_execute_by_db($sql, $dc->db());
                if (is_array($result)) {
                        $result = np_array_rekey($result, 'nns_id');
                }

                return $result;
        }

        /*
         * 修改节目单
         */

        static public function set_playbill_item_by_info($dc, $id, $params) {

                return self::_modify_playbill_item_by_db($dc->db(), $id, $params);
        }
        
        
        static public function query_by_import_id_cp($dc,$nns_import_id,$nns_cp_id,$fild=null)
        {
            $str_import_id = (is_array($nns_import_id)) ? " nns_playbill_import_id in ('".implode("','", $nns_import_id)."') " : " nns_playbill_import_id='{$nns_import_id}' ";
            $fild = strlen($fild) >0 ? $fild : "*";
            $sql="select {$fild} from " . self::$base_table . " where {$str_import_id} and nns_cp_id='{$nns_cp_id}' limit 1";
            $result = nl_query_by_db($sql, $dc->db());
            if(!$result)
            {
                return self::return_data(1,'数据库执行失败'.$sql);
            }
            $result = (is_array($result) && !empty($result)) ? $result : null;
		    return self::return_data(0,'OK'.$sql,$result);
        }
        
        
        static public function query_import_all_info($dc,$arr_import_id,$cp_id,$import_source)
        {
            if(empty($arr_import_id) || !is_array($arr_import_id))
            {
                return self::return_data(0,'OK',null);
            }
            $sql="select nns_state as nns_deleted,nns_playbill_import_id as nns_import_id from " . self::$base_table . " where nns_playbill_import_id in(".implode("','", $arr_import_id).") and nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}'";
            $result = nl_query_by_db($sql, $dc->db());
            if(!$result)
            {
                return self::return_data(1,'数据库查询失败,sql:'.$sql);
            }
            $result = (is_array($result) && !empty($result)) ? $result : null;
            $last_result = null;
            if(is_array($result))
            {
                foreach ($result as $val)
                {
                    $val['nns_deleted'] = ($val['nns_deleted'] !=0) ? 'deleted' : 'exsist';
                    $last_result[$val['nns_deleted']][] = $val['nns_import_id'];
                }
            }
            return self::return_data(0,'OK',$result);
        }

    /**
     * @description:通过注入id，注入来源，cp来查询是否存在这条数据
     * @author:xinxin.deng
     * @date: 2017/12/5 11:38
     * @param $dc
     * @param $arr_import_id
     * @param $cp_id
     * @param $import_source
     * @return array
     */
    static public function query_import_info($dc,$arr_import_id,$cp_id,$import_source)
    {
        if(empty($arr_import_id) || !is_array($arr_import_id))
        {
            return self::return_data(1,'节目单注入id为空',null);
        }
        $sql="select * from " . self::$base_table . " where nns_playbill_import_id in('".implode("','", $arr_import_id)."') and nns_cp_id='{$cp_id}' and nns_import_source='{$import_source}'";
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK',$result);
    }

    /**
     * @description:修改节目单数据
     * @author:xinxin.deng
     * @date: 2017/12/5 11:39
     * @param $dc
     * @param $id
     * @param $params
     * @param int $policy
     * @return bool
     */
    static public function modify_playbill_item_v2($dc, $id, $params, $policy = NL_DC_DB) {
        $params = self::except_useless_params(self::$arr_filed, $params);
        $result = self::_modify_playbill_item_by_db_v2($dc->db(), $id, $params);
        $info = self::get_playbill_item_info($dc, $id);
        if ($result === TRUE) {
            nl_video_label_bind_content::del_video_label_content($dc, $id, 2);
            $label_arr = array();

            $label_arr['kind'] = $params['kind'];

            foreach ($label_arr as $label_key => $label_item) {
                nl_video_label_bo::bind_video_label_by_video(
                    $dc, $params['id'], 2, array(
                        'type_id' => $label_key,
                        'name' => $label_item,
                    )
                );
            }
            self::delete_playbill_item_list_by_cache($dc, $info[0]['nns_live_id']);
            self::delete_playbill_item_info_by_cache($dc, $id);
        }
        return $result;
    }

    static private function _modify_playbill_item_by_db_v2($db, $id, $params) {
        $sql = 'update  nns_live_playbill_item set ';
        $query = '';

        foreach ($params as $key => $value) {
                $query .= ' ' . $key . '=\'' . $value . '\',';
        }
        $date_time = date("Y-m-d H:i:s");
        $query.='nns_modify_time=\'' . $date_time . '\',';
        $query = rtrim($query, ',');
        $sql.=$query . ' where nns_id=\'' . $id . '\'';

        $re =  nl_execute_by_db($sql, $db);

        return $re;
    }

    /**
     * @description:添加节目单
     * @author:xinxin.deng
     * @date: 2017/12/5 13:36
     * @param $dc
     * @param $params
     * @return array
     */
    static public function add($dc,$params) {
        if(!isset($params['nns_id']) || strlen($params['nns_id'])<1)
        {
            $params['nns_id'] = np_guid_rand();
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok:'.$sql);
    }

    /**
     * 修改直播片源
     * @param object $dc 数据库对象
     * @param array $params 数据数组
     * @param array|string $nns_id GUID
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     * @author liangpan
     * @date 2016-09-24
     */
    static public function edit($dc,$params,$nns_id)
    {
        if(strlen($nns_id)<0)
        {
            return self::return_data(1,'CDN直播频道队列guid为空');
        }
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(empty($params))
        {
            return self::return_data(1,'参数为空');
        }
        if(!isset($params['nns_modify_time']) || strlen($params['nns_modify_time'])<1)
        {
            $params['nns_modify_time'] = date("Y-m-d H:i:s");
        }
        $sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));

        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库执行失败'.$sql);
        }
        return self::return_data(0,'ok');
    }

    /**
     * @description:删除节目单
     * @author:xinxin.deng
     * @date: 2017/12/5 14:07
     * @param $dc
     * @param $params
     * @return array
     */
    static public function del($dc,$params)
    {
        $params = self::make_nns_pre($params);
        if(empty($params) || !is_array($params))
        {
            return self::return_data(1,'参数为空');
        }
        $sql = self::make_delete_sql(self::$base_table,$params);
        if(strlen($sql) <1)
        {
            return self::return_data(1,'数据组装sql为空，不允许删除');
        }
        $result = nl_execute_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        return self::return_data(0,'OK'.$sql);
    }

    /**
     * 根据条件获取节目单
     * @param $dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($dc,$params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if(!is_array($params) || empty($params))
        {
            return self::return_data(1,'查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table,$params);
        $result = nl_query_by_db($sql, $dc->db());
        if(!$result)
        {
            return self::return_data(1,'数据库查询失败,sql:'.$sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0,'OK'.$sql,$result);
    }
}
?>
