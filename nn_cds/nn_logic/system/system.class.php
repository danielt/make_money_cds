<?php
/*
 * Created on 2012-11-06
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
//include_once dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nl_common.func.php';

class nl_system{
	/**
	 * 从DB模块获取信息包列表
	 * @param db  DB模块
	 * @param string  读取策略     可为空            默认为NL_DC_AUTO   策略类型:NL_DC_AUTO| NL_DC_DB | NL_DC_CACHE   
	 * @return *
	 * 			Array 信息包列表
	 * 			FALSE 查询失败
	 * 			TRUE  查询成功，但无数据
	 */
	static function get_carrier_id($db){
		$sql = "SELECT nns_id FROM nns_carrier LIMIT 1 ";
		return nl_query_by_db($sql, $db);
		
	}
}
?>