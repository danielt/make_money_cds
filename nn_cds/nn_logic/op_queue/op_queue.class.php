<?php
/**
 * 中心同步队列管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_op_queue extends nl_public
{
	static $base_table='nns_mgtvbk_op_queue';
	
	static $arr_filed = array(
	    'nns_id',
	    'nns_type',
	    'nns_org_id',
	    'nns_create_time',
	    'nns_action',
	    'nns_status',
	    'nns_name',
	    'nns_video_id',
	    'nns_weight',
	    'nns_index_id',
	    'nns_media_id',
	    'nns_op_mtime',
	    'nns_release_time',
	    'nns_state',
	    'nns_ex_data',
	    'nns_from',
	    'nns_message_id',
	    'nns_encode_flag',
	    'nns_is_group',
	    'nns_cp_id',
        'nns_bk_queue_excute_url',
        'nns_command_task_id',
	);
	/**
	 * 查询op queue 数据 
	 * @param object $dc 数据库操作对象
	 * @param string $op_queue_id op_queue guid
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	static public function query_op_queue_info_by_id($dc,$op_queue_id)
	{
		if(empty($op_queue_id))
		{
			return self::return_data(1,'op queue id为空');
		}
		$sql="select * from " . self::$base_table . " where nns_id='{$op_queue_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok',$result);
	}
	
	
	/**
	 * 查询op queue 数据
	 * @param object $dc 数据库操作对象
	 * @param string $op_queue_id op_queue guid
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
	 * @authorliangpan
	 * @date 2015-12-25
	 */
	static public function query_op_queue_by_unique($dc,$nns_type,$nns_sp_id,$nns_action,$nns_video_id,$nns_index_id,$nns_media_id)
	{
	    $sql="select * from " . self::$base_table . " where nns_type='{$nns_type}' and nns_org_id='{$nns_sp_id}' and nns_action='{$nns_action}' and nns_video_id='{$nns_video_id}' and nns_index_id='{$nns_index_id}' and nns_media_id='{$nns_media_id}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    $result = (isset($result[0]) && is_array($result[0]) && !empty($result[0])) ? $result[0] : null;
	    return self::return_data(0,'ok'.$sql,$result);
	}
	
	/**
	 * 计算负载均衡服务器数量
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据'),
	 * @authorliangpan
	 * @date 2017-01-17
	 */
	static public function query_group_encode_flag($dc,$sp_id)
	{
	    if(empty($sp_id))
	    {
	        return self::return_data(1,'sp id为空');
	    }
	    $sql ="select count(*) as count,nns_encode_flag from " . self::$base_table . " where nns_org_id='{$sp_id}' and nns_status='8' and nns_action in('add','modify') and nns_type = 'media' group by nns_encode_flag";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok',$result);
	}
	
	
	/**
	 *
	 * @param unknown $dc
	 * @param unknown $sp_id
	 * @param string $flag
	 */
	static public function get_max_priority($dc,$sp_id)
	{
	    if(strlen($sp_id) <1)
	    {
	        return self::return_data(1,'SP_id为空');
	    }
	    $sql="select max(nns_weight) as max_priority from " . self::$base_table . " where nns_org_id='{$sp_id}' limit 1";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0])) ? (int)$result[0]['max_priority'] : 0;
	    return self::return_data(0,'OK',$result);
	}
	
	static public function add($dc,$params)
	{
	    $params['nns_id'] = (isset($params['nns_id']) && !empty($params['nns_id'])) ? $params['nns_id'] : np_guid_rand();
	    $params = self::except_useless_params(self::$arr_filed, $params);
	    if(empty($params))
	    {
	        return self::return_data(1,'参数为空');
	    }
	    $sql = self::make_insert_sql(self::$base_table, $params);
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok');
	}
	
	/**
	 * 修改CP
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit($dc,$params,$nns_id)
	{

	    if((!is_string($nns_id) && !is_array($nns_id)) || (is_string($nns_id) && strlen($nns_id)<0) || (is_array($nns_id) && empty($nns_id)))
	    {
	        return self::return_data(1,'nns_id为空');
	    }
	    $params = self::except_useless_params(self::$arr_filed, $params);
	    if(empty($params))
	    {
	        return self::return_data(1,'参数为空');
	    }
	    $sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok');
	}
	/**
	 * 根据相关条件获取中心同步指令数据
	 * @param object $dc
	 * @param array $params
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author zhiyong.luo
	 */
	static public function get_overtime_op_queue_by_condition($dc,$params)
	{
		if(!is_array($params))
		{
			return self::return_data(1,'参数错误');
		}
		$where_str = '';
		foreach ($params as $key=>$value)
		{
			if($key === "less_than" && !empty($value) && is_array($value))
			{
				foreach ($value as $k=>$val)
				{
					$where_str .= " {$k} <= '{$val}' AND ";
				}
			}
			else if($key === "more_than" && !empty($value) && is_array($value))
			{
				foreach ($value as $k=>$val)
				{
					$where_str .= " {$k} >= '{$val}' AND ";
				}
			}
			else if ($key === "not in" && !empty($value) && is_array($value))
			{
				foreach ($value as $k=>$val)
				{
					$not_str = implode("','", $val);
					$where_str .= " {$k} {$key} ('{$val}') AND ";
				}
			}
			else if ($key === "in" && !empty($value) && is_array($value))
			{
				foreach ($value as $k=>$val)
				{
					$not_str = implode("','", $val);
					$where_str .= " {$k} {$key} ('{$val}') AND ";
				}
			}
			else 
			{
				$where_str .= " {$key} = '{$value}' AND ";
			}
		}
		$where_str = rtrim($where_str," AND ");
		if(!empty($where_str))
		{
			$where_str = " WHERE " . $where_str;
		}
		$str_sql = "select * from " . self::$base_table . $where_str;
		$result = nl_query_by_db($str_sql, $dc->db());
		if(!is_array($result))
		{
			$result = array();
		}
		return self::return_data(0,'OK',$result);
	}
	/**
	 * 删除中心同步队列
	 * @param object $dc
	 * @param array $arr_id
	 * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author zhiyong.luo
	 */
	static public function delete_op_queue($dc,$arr_id)
	{
		if(!is_array($arr_id) || empty($arr_id))
		{
			return self::return_data(1,'参数错误');
		}
		$str_ids = implode("','", $arr_id);
		$str_sql = "delete from " . self::$base_table . " where nns_id in ('{$str_ids}')";
		$result = nl_execute_by_db($str_sql, $dc->db());
		if($result === false)
		{
			return self::return_data(1,'mysql execute failed');
		}
		else
		{
			return self::return_data(0,'OK');
		}
	}
	
	/**
	 * 消息查询
	 * @param unknown $dc
	 * @param unknown $message_id
	 * @param unknown $cp_id
	 */
	static public function query_message_by_message_id($dc,$message_id,$sp_id,$video_type)
	{
	    $str_temp = (is_array($sp_id) && !empty($sp_id)) ? " and nns_org_id in ('" . implode("','", $sp_id) . "') " : " and nns_org_id = '{$sp_id}' ";
	    $sql="select * from " . self::$base_table . " where nns_message_id='{$message_id}' and nns_type='{$video_type}' {$str_temp} limit 1" ;
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'消息队列查询失败'.$sql);
	    }
	    $result = (isset($result[0]) && !empty($result[0]) && is_array($result[0])) ? $result[0] : null;
	    return self::return_data(0,'ok',$result);
	}
	
    /**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	

    /**
     * 依据条件查询数据
     * @param $dc
     * @param $arr_params
     * @param null $limit
     * @return array
     */
	static public function query_by_params($dc,$arr_params,$limit=null)
	{
	    $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
	    if(empty($arr_params) || !is_array($arr_params) )
	    {
	        return self::return_data(1,'查询参数为空');
	    }
	    $limit = ($limit === null || strlen($limit)<1) ? 0 : (int)$limit;
	    $str_limit = $limit >0 ? " limit {$limit} " : '';
	    $arr_where=null;
	    foreach ($arr_params as $key=>$val)
	    {
	        if(is_array($val))
	        {
	            $val = array_values($val);
	            $arr_where[]= " {$key} in('" . implode("','", $val) . "') ";
	        }
	        else
	        {
	            $arr_where[]= " {$key}='{$val}' ";
	        }
	    }
	    $sql="select * from " . self::$base_table . " where " . implode(" and ", $arr_where) . " order by nns_weight desc,nns_op_mtime asc  {$str_limit} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}


    /**
     * 依据条件查询主媒资信息
     * @param $dc
     * @param $sql
     * @return array
     */
    static public function query_by_sql($dc, $sql)
    {
        $result = nl_query_by_db($sql, $dc->db());
        if (!$result)
        {
            return self::return_data(1, '数据库查询失败,sql:' . $sql);
        }
        $result = (isset($result) && !empty($result)) ? $result : null;
        return self::return_data(0, 'OK' . $sql, $result);
    }
}