<?php
include_once dirname(dirname(dirname(__FILE__))) . '/nn_cms_config/log_define_config.php';
class nn_log
{
	public static $base_log_dir = null;
	public static $relative_log_dir = null;
	public static $sql_model = 'sql';
	public static $message_model = 'message';
	public static $debug_model = 'debug';
	public static $error_model = 'error';
	public static $main_model_default = 'default_log';
	public static $timer_operation = 'timer';
	public static $manual_operation = 'manual';
	public static $str_model = '';
	public static $arr_log_model = array (
			'sp_import_log', 
			'distribute_log',
			'op_log', 
			'op_queue_log', 
			'cdn_log', 
			'cms_log', 
			'clip_log',
			'asset_import_log',
			'unline_log',
			'callback_log',
			'message_log',
			'cdn_bk_send_log',
			'cdn_bk_notify_log',
			'bk_cdn_send_log',
			'bk_cdn_notify_log',
			'msp_log',
	        'gateway_log',
			'pass_queue',
	        'bk_cdn_delivery_notify_log',
	        'bk_cdn_line_notify_log'
	);

	public function __construct()
	{
	}

	/**
	 * 把log日志逻辑处理写入相应的日志文件中
	 * @param string $main_model 主处理模块
	 * @param string $message 消息log信息 如果是debug模块 则消息为空
	 * @param string $sp_id  运营商id 如果为空 则不会创建文件路径
	 * @param string $video_type 媒资类型 如果为空 则不会创建文件路径
	 * @param string $model 日志模块 默认为message消息模块
	 * @return
	 * @author liangpan
	 * @date 2015-07-28
	 */
	public static function write_log_message($main_model, $message = null, $sp_id = null, $video_type = null, $model = 'message',$cp_id=null)
	{

		$log_enable = false;
		switch ($model)
		{
			case self::$message_model;
				$log_enable=MS_MESSAGE_ENABLE;
				self::$str_model='[message_model]';
				break;
			case self::$sql_model;
				$log_enable=MS_SQL_ENABLE;
				self::$str_model='[sql_model]';
				break;
			case self::$debug_model;
				$log_enable=MS_DEBUG_ENABLE;
				self::$str_model='[debug_model]';
				break;
			case self::$error_model;
				$log_enable=MS_MESSAGE_ENABLE;
				self::$str_model='[error_model]';
				break;
			default:
				$log_enable=MS_MESSAGE_ENABLE;
				self::$str_model='[message_model]';
				$model=self::$message_model;
		}
		if (!$log_enable)
		{
			return '';
		}
		unset($log_enable);
		$base_log_dir_pr = dirname(dirname(dirname(__FILE__))) ;
		self::$base_log_dir = "data/log_v2";
		$str_manual_timer = (defined("IS_LOG_TIMER_OPERATION") && IS_LOG_TIMER_OPERATION === true) ? self::$timer_operation : self::$manual_operation;
		$main_model = strtolower(trim($main_model));
		$main_model = in_array($main_model, self::$arr_log_model) ? $main_model : self::$main_model_default;
		self::$base_log_dir .= "/" . $main_model;
		if (strlen($cp_id) > 0)
		{
			$cp_id = strtolower($cp_id);
			self::$base_log_dir .= "/" . $cp_id;
		}
		if (strlen($sp_id) > 0)
		{
			$sp_id = strtolower($sp_id);
			self::$base_log_dir .= "/" . $sp_id;
		}
		if (strlen($video_type) > 0)
		{
			$video_type = strtolower($video_type);
			self::$base_log_dir .= "/" . $video_type;
		}
		self::$base_log_dir .= "/" . $str_manual_timer . '/' . date('Y-m-d');
		if (!is_dir($base_log_dir_pr.'/'.self::$base_log_dir))
		{
			mkdir($base_log_dir_pr.'/'.self::$base_log_dir, 0777, true);
		}
		self::$base_log_dir .= '/' . date('H') . '.txt';
		self::$relative_log_dir = self::$base_log_dir;
		self::$base_log_dir = $base_log_dir_pr.'/'.self::$relative_log_dir;
		if ($model == self::$debug_model)
		{
			self::get_debug_print_backtrace(2,$message);
			return self::$relative_log_dir;
		}
		$getmypid = getmypid();
		self::write_log_message_bk("[".var_export($getmypid,true)."] ".var_export($message, true));
		//echo var_export($message, true)."<br/>";
		unset($message);
		return self::$relative_log_dir;
	}

	/**
	 * 把php的debug追踪
	 * @param string $str
	 * @param string $model
	 * @return
	 * @author liangpan
	 * @date 2015-07-28
	 */
	private static function get_debug_print_backtrace($ignore = 2,$message)
	{
		$str_backtracel = '';
		$str_backtracel_params = '';
		if(!empty($message))
		{
			$str_meaasge = "调试自定义信息：".var_export($message,true);
			self::write_log_message_bk($str_meaasge);
			unset($message);
			unset($str_meaasge);
		}
		foreach (debug_backtrace() as $k => $v)
		{
			if ($k < $ignore)
			{
				continue;
			}
			if ($v['function'] == "include" || $v['function'] == "include_once" || $v['function'] == "require_once" || $v['function'] == "require")
			{
				$str_backtracel = "#" . ($k - $ignore) . " " . $v['function'] . "(" . $v['args'][0] . ") called at [" . $v['file'] . ":" . $v['line'] . "]";
			}
			else
			{
				$str_backtracel = "#" . ($k - $ignore) . " " . $v['function'] . "() called at [" . $v['file'] . ":" . $v['line'] . "]";
			}
			self::write_log_message_bk($str_backtracel);
			if (empty($v['args']) || !is_array($v['args']))
			{
				continue;
			}
			foreach ($v['args'] as $__k => $__v)
			{
				$str_backtracel_params = "#" . ($k - $ignore) . "__{$__k} 第" . ($__k + 1) . "个参数值是：" . var_export($__v, true);
				self::write_log_message_bk($str_backtracel_params);
			}
		}
		unset($str_backtracel);
		unset($str_backtracel_params);
		return;
	}

	/**
	 * 把log日志写入相应的日志文件中
	 * @param string $str
	 * @param string $model
	 * @return
	 * @author liangpan 
	 * @date 2015-07-28
	 */
	private static function write_log_message_bk($str)
	{
		if (empty($str))
		{
			return;
		}
		
// 		include_once dirname(dirname(dirname(dirname(__FILE__)))).'/np/np_log_v2.class.php';
		
		
// 		date_default_timezone_set('PRC');
// 		$obj_log = new np_log_v2();
// 		$obj_log->set_dir(dirname(__FILE__) . '/data'); //设置存放路径
// 		$obj_log->set_level_config('error', 10, 10, 'error', 60);
// 		$obj_log->set_level_config('info', 10, 10, 'info', 60);
// 		for ($i = 0; $i < 100; $i++) {
// 			$obj_test->error('error2error2error2error2error2error2error2error2');
// 			$obj_test->info('info2info2info2info2info2info2info2info2info2info2');
// 		}
// 		$obj_test->save();
		
		
		
		
		$msg = '[' . date('H:i:s') . ']    ' . self::$str_model . '    ' . $str . "\n";
		@error_log($msg, 3, self::$base_log_dir);
		unset($str);
		unset($msg);
		return;
	}
}