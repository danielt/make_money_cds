<?php
/**
 * 队列池管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_queue_pool extends nl_public
{
	static $base_table='nns_queue_pool';
	static $arr_filed = array(
			'nns_id',
			'nns_queue_id',
			'nns_value',
			'nns_state',
			'nns_create_time',
			'nns_modify_time',
	);
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_id($dc,$nns_id)
	{
		$sql = "select * from " . self::$base_table . " where nns_id='{$nns_id}' limit 1";
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result[0]) && !empty($result[0])) ? $result[0] : null;
		return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 查询单个列表
	 * @param object $dc 数据库对象
	 * @param string $nns_id guid
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_queue_value_id($dc,$queue_id,$values)
	{
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    $values = is_array($values) ? $values : array($values);
	    if(empty($values))
	    {
	        return self::return_data(1,'$values为空');
	    }
	    $sql = "select * from " . self::$base_table . " where nns_queue_id='{$queue_id}' and nns_value in('".implode("','", $values)."')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 添加全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function add($dc,$params)
	{
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$sql = self::make_insert_sql(self::$base_table, $params);
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}
	
	/**
	 * 修改全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit($dc,$params,$nns_id)
	{
		if(strlen($nns_id)<0)
		{
			return self::return_data(1,'全局错误日志_id为空');
		}
		$params = self::except_useless_params(self::$arr_filed, $params);
		if(empty($params))
		{
			return self::return_data(1,'参数为空');
		}
		$params['nns_modify_time'] = date("Y-m-d H:i:s");
		$sql = self::make_update_sql(self::$base_table, $params,array('nns_id'=>$nns_id));
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库执行失败'.$sql);
		}
		return self::return_data(0,'ok');
	}

	/**
	 * 修改全局错误日志
	 * @param object $dc 数据库对象
	 * @param array $params 数据数组
	 * @return Ambigous <array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function edit_v2($dc,$params,$where_condition)
	{
	    if(!is_array($where_condition))
	    {
	        return self::return_data(1,'条件查询必须为数组');
	    }
	    $where_condition = self::except_useless_params(self::$arr_filed, $where_condition);
	    if(empty($where_condition) || !is_array($where_condition))
	    {
	        return self::return_data(1,'全局错误日志_id为空');
	    }
	    $params = self::except_useless_params(self::$arr_filed, $params);
	    if(empty($params))
	    {
	        return self::return_data(1,'参数为空');
	    }
	    $params['nns_modify_time'] = date("Y-m-d H:i:s");
	    $sql = self::make_update_sql(self::$base_table, $params,$where_condition);
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库执行失败'.$sql);
	    }
	    return self::return_data(0,'ok');
	}
	
	
	/**
	 * 依据queueid  查询数据
	 * @param unknown $dc
	 * @param unknown $queue_id
	 */
	static public function query_by_queue_id($dc,$queue_id)
	{
	    if(strlen($queue_id)<0)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    $sql="select pool.*,queue.nns_queue as nns_queue,queue.nns_name as queue_name from " . self::$base_table . " as pool left join nns_queue as queue on queue.nns_id=pool.nns_queue_id where queue.nns_queue='{$queue_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功".$sql,$result);
	}
	
	/**
	 * 查询全局错误日志列表
	 * @param object $dc 数据库对象
	 * @param array $params 查询参数
	 * @param array $page_info 分页信息
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query($dc,$params,$page_info=null)
	{
		if(isset($params['where']) && !empty($params['where']))
		{
			foreach ($params['where'] as $where_key=>$where_val)
			{
			    if(is_array($where_val))
			    {
				    self::$str_where.=" {$where_key} in ('".implode("','", $where_val)."') and ";
			    }
			    else
			    {
			        self::$str_where.=" {$where_key} = '{$where_val}' and ";
			    }
			}	
		}
		if(isset($params['like']) && !empty($params['like']))
		{
			foreach ($params['like'] as $like_key=>$like_val)
			{
				self::$str_where.=" {$like_key} like '%{$like_val}%' and ";
			}
		}
		self::$str_where = (strlen(self::$str_where) > 5) ? trim(self::$str_where,'and ') : '';
		$str_limit = self::make_page_limit($page_info);
		$sql="select * from " . self::$base_table . " " . self::$str_where . " order by nns_create_time desc {$str_limit} ";
		$sql_count = "select count(*) as count from " . self::$base_table . " " . self::$str_where;
		$result_count = nl_query_by_db($sql_count,  $dc->db());
		$result = nl_query_by_db($sql, $dc->db());
		self::init_where();
		if(!$result || !$result_count)
		{
			return self::return_data(1,"查询数据失败".$sql.$sql_count);
		}
		return self::return_data(0,"查询数据成功".$sql_count.$sql,$result,$result_count);
	}
	
	/**
	 * 查询是否存在
	 * @param unknown $dc
	 * @param string $str_param
	 */
	static public function query_unique($dc,$str_param=null)
	{
	    $sql="select count(*) as count from " . self::$base_table;
	    if(isset($str_param) && strlen($str_param))
	    {
	        $str_param = trim($str_param);
	    }
	    $is_set_where = substr($str_param, 0,5);
	    if(strtolower($is_set_where) != 'where')
	    {
	        $str_param = " where ".$str_param;
	    }
	    $str_param = (strtolower($is_set_where) != 'where') ? " where ".$str_param : " ".$str_param;
	    $sql = $sql.$str_param;
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,"查询数据失败".$sql);
	    }
	    return self::return_data(0,"查询数据成功".$sql,$result);
	}
	
	/**
	 * 查询所有
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_all($dc)
	{
		$sql="select * from " . self::$base_table;
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 依据条件查询数据  条件必须要存在
	 * @param unknown $dc
	 * @param unknown $arr_params
	 */
	static public function query_data($dc,$arr_params)
	{
	    $arr_params = self::except_useless_params(self::$arr_filed, $arr_params);
	    if(empty($arr_params) || !is_array($arr_params))
	    {
	        return self::return_data(0,'OK');
	    }
	    $arr_where = array();
	    foreach ($arr_params as $key=>$val)
	    {
	        if(is_array($val))
	        {
	            $arr_where[] = " {$key} in ('".implode("','", $val)."') ";
	        }
	        else
	        {
	            $arr_where[] = " {$key}='{$val}' ";
	        }
	    }
	    $str_where = empty($arr_where) ? '' : " where ".implode(" and ", $arr_where);
	    $sql="select * from " . self::$base_table . " {$str_where} ";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 根据GUID获取多个键值
	 * @param object $dc 数据库对象
	 * @param $arr_key_ids array 键值主键
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_condition($dc,$arr_key_ids=array(),$other_filter=array())
	{
		$sql = "select * from " . self::$base_table;
		$ids_str = '';
		if(!empty($arr_key_ids) && is_array($arr_key_ids))
		{
		   	$str_id = implode("','", $arr_key_ids);
			$ids_str = " nns_id in('{$str_id}') AND ";
		}
		$other_str = '';
		if(!empty($other_filter) && is_array($other_filter))
		{
			foreach ($other_filter as $oter=>$filter)
			{
				if(!empty($filter))
				{
					$other_str .= " {$oter} like '%{$filter}%' AND ";
				}
			}
		}
		$str_sql = $ids_str . $other_str;
		$str_sql = rtrim($str_sql," AND ");
		if(!empty($str_sql))
		{
			$sql .= " where {$str_sql}";
		}
		$result = nl_query_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		$result = (isset($result) && !empty($result)) ? $result : null;
		return self::return_data(0,'OK',$result);
		
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete($dc,$nns_id)
	{
		if(strlen($nns_id) <1)
		{
			return self::return_data(1,'全局错误日志_id为空');
		}
		$sql = "delete from " . self::$base_table . " where nns_id='{$nns_id}' ";
		$result = nl_execute_by_db($sql, $dc->db());
		if(!$result)
		{
			return self::return_data(1,'数据库查询失败,sql:'.$sql);
		}
		return self::return_data(0,'OK');
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete_by_date($dc,$date)
	{
	    if(strlen($date) <1)
	    {
	        return self::return_data(1,'$date为空');
	    }
	    $sql = "delete from " . self::$base_table . " where nns_create_time<'{$date}'";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK');
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete_by_value($dc,$queue_id,$value)
	{
	    $value = is_array($value) ? $value : array($value);
	    if(empty($value))
	    {
	        return self::return_data(0,'OK');
	    }
	    $sql = "delete from " . self::$base_table . " where nns_queue_id='{$queue_id}' and nns_value in('".implode("','", $value)."')";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK');
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function delete_by_queue_date($dc,$queue_id,$date)
	{
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    if(strlen($date) <1)
	    {
	        return self::return_data(1,'$date为空');
	    }
	    $sql = "delete from " . self::$base_table . " where nns_queue_id='{$queue_id}' and nns_create_time <'{$date}'";
	    $result = nl_execute_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK');
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_queue_date($dc,$queue_id,$date)
	{
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    if(strlen($date) <1)
	    {
	        return self::return_data(1,'$date为空');
	    }
	    $sql = "select * from " . self::$base_table . " where nns_queue_id='{$queue_id}' and nns_create_time >='{$date}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function query_by_queue_date_del($dc,$queue_id,$date)
	{
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    if(strlen($date) <1)
	    {
	        return self::return_data(1,'$date为空');
	    }
	    $sql = "select * from " . self::$base_table . " where nns_queue_id='{$queue_id}' and nns_create_time <'{$date}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    $result = (isset($result) && is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function timer_query_by_queue_id($dc,$queue_id)
	{
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    $sql = "select *  from " . self::$base_table . " where nns_queue_id='{$queue_id}' and nns_state='0'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK,sql:'.$sql,$result);
	}
	
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function timer_query_by_queue_id_v2($dc,$queue_id)
	{
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    $sql = "select *  from " . self::$base_table . " where nns_queue_id='{$queue_id}'";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK,sql:'.$sql,$result);
	}
	
	/**
	 * 删除
	 * @param object $dc 数据库对象
	 * @return Ambigous array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
	 * @author liangpan
	 * @date 2016-03-06
	 */
	static public function timer_state_not_in_unqueue_id($dc,$queue_id,$values,$state=0)
	{
	    $values = is_array($values) ? $values : array($values);
	    if(empty($values))
	    {
	        return self::return_data(1,'$values为空'.var_export($values,true));
	    }
	    if(strlen($queue_id) <1)
	    {
	        return self::return_data(1,'$queue_id为空');
	    }
	    $sql = "update from " . self::$base_table . " set nns_state='{$state}'  where nns_queue_id='{$queue_id}' and nns_values not in('".implode("','", $values)."')";
	    $result = nl_query_by_db($sql, $dc->db());
	    if(!$result)
	    {
	        return self::return_data(1,'数据库查询失败,sql:'.$sql);
	    }
	    return self::return_data(0,'OK,sql:'.$sql,$result);
	}
}
