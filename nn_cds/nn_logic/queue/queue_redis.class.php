<?php
/**
 * 队列模板管理
 * @author liangpan
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_queue_redis extends nl_public
{
	/**
	 * 获取LIST 的数量
	 * @param unknown $redis_obj
	 * @param unknown $queue_model
	 */
	static public function get_list_count($redis_obj,$queue_model)
	{
	    $result = $redis_obj->lSize($queue_model);
	    $result = $result ? (int)$result : 0;
	    $result = $result >=0 ? $result : 0;
		return self::return_data(0,'OK',$result);
	}
	
	/**
	 * 获取LIST 的列表
	 * @param unknown $redis_obj
	 * @param unknown $queue_model
	 */
	static public function get_list($redis_obj,$queue_model)
	{
	    $result_count = self::get_list_count($redis_obj, $queue_model);
	    if($result_count['data_info'] <1)
	    {
	        return self::return_data(0,'OK');
	    }
	    $result = $redis_obj->lrange($queue_model,0,$result_count['data_info']);
	    $result = (is_array($result) && !empty($result)) ? $result : null;
	    return self::return_data(0,'OK',$result);
	}
	
	
	/**
	 * 推入数据
	 * @param unknown $redis_obj
	 * @param unknown $queue_model
	 */
	static public function push($redis_obj,$queue_model,$val)
	{
	    if(strlen($val)<1)
	    {
	        return self::return_data(1,'值为空');
	    }
	    $result = $redis_obj->lpush($queue_model,$val);
	    if(!$result)
	    {
	        return self::return_data(1,'插入队列池失败');
	    }
	    return self::return_data(0,'插入队列池成功');
	}
	
	/**
	 * 弹出数据
	 * @param unknown $redis_obj
	 * @param unknown $queue_model
	 */
	static public function pop($redis_obj,$queue_model)
	{
	    $result = $redis_obj->rpop($queue_model);
	    if(!$result)
	    {
	        return self::return_data(1,'弹出队列池失败');
	    }
	    return self::return_data(0,'弹出队列池成功',$result);
	}
	
	/**
	 * 弹出数据
	 * @param unknown $redis_obj
	 * @param unknown $queue_model
	 */
	static public function delete($redis_obj,$queue_model,$value)
	{
	    $result = $redis_obj->lremove($queue_model,$value,1);
	    if(!$result)
	    {
	        return self::return_data(1,'删除队列池失败');
	    }
	    return self::return_data(0,'删除队列池成功',$result);
	}

    /**
     * 删除Redis
     * @param $redis_obj
     * @param $queue_model
     * @return array
     */
    static public function delete_key($redis_obj,$queue_model)
    {
        $result = $redis_obj->delete($queue_model);
        if(!$result)
        {
            return self::return_data(1,'删除队列池失败');
        }
        return self::return_data(0,'删除队列池成功',$result);
    }
}
