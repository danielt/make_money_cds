<?php

/*
 * Created on 2012-10-10
 * BY S67
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

/**
 * 根据当前配置获取底层数据操作模块
 * @param Array(
 * 			'db_policy'=>db配置，标识读或写，NL_DB_WRITE | NL_DB_READ
 * 			'cache_policy'=>cache配置,标识cache策略 NP_KV_CACHE_TYPE_NULL | NP_KV_CACHE_TYPE_MEMCACHE | NP_KV_CACHE_TYPE_MEMCACHED
 * 			)
 * @param Array(
 * 			'db_log'=>  数据库操作调试参数 NP_DB_LOG_NULL | NP_DB_LOG_ERROR | NP_DB_LOG_ALL
 * 			'cache_type'=>  cache存储类型 NP_KV_CACHE_MAP_ALL | NP_KV_CACHE_MAP_HASH
 * 			)
 * @return nl_dc 数据底层操作模块
 */
function nl_get_sub_platform_dc($config, $params = NULL) {
        $db_obj = new nl_dc($config, $params, 'sub_platform');

        return $db_obj;
}

/*
 * ********************************************************************************
 * 	DB模块相关方法
 * 	nl_get_db
 * 	nl_get_db_config
 * 	nl_get_db_w_config_list
 * 	nl_get_db_r_config_list
 * 	nl_query_by_db
 * 	nl_execute_by_db
 * ******************************************************************************* */



/**
 * 根据读写类型获取数据库当前采用配置
 * @param String 数据库类型读或写 NL_DB_WRITE | NL_DB_READ
 * @return Array 当前数据库采用配置
 */
function nl_get_sub_platform_db_config($db_policy) {
	$c=NULL;
	if (defined('g_sub_platform_enable') && g_sub_platform_enable==1){
        $w_configs = nl_get_sub_platform_db_w_config_list();
        $r_configs = nl_get_sub_platform_db_r_config_list();

        $configs = nl_get_active_sub_platform_db_config_by_ini($w_configs, $r_configs);

        $w_active_configs = $configs['write'];
        $r_active_configs = $configs['read'];
		
		
		if (defined('g_sub_platform_db_mode')){
	        if ($db_policy === NL_DB_WRITE) {
	                $c = _get_w_sub_platform_db_config($w_active_configs, $r_active_configs);
	        } elseif ($db_policy === NL_DB_READ) {
	                $c = _get_r_sub_platform_db_config($w_active_configs, $r_active_configs);
	        }
		}

	}
//		BUG 修复 方法调用错误，这里会一直返回NULL BY S67 2013-12-3
//        if ($c === NULL) {
//                if ($db_policy === NL_DB_WRITE) {
//                        $c = _get_w_db_config($w_configs, $r_configs);
//                } elseif ($db_policy === NL_DB_READ) {
//                        $c = _get_r_db_config($w_configs, $r_configs);
//                }
//        }

		if ($c===NULL){
			 $c=nl_get_db_config($db_policy);
		}

        return $c;
}

function _get_r_sub_platform_db_config($w_configs, $r_configs) {
        switch (g_sub_platform_db_mode) {
                case 0:
                        $c = _get_w_sub_platform_db_config($w_configs, $r_configs);
                        break;
                case 1:
                        if (count($r_configs) > 0) {
                                //		从DB配置组中获取DB写模块配置，当前为默认选择第一个。
                                $c = $r_configs[0];
                        } else {
                                $c = _get_w_sub_platform_db_config($w_configs, $r_configs);
                        }

                        break;
                case 2:
                        $c = _get_w_sub_platform_db_config($w_configs, $r_configs);
                        if ($c === NULL) {
                                if (count($r_configs) > 0) {
                                        $c = $r_configs[0];
                                } else {
                                        $c = NULL;
                                }
                        }
                        break;
                case 30:
                	if (count($r_configs) > 0) {
                		$c =  _get_random_db_config($r_configs);
                	}else{
                		$c = NULL;
                	}
                break;
                
                case 31:
               	 	if (count($r_configs) > 0) {
                		$c = $r_configs[0];
               	 	}else{
               	 		$c = NULL;
               	 	}
                break;
        }

        if (is_array($c))
                $c['mode'] = 'READ';

        return $c;
}

function _get_w_sub_platform_db_config($w_configs, $r_configs) {
        $c = NULL;
        switch (g_sub_platform_db_mode) {
                case 0:
                        if (count($w_configs) > 0) {
                                //		从DB配置组中获取DB写模块配置，当前为默认选择第一个。
                                $c = $w_configs[0];
                                $c['mode'] = 'WRITE';
                        }
                        break;
                case 1:
                        if (count($w_configs) > 0) {
                                //		从DB配置组中获取DB写模块配置，当前为默认选择第一个。
                                $c = $w_configs[0];
                                $c['mode'] = 'WRITE';
                        }
                        break;
                case 2:
                        if (count($w_configs) > 0) {
                                //		从DB配置组中获取DB写模块配置，当前为默认选择第一个。
                                $c = $w_configs[0];
                                $c['mode'] = 'WRITE';
                        } else {
                                if (count($r_configs) > 0) {
                                        $c = $r_configs[0];
                                        $c['mode'] = 'READ';
                                }
                        }
                        break;
               case 30:
                	if (count($r_configs) > 0) {
                		$c =  _get_random_db_config($r_configs);
                		$c['mode'] = 'READ';
                	}else{
                		$c = NULL;
                	}
                break;
                
                case 31:
               	 	if (count($r_configs) > 0) {
                		$c = $r_configs[0];
                		$c['mode'] = 'READ';
               	 	}else{
               	 		$c = NULL;
               	 	}
                break;
        }

        return $c;
}

function nl_get_sub_platform_db($db_policy, $params = NULL) {

        $c = nl_get_sub_platform_db_config($db_policy);


        if (is_array($params)) {
                if (array_key_exists('db_log', $params)) {
                        $c['db_log'] = $params['db_log'];  //	重构数据模块调试参数
                }
        }


        $db = np_db_factory_create($c);

        return $db;
}

/**
 * 获取当前能用的配置
 * @param Array 配置组
 * @return Array 当前数据库采用配置
 */
function nl_get_active_sub_platform_db_config_by_ini($wconfigs, $rconfigs) {
        $lines = array();
        $db_ini = dirname(dirname(__FILE__)) . DIRECTORY_SEPARATOR . 'nn_cms_config' . DIRECTORY_SEPARATOR . 'global_sub_platform_db_ini';
        if (file_exists($db_ini)) {
                $handle = @fopen($db_ini, "r");
                if ($handle) {
                        while (!feof($handle)) {
                                $lines[] = fgets($handle, 4096);
                        }
                        fclose($handle);
                }
        } else {
                return array('write' => $wconfigs, 'read' => $rconfigs);
        }
//	var_dump($lines);die;
        $active_w_indexse = null;
        $active_r_indexse = null;
        if (empty($lines))
                return array('write' => $wconfigs, 'read' => $rconfigs);

        $lines[0] = trim($lines[0]);
        $lines[1] = trim($lines[1]);
        $lines[2] = trim($lines[2]);


        if (strtotime(date('YmdHis')) - strtotime($lines[0]) > 10) {
                return array('write' => $wconfigs, 'read' => $rconfigs);
        }

        $active_w_indexse = ',' . $lines[1] . ',';
        $active_r_indexse = ',' . $lines[2] . ',';


        $active_wconfigs = array();
        foreach ($wconfigs as $w_key => $w_value) {
                if (strpos($active_w_indexse, ',' . $w_key . ',') !== FALSE) {
                        array_push($active_wconfigs, $w_value);
                }
        }

        $active_rconfigs = array();
        foreach ($rconfigs as $r_key => $r_value) {
                if (strpos($active_r_indexse, ',' . $r_key . ',') !== FALSE) {
                        array_push($active_rconfigs, $r_value);
                }
        }


        return array('write' => $active_wconfigs, 'read' => $active_rconfigs);
}

/**
 * 获取当前写类型数据库配置列表
 * @return Array 数据库配置组
 */
function nl_get_sub_platform_db_w_config_list() {
        $configs = array();
        for ($i = 1; $i <= 32; $i++) {
                if (defined('g_sub_platform_db_w_host' . $i)) {
                        $c = np_db_build_config(
                                constant('g_sub_platform_db_w_host' . $i), constant('g_sub_platform_db_w_username' . $i), constant('g_sub_platform_db_w_password' . $i), constant('g_sub_platform_db_w_name' . $i), NP_DB_TYPE_MYSQL, NP_DB_LOG_NULL
                        );
                        if (isset($c)) {
                                array_push($configs, $c);
                        }
                }
        }
        return $configs;
}

/**
 * 获取当前读类型数据库配置列表
 * @return Array 数据库配置组
 */
function nl_get_sub_platform_db_r_config_list() {
        $configs = array();
        for ($i = 1; $i <= 32; $i++) {
                if (defined('g_sub_platform_db_r_host' . $i)) {
                        $c = np_db_build_config(
                                constant('g_sub_platform_db_r_host' . $i), constant('g_sub_platform_db_r_username' . $i), constant('g_sub_platform_db_r_password' . $i), constant('g_sub_platform_db_r_name' . $i), NP_DB_TYPE_MYSQL, NP_DB_LOG_NULL
                        );
                        if (isset($c)) {
                                array_push($configs, $c);
                        }
                }
        }
        return $configs;
}

/**
 * 获取cache操作模块
 * @param String cache类型 NP_KV_CACHE_TYPE_NULL | NP_KV_CACHE_TYPE_MEMCACHE | NP_KV_CACHE_TYPE_MEMCACHED
 * @param Array(
 * 			'cache_type'=> cache存储类型 NP_KV_CACHE_MAP_ALL | NP_KV_CACHE_MAP_HASH
 * 			)
 * 			cache层行为控制参数组
 * @return cache cache操作模块,NULL为无MEMCACH配置或MEMCACHE链接失败
 */
function nl_get_sub_platform_cache($cache_policy = NULL, $params = NULL) {

        if (nl_config::sub_platform_cache_server()===NULL) {

                return np_kv_cache_factory_create(
                        np_kv_cache_build_config(
                                NP_KV_CACHE_TYPE_NULL, NP_KV_CACHE_MAP_ALL, array()
                )); //未定义memcache地址，则返回
        }

        $servers = explode(';', rtrim(nl_config::sub_platform_cache_server(), ';'));

        if (empty($cache_policy) && $cache_policy !== NP_KV_CACHE_TYPE_NULL) {
                $cache_policy = NP_KV_CACHE_TYPE_MEMCACHE;  //未传入CACHE类型时，默认为 NP_KV_CACHE_TYPE_MEMCACHE
        }

        if (empty($params)) {
                $cache_type = NP_KV_CACHE_MAP_ALL;  //未传入存储类型时，默认为 NP_KV_CACHE_MAP_ALL
        } else {
                if (!array_key_exists('cache_type', $params)) {
                        $cache_type = NP_KV_CACHE_MAP_ALL;   //未传入存储类型时，默认为 NP_KV_CACHE_MAP_ALL
                } else {
                        $cache_type = $params['cache_type'];
                }
        }



        $c = np_kv_cache_build_config(
                $cache_policy, $cache_type, $servers
        );

        $cache = np_kv_cache_factory_create($c);

        return $cache;
}


?>
