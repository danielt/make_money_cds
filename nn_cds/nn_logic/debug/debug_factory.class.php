<?php

class nl_debug_factory
{
    static private $debuger = NULL;
    //是否开始调用xhprof性能分析
    static private $is_xhprof_start = FALSE;

    static public function create($type = 'xdebug')
    {
//		if (self::$debuger!==NULL) return TRUE;

        if (empty($type)) return FALSE;

        $model_file = dirname(__FILE__) . '/' . $type . '_model.class.php';
// var_dump($model_file);die;
        if (!file_exists($model_file)) return FALSE;

        include_once $model_file;

        $class_name = $type . '_model';
        if (!class_exists($class_name)) return FALSE;
        if (!is_array(self::$debuger))
        {
            self::$debuger = array();
        }

        self::$debuger[$type] = new $class_name();
        
        self::open_debug();
        return TRUE;


    }


    static public function output($modes)
    {
        $output = array();
        if (is_array(self::$debuger))
        {
            foreach (self::$debuger as $key => $item)
            {

                if ($item->check())
                {

                    $output[$key] = $item->output($modes[$key]);
                }
            }
        }

        return $output;
    }

    static public function close_debug()
    {
        if (is_array(self::$debuger))
        {
            foreach (self::$debuger as $item)
            {
                if ($item->check())
                {
                    $item->close();
                }
            }
        }
    }

    static public function open_debug()
    {
    	if (is_array(self::$debuger))
        {
            foreach (self::$debuger as $item)
            {
                if ($item->check())
                {
                	$item->open();
                }
            }
        }
    }
		
		/*
     *<code_review> 2014-8-28 ming.li
     *<question> 下面两个函数需格式化，注意：函数跟左括号没有空格，大括号要另起一行
     *<update> liangpan 2014-09-01
     */
	/**
     * @author huayi.cai
     * @date 2014-8-27
     * 开起xhprof调试
     */
	static public function xhprof_start()
	{
		if (!extension_loaded("xhprof"))
		{
			die("请安装xhprofphp扩展,才能使用此调试模块");
		}
		//启动 xhprof 进行性能分析。
		//XHPROF_FLAGS_CPU使输出的性能数据中添加 CPU 数据。
		//XHPROF_FLAGS_MEMORY使输出的性能数据中添加内存数据。
		xhprof_enable(XHPROF_FLAGS_CPU + XHPROF_FLAGS_MEMORY);
		self::$is_xhprof_start = TRUE;
	}

	/**
     * @author huayi.cai
     * @date 2014-8-27
     * 生成xhprof调试报告
     */
	static public function xhprof_output()
	{
		//开始分析，才输出报告
		if (self::$is_xhprof_start == FALSE)
			return;
		
		if (!extension_loaded("xhprof"))
		{
			return null;
		}
		//停止性能分析，并返回此次运行的 xhprof 数据。
		$data = xhprof_disable();
		
		//得到统计数据之后，以下的工作就是为页面显示做准备。
		//这里填写的就是你的xhprof的路径
		$xhprof_root = "/data/starcor/www/nn_cms/data/xhprof/";
		//xhprof报告web访问地址
		$xhprof_server = "http://" . $_SERVER['HTTP_HOST'] . "/nn_cms/data/xhprof/";
		//显示性能分析数据
		include_once $xhprof_root . "xhprof_lib/utils/xhprof_lib.php";
		include_once $xhprof_root . "xhprof_lib/utils/xhprof_runs.php";
		$objXhprofRun = new XHProfRuns_Default();
		
		//第一个参数 是xhprof_disable()的返回值
		//第二个参数 是自定义命名空间字符串
		//返回运行id，用这个id查看相关运行结果
		$run_id = $objXhprofRun->save_run($data, "xhprof");
		
		//查看运行结果的url
		//run的值来源于save_run的返回值
		//source值来源于save_run的第二个参数
		$url = $xhprof_server . "xhprof_html/index.php?run=" . $run_id . "&source=xhprof";
		//echo "<br/><a href='http://{$url}' target='_blank' style='font-size:16px;'>查看xhprof性能报告</a></br>";
		echo "<br><br>";
		echo "<h1 style='font-size:16px;'>XHPORF性能分析报告</h1>";
		echo file_get_contents($url);
	}

}

?>