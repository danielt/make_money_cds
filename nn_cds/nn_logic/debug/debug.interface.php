<?php
/*
 * Created on 2014-6-23
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
 define('NX_DEBUG_TRACE_LOG_PATH',dirname(dirname(dirname(__FILE__))).'/data/log/debug');
 interface debug{
 	
 	public function open();
 	
 	public function close();
 	
 	public function check();
 	
 	public function output($mode);
 }
?>
