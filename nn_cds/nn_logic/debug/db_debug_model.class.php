<?php
/**
 * @author S67
 * XDEBUG PHP检测调试工具
 */
 include_once 'debug.interface.php';
 include_once dirname(dirname(__FILE__)).'/nl_dc.class.php';
 include_once dirname(__FILE__).'/db_debug/db_debug_table.style.php';
class db_debug_model implements debug {
	
    public function __construct() {
    	
    }
    
    public function open(){
    	nl_dc::$db_debug_enable=1;
    	return;
    }
    
    public function close(){
    	return;
    }
    
    public function check(){
    	return TRUE;
    }
    
    public function output($mode){
    	$record=$this->__build_data();
    	return __db_debug_build_table($record);
    }
    
    private function __build_data(){
    	$result=array();
    	if (is_array(nl_dc::$db_debug_arr))
    	{
    		$result['data']=array();
    		foreach (nl_dc::$db_debug_arr as $db){
    			
    			$config=$db->get_db_config();
    			$host_ip=$config['host'];
    			$debug_info=$db->get_db_debug();
    			foreach ($debug_info as $debug){
//     				var_dump(strtolower(strpos($debug['SQL']), "nns_mgtvbk_c2_task"),'testtesttest');
    				if(strpos(strtolower($debug['SQL']), "nns_mgtvbk_c2_task") === false)
    				{
//     					continue;
    				}
    				$re=array();
    				$re['ip']=$host_ip;
    				$re['sql']=$debug['SQL'];
    				$re['desc']=$debug['debug'];
    				$re['time']=round($debug['time']*1000,3);
    				$re['explain_debug']=$debug['explain_debug'];
    				$result['data'][]=$re;
    			}
    		}
    	}
    	
    	return $result;
    }
    
    
}
?>