<?php
/*
 * Created on 2014-6-24
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
define ('NX_XDEBUG_TIP_TIME_PERCENT',10); 
define ('NX_XDEBUG_TIP_MEM_PERCENT',10); 
define ('NX_XDEBUG_TIP_EXECUTE_NUM',10); 
function  __xdebug_build_table($data){
$html=
<<<STYLE
<style>
*{
	font-family:'微软雅黑';
	padding: 0px 0px;
	margin:0px 0px;
	
}
a,p{
	font-size:12px;
	color:#333333;
}
pre{
	font-size:11px;
	color:#ff4100;
	padding:5px;
	background-color:#d4d4d4;
	white-space: pre-wrap;
	word-wrap: break-word;
}
.block{
	background:#eeeeee;
	font-size:12px;
}
table{
	font-size:12px;
	width:100%;
	background:#cccccc;
}
table th{
	background:#e4e4e4;
	font-weight:bold;
}
table td{
	background:#ffffff;
	padding:0px 5px;
	word-break:break-all;
	height:30px;
}
.time{
	font-size:11px;
}
</style>
STYLE;
// 		$html .= "<h1>XDEBUG 调试信息</h1>";
 		$html .= "<p>".date('Y-m-d H:i:s')." 总时间:{$data['total_time']}ms 总内存:{$data['total_mem']}kb</p>";
 		$html_float ="<div style='position:absolute; background-color:#fff; width:500px; border: 5px solid #cfcfcf;display:none;' id='xdebug_function_detail_box'>";
		$html .="<table width='100%' border='0' cellpadding='0' cellspacing='1'><tr>" .
				"<th></th>" .
				"<th>方法名</th>" .
				"<th>执行次数</th>" .
				"<th>实行时间(ms)</th>" .
				"<th>时间占比</th>" .
				"<th>内存消耗(kb)</th>" .
				"<th>内存占比</th>" .
				"<th>文件名</th>" .
				"<th>行数</th>" .
				"</tr>";
		if (is_array($data['data'])){
			foreach ($data['data'] as $key=>$item){
				
				$html_tr="<tr style=''>";
				$level=__xdebug_create_table_level_state($item['level'],$data['min_level']);
				$html_tr .="<td style='over-flow:hidden;height:30px;'>{$level}</td>";
				
				
				$filename_link=$item['func_name'];
				if ($item['func_type']==1 && empty($item['include'])){
					
					if (is_array($item['arg_list'])){
						
						$html_float .="<div id='xdebug_detail_{$key}' class='xdebug_func_detail' style='padding:10px;'>" .
								"<h4><b>{$item['func_name']}</b></h4><hr/>" .
								"<span><b>参数传入</b></span>" ;
						
						$arg_command='';
						foreach ($item['arg_list'] as $k=>$arg){
//							if (empty(trim($arg))) continue;
							$arg_command .="{$arg},";
							$html_float .="<p>参数{$k}:<pre> {$arg}</pre></p>";
						}
						
						$arg_command=rtrim($arg_command,',');
						$return='';
//						var_dump("\$return=".$item['func_name']."({$arg_command});");
//						eval("\$return=".$item['func_name']."({$arg_command});");

						if (!empty($item['return'])){
							$html_float .="<span><b>函数返回</b></span>".
									  "<p><pre>{$item['return']}</pre></p>";
						}
												
						$html_float .=
//									"<span><b>函数返回</b></span>".
//									  "<p>{$item['return']}</p>".
									  "</div>";
									  
					    $filename_link="<a href='javascript:xdebug_show_detail(\"{$key}\");' id='xdebug_link_{$key}'>{$item['func_name']}</a>";
					}
				}
				
				
				$html_tr .="<td>{$filename_link}  {$item['include']}</td>";
				
				$key=$item['file_name'].'|#'.$item['file_line'];
				$execute_num=$data['execute_status'][$key]['execute_num'];
				if ($execute_num>=NX_XDEBUG_TIP_EXECUTE_NUM){
					$color=__xdebug_get_color_by_num($item['file_line']);
					$time_style=" style='background-color:{$color};color:#ffffff'";
				}else{
					$time_style='';
				}
				$html_tr .="<td {$time_style}>{$execute_num}</td>";
				
				$html_tr .="<td>{$item['execute_time']}</td>";
				$time_percent=round(($item['execute_time']/$data['total_time'])*100,2);
				if ($time_percent>=NX_XDEBUG_TIP_TIME_PERCENT){
					$time_style=" style='background-color:#ff7f40;color:#ffffff'";
				}else{
					$time_style='';
				}
				$html_tr .="<td {$time_style}>{$time_percent}%</td>";
				$html_tr .="<td>{$item['execute_mem']}</td>";
				$mem_percent=round(($item['execute_mem']/$data['total_mem'])*100,2);
				if ($mem_percent>=NX_XDEBUG_TIP_MEM_PERCENT){
					$mem_style=" style='background-color:#ff7f40;color:#ffffff'";
				}else{
					$mem_style='';
				}
				
				$html_tr .="<td {$mem_style}>{$mem_percent}%</td>";
				$html_tr .="<td>{$item['file_name']}</td>";
				$html_tr .="<td>{$item['file_line']}</td>";
				$html_tr .="</tr>";
				$html .= $html_tr;
				
				
				
				
			}
		}
//		die;
		unset($data);
		
		$html .="</table>";
		$html_float .="</div>";	
		$html_script = '<script language="javascript" src="'.get_config_v2('g_webdir').'nn_cms_manager/js/jquery-1.6.2.min.js"></script>'; 
		$html_script .= 
<<<JS

<script>
		function xdebug_show_detail(id){
			var obj=document.getElementById("xdebug_function_detail_box");
			$(".xdebug_func_detail").hide();
			$("#xdebug_detail_"+id).show();
			
			
			var top=$("#xdebug_link_"+id).offset().top+$("#xdebug_link_"+id).parent().height();
   		    var left=$("#xdebug_link_"+id).offset().left+$("#xdebug_link_"+id).width();
			
			obj.style.left=left+"px";
			obj.style.top=top+"px";
			
			xdebug_tagg_close_open_detail();
		};
		function xdebug_tagg_close_open_detail(){
			var obj=document.getElementById("xdebug_function_detail_box");
			if (obj.style.display=="none"){
				obj.style.display="";
			}else{
				obj.style.display="none";
			}
		}
     </script>
JS;
		
		$html.=$html_float;
		$html =$html_script.$html;
		return $html;

}
 
 
 
function __xdebug_create_table_level_state($level,$min_level){		
		$level_html='';
		for($i=0;$i<=$level-$min_level;$i++){
			$color=__xdebug_get_color_by_num($i);
			$level_html .="<div style='float:left;width:5px;height:30px;background-color:{$color}'></div>";
		}
		return $level_html;
	}
	
function __xdebug_get_color_by_num($num){
	$color_arr=array('#008081','#00be40','#bfc041','#ffff01','#ffbf41','#ff7f40','#ff3f40','#fe0000','#be0040','#81007f','#4100c0','#0000fe');
	$color_len=count($color_arr);
		
	if ($num>=$color_len){
		$j = $num%$color_len;
	}else{
		$j=$num;
	}
	$color=$color_arr[$j];	
	return $color;
}
?>
