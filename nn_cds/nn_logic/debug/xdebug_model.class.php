<?php
/**
 * @author S67
 * XDEBUG PHP检测调试工具
 */
 include_once 'debug.interface.php';
// 简单输出，只输出自定义函数
 define ('NX_XDEBUG_TRACE_MODE_SIMPLE',0);
// 输出自定义函数及INCLUDE语句
 define ('NX_XDEBUG_TRACE_MODE_WITH_INCLUDE',1);
// 输出全部函数
 define ('NX_XDEBUG_TRACE_MODE_WITH_ALL',2);
 
class xdebug_model implements debug {
	
	private $log_path;
	private $log_file;
	private $trace_mode;
	public function __construct(){
		$this->log_path=NX_DEBUG_TRACE_LOG_PATH.'/xdebug/'.date('Ymd');
		if (!is_dir($this->log_path)){
			$old=umask(0);
			@mkdir($this->log_path,0777,true);
			chmod($this->log_path,0777);
			umask($old);
		}
	}
	
	public function open(){
		$request_uri=$_SERVER['REQUEST_URI'];
		$file_name='trace.';
		if (!empty($request_uri)){
			$request_uri=explode('?',$request_uri);
			$file_name .=preg_replace('/[\/|\.]/i','_',$request_uri[0]).'.';
		}		
		$file_name .= date('YmdHis');
		$this->log_file=$this->log_path.'/'.$file_name;
//		var_dump($this->log_file);die;
		xdebug_start_trace($this->log_file,2);
	}
	
	public function close(){
		xdebug_stop_trace();
	}
	
	public function output($mode){
		$this->close();
		$mode=empty($mode)?NX_XDEBUG_TRACE_MODE_SIMPLE:$mode;
		$this->trace_mode=$mode;
		return $this->__create_table_statistics();
	}
	
	public function check(){
		if (function_exists('xdebug_start_trace')){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	
	
	
	private function __create_table_statistics(){
		if (file_exists($this->log_file.'.xt')){
			$debug_file = $this->log_file.'.xt';
			$record=$this->__read_file($debug_file);
			
			include_once 'xdebug/xdebug_table.style.php';
			
			return __xdebug_build_table($record);
		}else{
			return '';
		}
	}
	

	
	
	
	
	
	private function __read_file($file_path){
		$file = fopen($file_path, "r") or exit("Unable to open file!");
 		$record=array();
 		$num=0;
 		$begin_time=0;
 		$end_time=0;
 		$begin_mem=0;
 		$end_mem=0;
 		$min_level=1;
 		
 		$execute_status=array();
 		
 		
  		while(!feof($file))
  		{
  			$line=fgets($file);
  			$lines = explode('	',$line);
			
			if (!is_numeric($lines[0])) continue;
			
			if ($this->trace_mode==NX_XDEBUG_TRACE_MODE_SIMPLE)
			{
				if (!empty($lines[7])) continue;
			}
  				
  				if ($num==0)
  				{
  					$begin_time=$lines[3];
  					$end_mem=$lines[4];
  					$min_level=$lines[0];
  				}
  				
  				if ($lines[3]>$end_time){
  					$end_time=$lines[3];
  				}
  				
  				if ($lines[4]>$end_mem){
  					$end_mem=$lines[4];
  				}
  				
  				
  				if ($lines[0]<$min_level){
  					$min_level=$lines[0];
  				}
  				
  				$num++;
  				
  				if ($lines[2]==1)
	  			{
	  					
		  			if (isset($record[$lines[1]]))
		  			{
		  					
		  					$record[$lines[1]]['execute_time']=round(($lines[3]-$record[$lines[1]]['time'])*1000,3);
		  					$record[$lines[1]]['execute_mem']=round(($lines[4]-$record[$lines[1]]['mem'])/1000,3);
		  			}
		  			continue;
		  		}
		  		
		  		if (strtolower($lines[2])=='r')
		  		{
		  			if (isset($record[$lines[1]]))
		  			{
		  					
		  					$record[$lines[1]]['return']=$lines[5];
//		  					$record[$lines[1]]['execute_mem']=round(($lines[4]-$record[$lines[1]]['mem'])/1000,3);
		  			}
		  			continue;
		  		}
  				
  			if ($lines[6]==1 || $this->trace_mode==NX_XDEBUG_TRACE_MODE_WITH_ALL)
  			{	
	  			$item=array();
	  			$item['level']=$lines[0];
	  			$item['action']=$lines[2];
	  			$item['time']=$lines[3];
	  			$item['mem']=$lines[4];
	  			$item['func_name']=$lines[5];
	  			$item['func_type']=$lines[6];
	  			$item['include']=$lines[7];
	  			$item['file_name']=$lines[8];
	  			$item['file_line']=$lines[9];
	  			$arg_num=$lines[10];
	  			$arg_list=array();
	  			for ($arg_index=1;$arg_index<=$arg_num;$arg_index++){
	  				$arg_list[$arg_index-1]=$lines[$arg_index+10];
	  			}
	  			$item['arg_list']=$arg_list;
	  			$item['num']=$num;
	  			
	  			$key=$lines[8].'|#'.$lines[9];
	  			
	  			if(!isset($execute_status[$key])){
	  				$execute_status[$key]=array();
	  				$execute_status[$key]['execute_num']=0;
	  			}
	  			
	  			$execute_status[$key]['execute_num']++;
	  			
	  			$record[$lines[1]]=$item;
	  			unset($line);
	  			unset($lines);
	  			unset($item);
	  			
  			}
  		}
  		
  		
  		$total_mem=round(($end_mem-$begin_mem)/1000,3);
  		$total_time=round(($end_time-$begin_time)*1000,3);
  		return array(
  			'min_level'=>$min_level,
  			'total_time'=>$total_time,
  			'total_mem'=>$total_mem,
  			'execute_status'=>$execute_status,
  			'data'=>$record
  		);
	}
}
?>