<?php
/*
 * Created on 2014-6-24
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */
define ('NX_XDEBUG_TIP_TIME_PERCENT',10); 
define ('NX_XDEBUG_TIP_MEM_PERCENT',10); 
define ('NX_XDEBUG_TIP_EXECUTE_NUM',10); 
function  __db_debug_build_table($data){
$html="<style>
*{
	font-family:'微软雅黑';
	padding: 0px 0px;
	margin:0px 0px;
	
}
a,p{
	font-size:12px;
	color:#333333;
}
.block{
	background:#eeeeee;
	font-size:12px;
}
table{
	font-size:12px;
	width:100%;
	background:#cccccc;
}
table th{
	background:#e4e4e4;
	font-weight:bold;
}
table td{
	background:#ffffff;
	padding:0px ;
	word-break:break-all;
	height:30px;
}
.time{
	font-size:11px;
}
</style>";
// 		$html .= "<h1>XDEBUG 调试信息</h1>";
 		$html .= "<p>".date('Y-m-d H:i:s')."</p>";
		
		if (is_array($data['data'])){
			foreach ($data['data'] as $item){
				$html .="<table width='100%' border='0' cellpadding='0' cellspacing='1'><tr>" .
				"<th>数据库</th>" .
				"<th>SQL语句</th>" .
				"<th>执行状态</th>" .
				"<th>实行时间(ms)</th>" .
				"</tr>";
				$html_tr="<tr style=''>";
				$html_tr .="<td style='over-flow:hidden;height:30px;'>{$item['ip']}</td>";
				$html_tr .="<td style='width:800px;'>{$item['sql']}</td>";
				
				if (strtoupper($item['desc'])!='OK'){
					$time_style=" style='background-color:#ff7f40;color:#ffffff'";
				}else{
					$time_style="";
				}
				$html_tr .="<td {$time_style}>{$item['desc']}</td>";
				
				$html_tr .="<td>{$item['time']}</td>";
				
				$html_tr .="</tr>";
				
				if (is_array($item['explain_debug']))
				{
					$html_tr .="<tr><td colspan=4 style='height:20px;'><b>索引检查</b></td></tr>";
					$html_tr .="<tr><td colspan=4 style='padding:0px;'><table width='100%' border='0' cellpadding='0' cellspacing='1'>";
					$html_tr .="<tr><th>表名</th>";
					$html_tr .="<th>连接类型</th>";
					$html_tr .="<th>可用索引</th>";
					$html_tr .="<th>实际使用索引</th>";
					$html_tr .="<th>索引长度</th>";
					$html_tr .="<th>索引列</th>";
					$html_tr .="<th>受影响行数</th>";
					$html_tr .="<th>解析方式</th></tr>";
					foreach ($item['explain_debug'] as $explain)
					{
						$html_tr .="<tr>";
//						var_dump($explain);die;
						$html_tr .="<td>{$explain['table']}</td>";
						$html_tr .="<td>{$explain['type']}</td>";
						$html_tr .="<td>{$explain['possible_keys']}</td>";
						$html_tr .="<td>{$explain['key']}</td>";
						$html_tr .="<td>{$explain['key_len']}</td>";
						$html_tr .="<td>{$explain['ref']}</td>";
						$html_tr .="<td>{$explain['rows']}</td>";
						$html_tr .="<td>{$explain['Extra']}</td>";
						$html_tr .="<tr>";
					}
					$html_tr .= "</table></td></tr>";
				}
				
				$html .= $html_tr;
				$html .="</table>";
				$html .="<div style='width:100%;height:2px;background-color:#00be40'></div>";
			}
		}
		
		unset($data);
		
		
		return $html;

}
 
 
 

?>
