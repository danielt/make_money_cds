<?php

/*
 * Created on 2013-7-25
 *
 * To change the template for this generated file go to
 * Window - Preferences - PHPeclipse - PHP - Code Templates
 */

class nl_view_cache
{
	
	public static  $proxy_key = '';
	
	
    const CACHE_TIME_OUT = 60;
    public static $except_keys = array (
                                "nns_user_id",
                                "nns_token",
                                "nns_mac_id",
                                "nns_smart_card_id",
                                "nns_device_id",
                                "nns_user_password",
                                "nns_webtoken",
                                "nns_ex_data",
                                "nns_net_id",
                                "nns_ex_data"
                            );

    /*
     *<code_review> 2014-8-20 ming.li
     *<question> 代码需格式化，需添加函数注释
     *<update>已加注释，已格式化 陈波 2014-08-26
     */
    /**
     * MEMCACHE缓存设置
     * @author 陈波
     * @date   2014-08-26
     * @param object       $dc
     * @param string       $key        MEMCACHE 键
     * @param string|array $value      MEMCACHE 值
     * @param string       $cache_time 缓存时间
     * @return true|false
     * */
    public static function set ($dc, $key, $value, $cache_time = null)
    {
        if ($cache_time === null)
        {
            $cache_time = self::CACHE_TIME_OUT;
        }
        if ($dc->is_cache_enabled())
        {
            return $dc->cache()
                      ->set($key, serialize($value), $cache_time);
        }
    }

    /**
     * MEMCACHE缓存获取
     * @author 陈波
     * @date   2014-08-26
     * @param object $dc
     * @param string $key MEMCACHE 键
     * @return string true|false|array
     * */
    public static function get ($dc, $key)
    {
    	if ($dc->is_cache_enabled())
        {
            $result = unserialize($dc->cache()
                                     ->get($key));

            return $result;
        }
        else
        {
            return false;
        }
    }

    /**
     * MEMCACHE删除
     * @author 陈波
     * @date   2014-08-26
     * @param object $dc
     * @param string $key MEMCACHE 键
     * @return boolean true|false
     * */
    public static function delete ($dc, $key)
    {
        if ($dc->is_cache_enabled())
        {
            $result = $dc->cache()
                         ->delete($key);

            return $result;
        }
        else
        {
            return true;
        }
    }

    /**
     * MEMCACHE key
     * @author 陈波
     * @date   2014-08-26
     * @param string $mainkey 主KEY
     * @param array  $params  生成KEY相关参数
     * @return string
     * */
    public static function gkey ($mainkey, $params = null)
    {
        if (!$params)
        {
            $params = $_GET;
        }
        $gkey = $mainkey;
        if(strlen(self::$proxy_key)>0&&filter_var(self::$proxy_key,FILTER_VALIDATE_IP))
        {
        	$params[]=self::$proxy_key;
        }
        foreach ($params as $pkey => $pvalue)
        {
            if (!array_search($pkey, self::$except_keys))
            {
                $gkey .= '|#' . $pvalue;
            }
        }

        return MD5($gkey);
    }

    /**
     * MEMCACHE key
     * @author 陈波
     * @date   2014-08-26
     * @param string $base_url      基础URL
     * @param array  $params        生成KEY相关参数
     * @param array  $except_params 排出参数
     * @return string
     * */
    public static function build_key ($base_url, $params, $except_params = null)
    {
        $key = $base_url;
        foreach ($params as $pkey => $pvalue)
        {
            if (is_array($except_params))
            {
                if (!array_search($pkey, $except_params))
                {
                    $key .= '|#' . $pvalue;
                }
            }
            else
            {
                $key .= '|#' . $pvalue;
            }
        }

        return MD5($key);
    }
}

?>
