<?php
/**
 * V2状态任务
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_command_task extends nl_public
{
    static $base_table = "nns_command_task";
    static $arr_filed = array(
        'nns_id',
        'nns_command_id',
        'nns_sp_id',
        'nns_action',
        'nns_status',
        'nns_mtime',
        'nns_create_time',
        'nns_modify_time',
    );

    /**
     * 添加SP指令状态
     * @param object $hash_dc 为hashdc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function add($hash_dc, $params)
    {
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || ! is_array($params))
        {
            return self::return_data(NS_CDS_SUCCE, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库执行失败' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql);
    }

    /**
     * 添加SP指令状态
     * @param object $hash_dc 为hashdc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function query_by_condition($hash_dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }

    /**
     * 查询状态池列表
     * @param object $hash_dc 为hashdc
     * @param $params //数据数组
     * @param string $order //排序规则
     * @return array
     */
    static public function get_list($hash_dc, $params, $order = 'nns_create_time desc')
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }
}