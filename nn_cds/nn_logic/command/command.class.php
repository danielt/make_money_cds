<?php
/**
 * V2状态池
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_command extends nl_public
{
    static $base_table = "nns_command";
    static $arr_filed = array(
        'nns_id',
        'nns_message_id',
        'nns_source_id',
        'nns_type',
        'nns_original_id',
        'nns_name',
        'nns_content_id',
        'nns_create_time',
        'nns_modify_time',
    );

    /**
     * 添加消息分发状态池
     * @param object $hash_dc 为hashdc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function add($hash_dc, $params)
    {
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || ! is_array($params))
        {
            return self::return_data(NS_CDS_SUCCE, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库执行失败' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql);
    }


    /**
     * 查询指令池，只能根据消息ID
     * @param $hash_dc
     * @param $params
     * @return array
     */
    static public function query_by_condition($hash_dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }


    /**
     * 查询分发状态列表,name进行模糊匹配
     * @param $hash_dc
     * @param $params
     * @return array
     */
    static public function query_by_condition_v2($hash_dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            $sql = "select * from " . self::$base_table;
        }
        else
        {
            $name_where = '';
            if (isset($params['nns_name']) && strlen($params['nns_name']) > 0)
            {
                $name_where .= " and nns_name like '%{$params['nns_name']}%'";
            }
            unset($params['nns_name']);
            $sql = self::make_query_sql(self::$base_table, $params);
            $sql .= $name_where;
        }

        $result = nl_query_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }

    /**
     * @param $hash_dc
     * @param $sql
     * @return array
     */
    static public function get_list_by_sql($hash_dc, $sql)
    {
        $result = nl_query_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }

    /**
     * 删除command 指令池数据，只能根据消息ID
     * @param $hash_dc
     * @param $params
     */
    static public function del_command($hash_dc,$params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '删除条件为空不允许删除');
        }
        $sql = self::make_delete_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $hash_dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库执行失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }
}