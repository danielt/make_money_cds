<?php
/**
 * v2指令队列日志
 * Created by <xinxin.deng>.
 * Author: xinxin.deng
 * Date: 2018/8/6 14:40
 */
include_once dirname(dirname(__FILE__)).DIRECTORY_SEPARATOR.'public.class.php';
class nl_command_task_log extends nl_public
{
    static $base_table = "nns_command_task_log";
    static $arr_filed = array(
        'nns_command_task_id',
        'nns_status',
        'nns_desc',
        'nns_execute_url',
        'nns_create_time',
    );

    /**
     * 添加指令队列日志
     * @param object $dc 为dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function add($dc, $params)
    {
        $params['nns_create_time'] = $params['nns_modify_time'] = date('Y-m-d H:i:s');
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (empty($params) || ! is_array($params))
        {
            return self::return_data(NS_CDS_SUCCE, '参数为空');
        }
        $sql = self::make_insert_sql(self::$base_table, $params);
        $result = nl_execute_by_db($sql, $dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库执行失败' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql);
    }

    /**
     * 按条件查询日志列表
     * @param object $dc 为dc
     *            数据库对象
     * @param array $params
     *            数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function query_by_condition($dc, $params)
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }

    /**
     *
     * @param object $dc
     * @param array $params 数据数组
     * @return array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    /**
     * 添加SP指令状态
     * @param $dc //为dc 数据库对象
     * @param array $params 数据数组
     * @param string $order //排序规则
     * @return array array('ret'=>'状态码','reason'=>'原因','data'=>'数据')
     */
    static public function get_list($dc, $params, $order = 'nns_create_time desc')
    {
        $params = self::except_useless_params(self::$arr_filed, $params);
        if (!is_array($params) || empty($params))
        {
            return self::return_data(1, '查询条件为空不允许查询');
        }
        $sql = self::make_query_sql(self::$base_table, $params);
        $str_order = '';
        if (strlen($order) > 0)
        {
            $str_order = ' order by ' . $order;
        }
        $sql .= $str_order;
        $result = nl_query_by_db($sql, $dc->db());
        if (! $result)
        {
            return self::return_data(NS_CDS_FAIL, '数据库查询失败,sql:' . $sql);
        }
        return self::return_data(NS_CDS_SUCCE, 'ok:' . $sql, $result);
    }
}