<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

include_once LOGIC_DIR.'/log/manager_log.class.php';
class common_action extends nf_action {

    protected $arr_call_data = array();

    public function init() {
        if (method_exists($this, "checkLog")) {
            $this->checkLog();
        }
        $arr_config = include APP_DIR . '/config/map.inc.php';
        $str_control_method = strtolower($this->arr_request['m']) . '.' . strtolower($this->arr_request['a']);
        if (array_key_exists($str_control_method, $arr_config)) {
            $arr_params = $this->arr_request;
            unset($arr_params['m']);
            unset($arr_params['a']);
            include $arr_config[$str_control_method]['file_name'];
            if (isset($arr_config[$str_control_method]['class_name'])) {
                $arr_method = array($arr_config[$str_control_method]['class_name'], $arr_config[$str_control_method]['method_name']);
            } else {
                $arr_method = $arr_config[$str_control_method]['method_name'];
            }
            $this->arr_call_data = call_user_func_array($arr_method, $arr_params);
        }
    }

}
