<?php
/**
 * @author LZ
 * created on 2014-04-21
 * 打点信息
 */

class seekpoint_action extends common_action {
	public $dc = null;
	
	public function __construct() {
		include_once LOGIC_DIR.'seekpoint/seekpoint.class.php';
		$this->dc = nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));		
/* 		$this->obj_log = new np_log_v2();
		$this->obj_log->set_dir(PROJECT_DIR . '/data/log/' . APP_NAME . '/' . $_REQUEST['m'] . '/'); //设置存放路径
		$this->obj_log->set_level_config('error', 10, 10, 'error', 3600);
		$this->obj_log->set_level_config('info', 10, 10, 'info', 3600); */
	}
	
	/**
	 * 添加打点信息
	 */
	public function set_seekpoint() {
		$data = array(
				'nns_video_index' => $this->arr_request['video_index'],
				'nns_video_id' => $this->arr_request['video_id'],
				'nns_begin' => $this->arr_request['begin_time'],
				'nns_end' => $this->arr_request['end_time'],
				'nns_fragment_type' => $this->arr_request['fragment_type'],
				'nns_image' => $this->arr_request['image'],
				'nns_name' => $this->arr_request['name']
		);
		$result = nl_seekpoint::add_seekpoint($this->dc,$data);	
		if($result) {		
			$return = array("ret"=>1,"reason"=>"添加成功","data"=>array());
		} else {
			$return = array("ret"=>0,"reason"=>"添加失败","data"=>$result);
		}
		$this->set_response_data($return);
		$this->out_put();
	}
	/**
	 * 修改
	 */
	public function update_seekpoint() {
		include_once PROJECT_DIR.'nn_cms_manager/controls/nncms_controls_public_function.php';
		$data = array();
		// 若是表单提交过来会使没传的值为空
		if(isset($this->arr_request['begin_time']) && $this->arr_request['begin_time'] != '')
			$data['nns_begin'] = $this->arr_request['begin_time'];
		if(isset($this->arr_request['end_time']) && $this->arr_request['end_time'] != '')
			$data['nns_end'] = $this->arr_request['end_time'];
		if(isset($this->arr_request['fragment_type']) && $this->arr_request['fragment_type'] != '')
			$data['nns_fragment_type'] = $this->arr_request['fragment_type'];
		if(isset($this->arr_request['name']) && $this->arr_request['name'] != '')
			$data['nns_name'] = $this->arr_request['name'];
		$where = array(
				'nns_id' => $this->arr_request['id'],
				'nns_video_id' => $this->arr_request['video_id'],
				'nns_video_index' => $this->arr_request['video_index']				
		);
		if(isset($this->arr_request['image']) && $this->arr_request['image'] != '') {
			$result = nl_seekpoint::query($this->dc,$where);
			if(!is_array($result)) {
				$return = array("ret"=>0,"reason"=>"没有相关的数据","data"=>array());
				$this->set_response_data($return);
				$this->out_put();
				die;
			}
	        //删除修改前的图片
	        pub_func_image_delete($result[0]['nns_image']);
			$data['nns_image'] = $this->arr_request['image'];
		}
		$re = nl_seekpoint::modify($this->dc,$data, $where);
		if($re === true) {
			$return = array("ret"=>1,"reason"=>"修改成功","data"=>array());
		} else {
			$return = array("ret"=>0,"reason"=>"修改失败","data"=>array());
		}
		$this->set_response_data($return);
		$this->out_put();
	}
	/**
	 * 删除已有的打点信息数据
	 */
	public function del_seekpoint() {
		
		include_once PROJECT_DIR.'nn_cms_manager/controls/nncms_controls_public_function.php';
		
		$where = array();
		$where['nns_id'] =  $this->arr_request['id'];
		if(isset($this->arr_request['video_id']) && $this->arr_request['video_id'] != '')
			$where['nns_video_id'] = $this->arr_request['video_id'];
		if(isset($this->arr_request['video_index']) && $this->arr_request['video_index'] != '')
			$where['nns_video_index'] = $this->arr_request['video_index'];		

		//先删除图片
		$result = nl_seekpoint::query($this->dc,$where);
		if(is_array($result)) {
			foreach($result as $value) {
				pub_func_image_delete($value['nns_image']);
				$id_arr = array('nns_id'=>$value['nns_id']);
				$re = nl_seekpoint::delete($this->dc,$id_arr);
				
				//失败一次，则退出删除
				if($re === false) {
					nl_log_v2_error('seekpoint', 'delete_seekpoint_error:'.var_export($id_arr,true),true);
					break;
				}
			}			
			if($re) {
				$return = array("ret"=>1,"reason"=>"删除成功","data"=>'');
			} else {
				$return = array("ret"=>0,"reason"=>"删除失败","data"=>'');
			};						
		} else {
			$return = array("ret"=>0,"reason"=>"没有相关的数据","data"=>'');
		}
		$this->set_response_data($return);
		$this->out_put();
	}
	/**
	 * 获取打点信息列表
	 */
	public function get_seekpoint() {
		
		$where = array(
					'nns_video_index' => $this->arr_request['index'],
					'nns_video_id' => $this->arr_request['id']
				);
		if(isset($this->arr_request['type']) && $this->arr_request['type'] != '')
			$where['nns_type'] = $this->arr_request['type'];
		$result = nl_seekpoint::query($this->dc,$where);
		if(!is_array($result))$result=array();
		if(!empty($result)) {
			foreach ($result as $key=>$value) {
				if(!empty($value['nns_image'])) {
					$result[$key]['nns_image'] = nl_image::get_manager_image_url($value['nns_image']);
				}
			}
		}
		$return = array("ret"=>1,"reason"=>"success","data"=>array('list'=>$result));
		$this->set_response_data($return);
		$this->out_put();
	}
	/**
	 * 图片上传
	 */
	public function upload_seekpoint() {
		include_once LOGIC_DIR.'image/image.class.php';
		$upload_obj = new nf_upload_file(array('bool_auto_sub'=>false));
		$image = array_pop($_FILES);
		$video_id = $this->arr_request['video_id'];
		//上传图片
		$save_path = 'data/upimg/seekpoint/'.$video_id.'/';
		$img = $upload_obj->upload_one($image,PROJECT_DIR.$save_path);
		if ($img['ret'] == 1) {
			$name = $img['data'][0]['savename'];
			$result_path = $save_path . $name;
			//上传到FTP
			$path = nl_image::_ftp_copy_images($save_path, $result_path);
			if($path === FALSE) {
				$result = array("ret"=>0,"reason"=>"上传到FTP失败","data"=>'');
			} else {
				$ftp_url = nl_image::get_manager_image_url($path);
				$result = array("ret"=>1,"reason"=>"success","data"=>array('image_url'=>$path,'ftp_url'=>$ftp_url));
			}
		} else {
			@unlink($img['data'][0]['savepath'].$img['data'][0]['savename']);
			$result = array(
						'ret' => 0,
						'reason' => '图片上传失败',
						'data' => array()
					);
		}
		$this->set_response_data($result);
		$this->out_put();
	}
	/**
	 * 删除图片操作
	 */
	public function del_image_seekpoint() {
		include_once PROJECT_DIR.'nn_cms_manager/controls/nncms_controls_public_function.php';
		$where = array('nns_id' => $this->arr_request['id']);
		$result = nl_seekpoint::query($this->dc,$where);
		if(is_array($result)) {
			pub_func_image_delete($result[0]['nns_image']);
			$params = array('nns_image'=>'');
			$result = nl_seekpoint::modify($this->dc, $params, $where);
			if($result === true) {
				$return = array("ret"=>1,"reason"=>"图片删除成功","data"=>'');
			} else {
				$return = array("ret"=>0,"reason"=>"图片删除失败","data"=>'');
			}
		} else {
			$return = array("ret"=>0,"reason"=>"没有相关的数据","data"=>'');
		}
		$this->set_response_data($return);
		$this->out_put();
	}
}