<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function nf_get_r_dc() {
    static $dc = null;
    if ($dc !== null) {
        return $dc;
    }
    $dc = nl_get_dc(array("db_policy" => NL_DB_READ, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
    $dc->open();
    return $dc;
}
function nf_get_w_dc() {
    static $dc = null;
    if ($dc !== null) {
        return $dc;
    }
    $dc = nl_get_dc(array("db_policy" => NL_DB_WRITE, "cache_policy" => NP_KV_CACHE_TYPE_MEMCACHE));
    $dc->open();
    return $dc;
}
