<?php
return array(
    'return_data_type'=>'json',
	//统计模块生成的csv文件的表头和条件中的步长
	'type' =>array('date'=>'日期','video'=>'主媒资','index'=>'分集','media'=>'片源','data'=>'用户'),
	'scale'=>array('year'=>'年','month'=>'月','day'=>'日','week'=>'周'),
	//统计模块服务器生成的csv文件临时存放地址
	'fileTemp'=>dirname(dirname(dirname(__FILE__))) . '/data/csv_temp/'	
);