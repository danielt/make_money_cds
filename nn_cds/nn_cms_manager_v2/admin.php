<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
session_start();
define("APP_NAME", 'cms_manager_v2');
define("APP_DIR", dirname(__FILE__).'/');
define("DEBUG",0);
define("PROJECT_DIR", dirname(dirname(__FILE__)).'/');
if(!defined("LOGIC_DIR")) {
    define("LOGIC_DIR", dirname(dirname(__FILE__)) . '/nn_logic/');
}
if(!defined("NP_DIR")) {
    define("NP_DIR", dirname(dirname(dirname(__FILE__))) . '/np/');
}
//载入logic配置文件
include LOGIC_DIR.'nl_common.func.php';
require('../nn_framework/nf_framework.php'); //加载框架的入口文件
