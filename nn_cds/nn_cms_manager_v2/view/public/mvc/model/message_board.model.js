var messageBoardModel = new Starcor.S67.MVC.Model("message_board");
/**
 * 获取留言板
 * @param params 为空
 */
messageBoardModel.query=function(params){
    NBOSS.ajax(params,'get_list',function(data){
        if (data.code=='1'){
            messageBoardModel.data=data.data;
        }else{
            messageBoardModel.data = {};
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageBoardModel.disEvent('query', messageBoardModel.data);
    },'weixin/message_board');
};
/**
 * 添加留言板
 * @param params={user_name:用户名,content:内容}
 * 说明：注意内容不超过100个字
 */
messageBoardModel.add=function(params){
    NBOSS.ajax(params,'add',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageBoardModel.disEvent('add');
    },'weixin/message_board');
};

/**
 * 删除留言板
 * @param params={id:[]}，留言id，注意是一个数组格式
 */
messageBoardModel.del=function(params){
    NBOSS.ajax(params,'del',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageBoardModel.disEvent('del');
    },'weixin/message_board');
};
/**
 * 修改留言板
 * @param params={id:内容id，user_name:用户名,content:内容}
 */
messageBoardModel.edit=function(params){
    NBOSS.ajax(params,'edit',function(data){
        if (data.code=='1'){
            var n=messageBoardModel.getDataById(params.id);
            if (n!==null){
                messageBoardModel.data.list[n].name=params.name;
                messageBoardModel.refresh();
            }
            window.top.NBOSS.Alert('提示','修改成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageBoardModel.disEvent('edit');
    },'weixin/message_board');
};

/**
 * 修改留言板状态
 * @param params={id:[],state:状态值}，id是一个数组对象，state=1通过，0未审核，2未通过
 */
messageBoardModel.change_state=function(params){
    NBOSS.ajax(params,'change_state',function(data){
        if (data.code=='1'){
            var this_arry = messageBoardModel.getDataByarryId(params.id);
            for(var n in this_arry) {
                messageBoardModel.data.list[this_arry[n]].state=params.state;
                messageBoardModel.refresh();
            }
            window.top.NBOSS.Alert('提示','修改成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageBoardModel.disEvent('change_state');
    },'weixin/message_board');
};
messageBoardModel.info=function(params){
    var n=messageBoardModel.getDataById(params.id);
    if (n!==null){
        messageBoardModel.info_data=messageBoardModel.data.list[n];
    }
    else{
        messageBoardModel.info_data={}
    }
    messageBoardModel.disEvent('info',messageBoardModel.info_data);
}
messageBoardModel.getDataById=function(id){
    for (var i=0;i<messageBoardModel.data.list.length;i++){
        if (messageBoardModel.data.list[i].id==id){
            return i;
        };
    }
    return null;
};
messageBoardModel.getDataByarryId = function(id) {
    var this_arry=[]
    for (var i = 0; i < messageBoardModel.data.list.length; i++) {
        for(var j=0;j<id.length;j++){
            if (messageBoardModel.data.list[i].id == id[j]) {
                this_arry.push(i);
                continue;
            }
        }  
    }
    return this_arry;
}

