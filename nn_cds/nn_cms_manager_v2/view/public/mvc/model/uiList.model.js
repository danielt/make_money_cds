var UilayModel = new Starcor.S67.MVC.Model("Uilay");
 UilayModel.add=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=add',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        UilayModel.disEvent('add');
    });
};
UilayModel.get_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=query',function(data){
        if (data.code=='1'){
            UilayModel.data=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       UilayModel.disEvent('get_list',UilayModel.data);
    });
};
UilayModel.get_pic_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager/controls/image_thumb/nncms_controls_image_ftp.php',function(data){
        if (data.ret=='0'){
            UilayModel.picdata=data;
        }
       UilayModel.disEvent('get_pic_list',UilayModel.picdata);
    });
};
UilayModel.del=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=delete',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       UilayModel.disEvent('del');
    });
};
UilayModel.edit=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=modify',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','修改成功');
            UilayModel.editdata=data.data
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       UilayModel.disEvent('edit',UilayModel.editdata);
    });
};
