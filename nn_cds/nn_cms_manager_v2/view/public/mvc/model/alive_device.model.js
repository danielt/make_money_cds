var alive_deviceModel = new Starcor.S67.MVC.Model("alive_device");
/**
 * 获取设备列表
 * @param params={condition:[{键名:键值}...],pager[{page_size:每页大小},{page_num:第几页}]}
 */
alive_deviceModel.query=function(params){
    NBOSS.ajax(params,'get_list',function(data){
        if (data.code=='1'){
            alive_deviceModel.data=data.data;
        }else{
            alive_deviceModel.data = {};
            window.top.NBOSS.Alert('错误',data.reason);
        }
        alive_deviceModel.disEvent('query', alive_deviceModel.data);
    },'weixin/alive_device');
}

alive_deviceModel.delete=function(params){
	if (!params.id){
		return;
	}
	
	NBOSS.ajax(params,'del',function(data){
		if (data.code=='1'){
			// var ids=params.id.split(",");
			alive_deviceModel.disEvent('delete');
			window.top.NBOSS.Alert('提示','设备删除成功');
		}else{
			 window.top.NBOSS.Alert('错误',data.reason);
		}
		
	},'weixin/alive_device');
}


alive_deviceModel.getDataById=function(id){
	for (var i=0;i<alive_deviceModel.data.item.length;i++){
		if (alive_deviceModel.data.item[i].id==id){
			return i;
		};
	}
	return null;
};



