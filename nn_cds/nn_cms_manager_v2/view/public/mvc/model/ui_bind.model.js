var ui_bindView = new Starcor.S67.MVC.Model("ui_bind");
ui_bindView.add=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_d&a=add',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        ui_bindView.disEvent('add');
    });
};
ui_bindView.get_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_d&a=query',function(data){
        if (data.code=='1'){
            ui_bindView.data=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_bindView.disEvent('get_list',ui_bindView.data);
    });
};
ui_bindView.get_list_lay=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_a&a=query',function(data){
        if (data.code=='1'){
            ui_bindView.datalay=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_bindView.disEvent('get_list_lay',ui_bindView.datalay);
    });
};

ui_bindView.del=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_d&a=delete',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_bindView.disEvent('del');
    });
};



