/**
 * 图文消息
 * Created by IntelliJ IDEA.
 * User: yuyi
 * Date: 2014/4/1
 * Time: 18:45
 */
var news_model = new Starcor.S67.MVC.Model("news");
/**
 * @auth yxt
 * 添加图文消息
 * @param params={global_name:全局名称,data:[{url:媒资页面地址，title:标题,author:作者，image_address:封面图url},{url:媒资页面地址，title:标题,author:作者，image_address:封面图url}]}
 */
news_model.add = function (params) {
    NBOSS.ajax(params, 'add', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '添加成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        news_model.disEvent('add', news_model.data);
    }, 'weixin/news');
};
/**
 * @auth yxt
 * 修改图文消息
 * @param params={id:消息id，global_name:全局名称,data:[{url:媒资页面地址，title:标题,author:作者，image_address:封面图url},{url:媒资页面地址，title:标题,author:作者，image_address:封面图url}]}
 */
news_model.edit = function (params) {
    NBOSS.ajax(params, 'edit', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '修改成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        news_model.disEvent('edit');
    }, 'weixin/news');
};
/**
 * 获取图文消息列表
 * @param params={condition:[{键名:键值}...],pager[{page_size:每页大小},{page_num:第几页}]}
 */
news_model.get_news_list = function (params) {
    NBOSS.ajax(params, 'get_news_list', function (data) {
        if (data.code == '1') {
            news_model.data = data.data;
        } else {
            news_model.data = {
            };
            window.top.NBOSS.Alert('错误', data.reason);
        }
        news_model.disEvent('get_news_list', news_model.data);
    }, 'weixin/news');
};
/**
 * 获取图文消息内容列表
 * @param params={message_id:图文消息id}
 */
news_model.get_news_content_list = function (params) {
    NBOSS.ajax(params, 'get_content_list', function (data) {
        if (data.code == '1') {
            news_model.news_content_data = data.data;
        } else {
            news_model.news_content_data = {
            };
            window.top.NBOSS.Alert('错误', data.reason);
        }
        news_model.disEvent('get_news_content_list', news_model.news_content_data);
    }, 'weixin/news');
}
/**
 * 删除图文消息，就是完整的图文消息，一次可以删除多条
 * @param params={id:[{消息id},{消息id},{消息id},{消息id}]}
 */
news_model.del_news = function (params) {
    NBOSS.ajax(params, 'del_news', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '删除成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        news_model.disEvent('del_news');
    }, 'weixin/news');
}
/**
 * 删除图文内容消息，一条图文消息中的一个小的图文
 * @param params{id:消息id}
 */
news_model.del_news_content = function (params) {
    NBOSS.ajax(params, 'del_news_content', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '删除成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        news_model.disEvent('del_news_content');
    }, 'weixin/news');
}