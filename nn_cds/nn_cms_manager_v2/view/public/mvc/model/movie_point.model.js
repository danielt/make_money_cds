var moviePointModel = new Starcor.S67.MVC.Model("movie_point");
moviePointModel.add=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=seekpoint/seekpoint&a=set_seekpoint',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        moviePointModel.disEvent('add',moviePointModel.data);
    });
};
moviePointModel.get_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=seekpoint/seekpoint&a=get_seekpoint',function(data){
        if (data.code=='1'){
            moviePointModel.data=data.data;
        }else{
            moviePointModel.data = {};
        }
        moviePointModel.disEvent('get_list',moviePointModel.data);
    });
};
moviePointModel.del_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=seekpoint/seekpoint&a=del_seekpoint',function(data){
        if (data.code=='1'){
             window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        moviePointModel.disEvent('del_list');
    });
};

moviePointModel.edit_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=seekpoint/seekpoint&a=update_seekpoint',function(data){
        if (data.code=='1'){
         window.top.NBOSS.Alert('提示','修改成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        moviePointModel.disEvent('edit_list');
    });
};

moviePointModel.del_image=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=seekpoint/seekpoint&a=del_image_seekpoint',function(data){
        if (data.code=='1'){
             window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        moviePointModel.disEvent('del_image');
    });
};

