var homeMenuModel = new Starcor.S67.MVC.Model("homeMenu");
homeMenuModel.rights = [];
// homeMenuModel.homeMenu = function(params) {
//     if (!$.isEmptyObject(homeMenuModel.$data)) {
//         homeMenuModel.disEvent('homeMenu', homeMenuModel.$data);
//     } else {
//         NBOSS.ajax(params, 'menu', function(data) {
//             if (data.code == "000000") {
//                 var tempData = {};
//                 var menuData = data.data;
//                 homeMenuModel.rights = data.data;
//                 for (var i = 0; i < menuData.length; i++) {
//                     if (menuData[i].id == '1') {
//                         menuData[i].navigation = [menuData[i].name];
//                         menuData[i].menuLevel = 1;

//                         tempData[menuData[i].model_id] = menuData[i];
//                         tempData[menuData[i].model_id].subMenuList = [];
//                     }
//                 }

//                 for (var i = 0; i < menuData.length; i++) {
//                     if (menuData[i].id != "1") {
//                         if (tempData[menuData[i].model_id]) {
//                             homeMenuModel.command_data(menuData[i], tempData[menuData[i].model_id], tempData[menuData[i].model_id]);
//                         } else {
//                             tempData[menuData[i].model_id] = menuData[i];
//                             tempData[menuData[i].model_id].subMenuList = [];
//                             tempData[menuData[i].model_id].menuLevel = 1;
//                             tempData[menuData[i].model_id].navigation = [menuData[i].name];
//                         }
//                     }
//                 }



//                 homeMenuModel.$data = tempData;

//                 homeMenuModel.disEvent('homeMenu', homeMenuModel.$data);
//             } else {
//                 homeMenuModel.$data = {};

//                 NBOSS.tips.error("错误", data.reason, function() {
//                     //TODO: 留着扩展
//                 });
//             }
//         });
//     }
// };

homeMenuModel.homeMenu = function(params) {
    var fork_data=[{'action': 0,'id': 1,'model_id': "systemManage",'name': "微信管理",'template': ""},
    {'action': 0,'id': 101,'model_id': "systemManage",'name': "编辑功能",'template': ""},
    {'action': 1,'id': 10101,'model_id': "systemManage",'name': "自动回复设置",'template': "html/massege/message_manage.html"},
    {'action': 1,'id': 10102,'model_id': "systemManage",'name': "自定义菜单设置",'template': "html/massege/custom_menu_manage.html"},
    {'action': 1,'id': 10103,'model_id': "systemManage",'name': "素材库管理",'template': "html/resources/resources_manage.html"},
    {'action': 1,'id': 102,'model_id': "systemManage",'name': "公众号设置",'template': "html/massege/account_set_manage.html"},
    // {'action': 1,'id': 103,'model_id': "systemManage",'name': "留言板",'template': "html/message_board/message_board_manage.html"},
    {'action': 0,'id': 103,'model_id': "systemManage",'name': "可控设备列表",'template': ""},
    {'action': 1,'id': 10301,'model_id': "systemManage",'name': "活动设备列表",'template': "html/device/alive_device.html"},
    {'action': 0,'id': 1,'model_id': "defer",'name': "模版中心",'template': ""},
    {'action': 0,'id': 101,'model_id': "defer",'name': "模版中心",'template': ""},
     {'action': 1,'id':10101,'model_id': "defer",'name': "模版页面",'template': "html/example/table.html"},
        {'action': 1,'id':10102,'model_id': "defer",'name': "模版页面2",'template': "html/example/template.html"},

        {'action': 0,'id':9,'model_id': "totleReport",'name': "统计报表",'template': ""},
        {'action': 0,'id':901,'model_id': "totleReport",'name': "资源库数据统计",'template': ""},

        {'action': 1,'id':90101,'model_id': "totleReport",'name': "资源总览图",'template': "html/totleReport/videototle.html"},
        {'action': 1,'id':90102,'model_id': "totleReport",'name': "资源增量统计",'template': "html/totleReport/videozl.html"},
        {'action': 1,'id':90103,'model_id': "totleReport",'name': "资源存量统计",'template': "html/totleReport/videocl.html"},
        {'action': 1,'id':90104,'model_id': "totleReport",'name': "资源分布统计",'template': "html/totleReport/videofb.html"},
        {'action': 1,'id':90105,'model_id': "totleReport",'name': "资源增量详情统计",'template': "html/totleReport/videozl_detail.html"},
        {'action': 1,'id':90106,'model_id': "totleReport",'name': "资源存量详情统计",'template': "html/totleReport/videocl_sellect.html"},

       /* {'action': 1,'id':90105,'model_id': "totleReport",'name': "分集统计图",'template': "html/totleReport/video_librarybyindex.html?headindex=0&leftindex=4"},
        {'action': 1,'id':90106,'model_id': "totleReport",'name': "分集快捷存量图",'template': "html/totleReport/video_shortbyindex.html?headindex=0&leftindex=5"},
        {'action': 1,'id':90107,'model_id': "totleReport",'name': "分集存量图",'template': "html/totleReport/video_detailbyindex.html?headindex=0&leftindex=6"},

        {'action': 1,'id':90108,'model_id': "totleReport",'name': "片源统计图",'template': "html/totleReport/video_librarybymedia.html?headindex=0&leftindex=7"},
        {'action': 1,'id':90109,'model_id': "totleReport",'name': "片源快捷存量图",'template': "html/totleReport/video_shortbymedia.html?headindex=0&leftindex=8"},
        {'action': 1,'id':90110,'model_id': "totleReport",'name': "片源存量图",'template': "html/totleReport/video_detailbymedia.html?headindex=0&leftindex=9"},
*/
        {'action': 0,'id':902,'model_id': "totleReport",'name': "媒资包数据统计",'template': ""},
        {'action': 1,'id':90201,'model_id': "totleReport",'name': "媒资增量统计",'template': "html/totleReport/mediazl.html"},
        {'action': 1,'id':90203,'model_id': "totleReport",'name': "媒资存量统计",'template': "html/totleReport/mediacl.html"},
        {'action': 1,'id':90202,'model_id': "totleReport",'name': "媒资分布统计",'template': "html/totleReport/mediafb.html"},
        {'action': 1,'id':90204,'model_id': "totleReport",'name': "媒资存量查询统计",'template': "html/totleReport/mediacl_select.html"},

        {'action': 0,'id':903,'model_id': "totleReport",'name': "用户数据统计",'template': ""},
        {'action': 1,'id':90301,'model_id': "totleReport",'name': "用户开户统计",'template': "html/totleReport/userkh.html"},
        {'action': 1,'id':90302,'model_id': "totleReport",'name': "用户总量统计",'template': "html/totleReport/usertotle.html"},

         //{'action': 1,'id':90303,'model_id': "totleReport",'name': "用户总量查询统计",'template': "html/totleReport/usertotle_select.html"},

        {'action': 0,'id':904,'model_id': "totleReport",'name': "产品数据统计",'template': ""},
      //  {'action': 1,'id':90401,'model_id': "totleReport",'name': "产品增量统计",'template': "html/totleReport/productzl.html"},

        {'action': 1,'id':90401,'model_id': "totleReport",'name': "产品内容分布统计",'template': "html/totleReport/productfb.html"},
        {'action': 1,'id':90402,'model_id': "totleReport",'name': "产品购买分布统计",'template': "html/totleReport/userfb.html"},
        {'action': 1,'id':90403,'model_id': "totleReport",'name': "用户订购统计",'template': "html/totleReport/userdg.html"},

        //{'action': 1,'id':90402,'model_id': "totleReport",'name': "产品增量图",'template': "html/totleReport/product_detail.html?headindex=0&leftindex=11"},

       /* {'action': 0,'id':905,'model_id': "totleReport",'name': "增量数据统计",'template': ""},
        {'action': 1,'id':90501,'model_id': "totleReport",'name': "资源库增量统计",'template': "html/totleReport/video_table.html?headindex=0&leftindex=16"},
        {'action': 1,'id':90502,'model_id': "totleReport",'name': "媒资包增量统计",'template': "html/totleReport/media_table.html?headindex=0&leftindex=17"},

       */ {'action': 0,'id': 1,'model_id': "point",'name': "打点信息",'template': ""},
        {'action': 1,'id': 101,'model_id': "point",'name': "打点信息",'template': "html/point/point_manage.html"},

        {'action': 0,'id':2,'model_id': "lay",'name': "终端布局",'template': ""},
        {'action': 0,'id':201,'model_id': "lay",'name': "UI分组管理",'template': ""},
        {'action': 1,'id': 2011,'model_id': "lay",'name': "UI分组",'template': "html/uiGroup/uiGrouplist_manage.html"},
        {'action': 0,'id':202,'model_id': "lay",'name': "UI包管理",'template': ""},
        {'action': 1,'id': 2021,'model_id': "lay",'name': "UI版本",'template': "html/uiGroup/uiGroupAdd_manage.html"},
        {'action': 1,'id': 2022,'model_id': "lay",'name': "UI布局模板",'template': "html/uiGroup/uiLaylist_manage.html"},
       
    ]
	
        var tempData = {};
        var menuData = fork_data;
        homeMenuModel.rights = fork_data;
        for (var i = 0; i < menuData.length; i++) {
            if (menuData[i].id == '1') {
                menuData[i].navigation = [menuData[i].name];
                menuData[i].menuLevel = 1;

                tempData[menuData[i].model_id] = menuData[i];
                tempData[menuData[i].model_id].subMenuList = [];
            }
        }

        for (var i = 0; i < menuData.length; i++) {
            if (menuData[i].id != "1") {
                if (tempData[menuData[i].model_id]) {
                    homeMenuModel.command_data(menuData[i], tempData[menuData[i].model_id], tempData[menuData[i].model_id]);
                } else {
                    tempData[menuData[i].model_id] = menuData[i];
                    tempData[menuData[i].model_id].subMenuList = [];
                    tempData[menuData[i].model_id].menuLevel = 1;
                    tempData[menuData[i].model_id].navigation = [menuData[i].name];
                }
            }
        }
        homeMenuModel.$data = tempData;
        homeMenuModel.disEvent('homeMenu', homeMenuModel.$data);
};

homeMenuModel.loginOut = function() {
    NBOSS.ajax('', 'unlogin', function(data) {
        if (data.code == "000000") {
            window.location.href = NBOSS.BaseUrl() + '/admin/index.html';
        }
        else {
            NBOSS.tips.error("错误", data.reason, function() {
                //TODO: 留着扩展

            });
        }

    }, "manage");
};

homeMenuModel.command_data = function(data, obj, parentObj) {
    var isSelectParent = false;


    /*for(var i=0; i<tempNum; i++){
     if(data.id.toString().indexOf(obj.id) != -1){
     if(obj.subMenuList && obj.subMenuList.length > 0){
     homeMenuModel.command_data();
     }else{
     obj.subMenuList = [];
     obj.subMenuList.push(data);
     break;
     }
     }else{
     data.navigation = parentObj.navigation.concat([data.name]);
     parentObj.push(data);
     break;
     }
     }*/



    if (data.id.toString().substring(0, obj.id.toString().length) == obj.id.toString()) {
        if (obj.subMenuList && obj.subMenuList.length > 0) {
            var tempNum = obj.subMenuList.length;
            var isDataInSub = false;
            for (var i = 0; i < tempNum; i++) {
                isDataInSub = homeMenuModel.command_data(data, obj.subMenuList[i], obj);

                if (isDataInSub) {
                    isSelectParent = true;
                    break;
                }
            }

            if (!isDataInSub) {
                data.navigation = obj.navigation.concat([data.name]);
                obj.subMenuList.push(data);
                isSelectParent = true;
            }
        } else {
            obj.subMenuList = [];
            data.navigation = obj.navigation.concat([data.name]);
            //data.menuLevel = obj.menuLevel + 1;
            obj.subMenuList.push(data);
            isSelectParent = true;
        }
    } else {
        obj = data;
    }

    return isSelectParent;
};
homeMenuModel.get_precent = function(params) {
    var rdata = {};
    NBOSS.ajax(params, 'getCreatePercent', function(data) {
        if (data.code == '000000') {
            rdata = data.data;
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        homeMenuModel.disEvent('get_precent', rdata);
    },'device',true);
}