var ui_packageModel = new Starcor.S67.MVC.Model("ui_package");
ui_packageModel.add=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_c&a=add',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        ui_packageModel.disEvent('add');
    });
};
ui_packageModel.get_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_c&a=query',function(data){
        if (data.code=='1'){
            ui_packageModel.data=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_packageModel.disEvent('get_list',ui_packageModel.data);
    });
};
ui_packageModel.get_list_group=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=query',function(data){
        if (data.code=='1'){
            ui_packageModel.groupdata=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_packageModel.disEvent('get_list_group',ui_packageModel.groupdata);
    });
};
ui_packageModel.get_list_lay=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_a&a=query',function(data){
        if (data.code=='1'){
            ui_packageModel.groupdata=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_packageModel.disEvent('get_list_lay',ui_packageModel.groupdata);
    });
};
ui_packageModel.del=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_c&a=delete',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       ui_packageModel.disEvent('del');
    });
};
ui_packageModel.edit=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_c&a=modify',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','修改成功');
            ui_packageModel.editdata=data.data
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
      ui_packageModel.disEvent('edit');
    });
};



