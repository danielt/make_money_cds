/**
 * 自定义菜单model
 * Created by yxt on 2014/4/1.
 */
var custom_menu_model = new Starcor.S67.MVC.Model("custom_menu");
/**
 * @auth yxt
 * 获取菜单
 * @param params为空
 */
custom_menu_model.query = function (params) {
    NBOSS.ajax(params, 'get', function (data) {
        if (data.code == '1') {
            custom_menu_model.data = data.data;
        } else {
            custom_menu_model.data = {};
            window.top.NBOSS.Alert('错误', data.reason);
        }
        custom_menu_model.disEvent('query', custom_menu_model.data);
    }, 'weixin/custom_menu');
};
/**
 * @auth yxt
 * 添加菜单
 * @param params={name:菜单名称,pid:菜单父id}//如果是一级菜单pid=1
 */
custom_menu_model.add = function (params) {
    NBOSS.ajax(params, 'add', function (data) {
        if (data.code == '1') {
            // custom_menu_model.data = data.data;
            window.top.NBOSS.Alert('提示', '栏目名称添加成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        custom_menu_model.disEvent('add');
    }, 'weixin/custom_menu');
};
/**
 * @auth yxt
 * 修改菜单名称
 * @param params={id:菜单id,name:菜单新名称}
 */
custom_menu_model.edit = function (params) {
    NBOSS.ajax(params, 'edit', function (data) {
        if (data.code == '1') {
            custom_menu_model.data = data.data;
        } else {
            custom_menu_model.data = {};
            window.top.NBOSS.Alert('错误', data.reason);
        }
        custom_menu_model.disEvent('edit', custom_menu_model.data);
    }, 'weixin/custom_menu');
};
/**
 * @auth yxt
 * 删除菜单
 * @param params={id:菜单id}
 */
custom_menu_model.del = function (params) {
    NBOSS.ajax(params, 'del', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '删除成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        custom_menu_model.disEvent('del');
    }, 'weixin/custom_menu');
};

/**
 * @auth yxt
 * 菜单排序
 * @param params={'data':[{'id':菜单id,'sort':顺序},{'id':菜单id,'sort':顺序},{'id':菜单id,'sort':顺序}]}
 */
custom_menu_model.sort = function (params) {
    NBOSS.ajax(params, 'sort', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '排序成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        custom_menu_model.disEvent('sort', custom_menu_model.data);
    }, 'weixin/custom_menu');
}
/**
 * @auth yxt
 * 给菜单绑定事件
 * @param params={'id':菜单id,'event_type':事件类型，'message_type':消息类型，'message_content':消息内容}
 * 说明：
 * 事件类型：如果为跳转网页，则事件类型为view，如果是发送消息为click
 * 消息类型：如果是跳转网页则消息类型为空，如果是发送消息，如果是文本消息则为1，如果是语音或者图文消息则为2
 * 消息内容：如果是跳转网页则为跳转网页的url，如果是发送文本消息则为文本消息内容，如果是语音或者图文消息则为其id
 */
custom_menu_model.event = function (params) {
    NBOSS.ajax(params, 'event', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('成功', '绑定事件成功');
            custom_menu_model.disEvent('event');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
    }, 'weixin/custom_menu');
}

/**
 * 发布菜单
 * @param params 为空
 */
custom_menu_model.publish = function (params) {
    NBOSS.ajax(params, 'publish', function (data) {
        if (data.code == '1') {
             window.top.NBOSS.Alert('成功', '发布成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        custom_menu_model.disEvent('publish');
    }, 'weixin/custom_menu');
}
custom_menu_model.menu_info=function(params){
    var id=params.id;
    custom_menu_model.single_data=custom_menu_model.getDataById(id);
    if(custom_menu_model.single_data){
        custom_menu_model.disEvent('menu_info',custom_menu_model.single_data);
    }
      
};
custom_menu_model.getDataById=function(id){
    for(var i in custom_menu_model.data.list){
        if(custom_menu_model.data.list[i].child.length!=0){
            var children=custom_menu_model.data.list[i].child;
            for(var j in custom_menu_model.data.list[i].child){
                if(children[j].id==id){
                    return children[j];
                }
                
            }
        }
        if (custom_menu_model.data.list[i].id==id){
            return custom_menu_model.data.list[i];
        };
    }
    return null;
};