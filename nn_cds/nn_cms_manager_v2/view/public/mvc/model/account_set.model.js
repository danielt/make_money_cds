var account_setModel = new Starcor.S67.MVC.Model("account_set");
/**
 * @auth yxt
 * 获取票据
 * @param params为空
 */
account_setModel.query_bill=function(params){
    NBOSS.ajax(params,'get_bill',function(data){
        if (data.code=='1'){
            account_setModel.data=data.data;
        }else{
            account_setModel.data = {};
            window.top.NBOSS.Alert('错误',data.reason);
        }
        account_setModel.disEvent('query_bill', account_setModel.data);
    },'weixin/account_set');
};
/**
 * @auth yxt
 * 设置票据
 * @param params={app_id:appid,app_secret:app_secret}
 */
account_setModel.set_bill=function(params){
    if (params.app_id==undefined||params.app_secret==undefined){
        window.top.NBOSS.Alert('提示','参数不能够为空');
        return;
    }
    NBOSS.ajax(params,'set_bill',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加消息成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        account_setModel.disEvent('add');
    },'weixin/account_set');
};
/**
 * @auth yxt
 * 设置公众号
 * @param params={pub_account_id:'公众号id',pub_account_name:'公众号名称',token:'公众号token',app_id:appid,app_secret:app_secret}
 */
account_setModel.set_account=function(params){
    if (params.app_id==undefined||params.app_secret==undefined||params.token==undefined){
        window.top.NBOSS.Alert('提示','参数不能够为空');
        return;
    }
    NBOSS.ajax(params,'set_account',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','设置公众号成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        account_setModel.disEvent('set_account');
    },'weixin/account_set');
};
/**
 * @auth yxt
 * 获取公众号信息
 * @param params为空
 */
account_setModel.query_account=function(params){
    NBOSS.ajax(params,'get_account',function(data){
        if (data.code=='1'){
            account_setModel.account_data=data.data;
        }else{
            account_setModel.account_data = {};
            window.top.NBOSS.Alert('错误',data.reason);
        }
        account_setModel.disEvent('query_account', account_setModel.account_data);
    },'weixin/account_set');
};