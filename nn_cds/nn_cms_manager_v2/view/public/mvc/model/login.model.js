var loginModel = new Starcor.S67.MVC.Model("login");
//后台登陆
loginModel.login = function(params) {
    data={};
    data.code="000000";
    loginModel.disEvent('check', data);
//    NBOSS.ajax(params, 'login', function(data) {
//        //抛出login事件
//        delCookie('visite_aarr');
//        delCookie('head_menu_arr');
//        loginModel.disEvent('check', data);
//    }, 'login');
};
//获取管理员信息
loginModel.logInfo=function(params){
    NBOSS.ajax(params, 'logInfo', function(data) {
        loginModel.disEvent('logInfo', data);
    }, 'weixin/login');
};
loginModel.logOut=function(){
    NBOSS.ajax(null, 'logOut', function(data) {
        delCookie('visite_aarr');
        delCookie('head_menu_arr');
        window.top.location.href = NBOSS.BaseUrl() + '/admin/index.html';
    }, 'weixin/login');
}