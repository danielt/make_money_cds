var messageModel = new Starcor.S67.MVC.Model("message");
/**
 * 获取关注自动回复
 * @param params 为空
 */
messageModel.query=function(params){
    NBOSS.ajax(params,'get_add_auto_reply',function(data){
        if (data.code=='1'){
            messageModel.data=data.data;
        }else{
            messageModel.data = {};
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageModel.disEvent('query', messageModel.data);
    },'weixin/account_set');
};
/**
 * 添加关注自动回复，
 * @param params={type:消息类型,content:内容}
 * 说明：
 * 消息类型:文本消息(text)，语音(voice),图文（news），可以不传递，默认为文本
 * 内容：如果是文本消息则为消息内容，如果是语音或者图文，则为其id
 */
messageModel.add=function(params){
    NBOSS.ajax(params,'set_add_auto_reply',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加自动回复成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageModel.disEvent('add');
    },'weixin/account_set');
};

/**
 * 获取消息自动回复
 * @param params 为空
 */
messageModel.query_message_auto_replay=function(params){
    NBOSS.ajax(params,'get_auto_reply',function(data){
        if (data.code=='1'){
            messageModel.rdata=data.data;
        }else{
            messageModel.rdata = {};
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageModel.disEvent('query_message_auto_replay', messageModel.rdata);
    },'weixin/account_set');
};
/**
 * 添加消息自动回复，
 * @param params={type:消息类型,content:内容}
 * 说明：
 * 消息类型:文本消息(text)，语音(voice),图文（news），可以不传递，默认为文本
 * 内容：如果是文本消息则为消息内容，如果是语音或者图文，则为其id
 */
messageModel.add_message_auto_replay=function(params){
    if (!params.content){
        return;
    }
    NBOSS.ajax(params,'set_auto_reply',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加消息成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        messageModel.disEvent('add_message_auto_replay');
    },'weixin/account_set');
};



