var ActiveCheckModel = new Starcor.S67.MVC.Model("ActiveCheck");
ActiveCheckModel.query = function(params) {
    NBOSS.ajax(params, 'getList', function(data) {
        if (data.code == '000000') {
            ActiveCheckModel.data = data.data;
        } else {
            ActiveCheckModel.data = {};
            window.top.NBOSS.Alert('错误', data.reason);
        }
        ActiveCheckModel.disEvent('query', ActiveCheckModel.data);
    },'weixin/ActiveCheck');
};
ActiveCheckModel.query_status = function(params) {
    NBOSS.ajax(params, 'getStatusList', function(data) {
        if (data.code == '000000') {
            ActiveCheckModel.data_state = data.data;
        } else {
            ActiveCheckModel.data_state = {};
            window.top.NBOSS.Alert('错误', data.reason);
        }
        ActiveCheckModel.disEvent('query_status', ActiveCheckModel.data_state);
    },'weixin/ActiveCheck');
};
ActiveCheckModel.set_done = function(params) {
    if (!params.id) {
        return;
    }
    NBOSS.ajax(params, 'edit', function(data) {
        if (data.code == '000000') {
            var this_arry = ActiveCheckModel.getDataById(params.id);
            for(var n in this_arry) {
                ActiveCheckModel.data.item[this_arry[n]].status_name = '已处理';
                ActiveCheckModel.data.item[this_arry[n]].status = params.status; 
            }
            ActiveCheckModel.refresh();
            window.top.NBOSS.Alert('提示', '处理成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        ActiveCheckModel.disEvent('set_done');
    },'weixin/ActiveCheck');
}
ActiveCheckModel.set_ignore = function(params) {
    if (!params.id) {
        return;
    }
    NBOSS.ajax(params, 'edit', function(data) {
        if (data.code == '000000') {
            var this_arry = ActiveCheckModel.getDataById(params.id);
            for(var n in this_arry) {
                ActiveCheckModel.data.item[this_arry[n]].status_name = '忽略';
                ActiveCheckModel.data.item[this_arry[n]].status = params.status;
            }
            ActiveCheckModel.refresh();
            window.top.NBOSS.Alert('提示', '忽略成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        ActiveCheckModel.disEvent('set_ignore');
    },'weixin/ActiveCheck');
}
ActiveCheckModel.delete=function(params){
    if (!params.id){
        return;
    }
    NBOSS.ajax(params,'del',function(data){
        if (data.code=='000000'){
            // var ids=params.id.split(",");
            window.top.NBOSS.Alert('提示','冲突列表删除成功');
        }else{
             window.top.NBOSS.Alert('错误',data.reason);
        }
        ActiveCheckModel.disEvent('delete');
    },'weixin/ActiveCheck');
}
ActiveCheckModel.getDataById = function(id) {
    var this_arry=[]
    for (var i = 0; i < ActiveCheckModel.data.item.length; i++) {
        for(var j=0;j<id.length;j++){
            if (ActiveCheckModel.data.item[i].id == id[j]) {
                this_arry.push(i);
                continue;
            }
        }  
    }
    return this_arry;
}