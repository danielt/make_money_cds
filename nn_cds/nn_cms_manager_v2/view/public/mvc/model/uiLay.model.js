var UilayoutModel = new Starcor.S67.MVC.Model("Uilayout");
 UilayoutModel.add=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_a&a=add',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','添加成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
        UilayoutModel.disEvent('add');
    });
};
UilayoutModel.get_list=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_a&a=query',function(data){
        if (data.code=='1'){
            UilayoutModel.data=data.data;
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       UilayoutModel.disEvent('get_list',UilayoutModel.data);
    });
};
UilayoutModel.del=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=delete',function(data){
        if (data.code=='0'){
            window.top.NBOSS.Alert('提示','删除成功');
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
       UilayoutModel.disEvent('get_list');
    });
};
UilayoutModel.edit=function(params){
    NBOSS.ajax_v2(params,'/nn_cms_manager_v2/admin.php?m=ui_layout/m1_b&a=modify',function(data){
        if (data.code=='1'){
            window.top.NBOSS.Alert('提示','修改成功');
            UilayoutModel.editdata=data.data
        }else{
            window.top.NBOSS.Alert('错误',data.reason);
        }
      UilayoutModelu.disEvent('edit');
    });
};
