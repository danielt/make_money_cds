/**
 * 语音model
 * Created by yuyi on 2014/4/2.
 */
var voice_model = new Starcor.S67.MVC.Model("voice");
/**
 * @auth yxt
 * 获取语音消息
 * @param params={condition:[{键名:键值}...],pager[{page_size:每页大小},{page_num:第几页}]}
 */
voice_model.get_voice_list = function (params) {
    NBOSS.ajax(params, 'get_voice_list', function (data) {
        if (data.code == '1') {
            voice_model.data = data.data;
        } else {
            voice_model.data = {
            };
            window.top.NBOSS.Alert('错误', data.reason);
        }
        voice_model.disEvent('get_voice_list', voice_model.data);
    }, 'weixin/voice');
};

/**
 * 删除语音，一次可以删除多条
 * @param params={id:[{消息id},{消息id},{消息id},{消息id}]}
 */
voice_model.del_voice = function (params) {
    NBOSS.ajax(params, 'del_voice', function (data) {
        if (data.code == '1') {
            window.top.NBOSS.Alert('提示', '删除成功');
        } else {
            window.top.NBOSS.Alert('错误', data.reason);
        }
        voice_model.disEvent('del_voice', voice_model.data);
    }, 'weixin/voice');
}
/**
 * 修改语音名称
 * @param params={id:语音id}
 */
// voice_model.edit_voice = function (params) {
//     NBOSS.ajax(params, 'edit_voice', function (data) {
//         if (data.code == '1') {
//             window.top.NBOSS.tips.success('提示', '修改成功');
//         } else {
//             window.top.NBOSS.tips.error('错误', data.reason);
//         }
//         voice_model.disEvent('edit_voice', voice_model.edata);
//     }, 'weixin/voice',true);
// }
voice_model.edit_voice=function(params){
    if (!params.id || !params.name){
        window.top.NBOSS.tips.warning('提示', '名字不能为空');
        return;
    }
    NBOSS.ajax(params,'edit_voice',function(data){
        if (data.code=='1'){
            var n=voice_model.getDataById(params.id);
            if (n!==null){
                voice_model.data.list[n].name=params.name;
                voice_model.refresh();
            }
            window.top.NBOSS.tips.success('提示', '修改成功');
        }else{
            voice_model.refresh();
            window.top.NBOSS.tips.error('错误', data.reason);
        }
        voice_model.disEvent('edit_voice');
    }, 'weixin/voice',true);  
};
voice_model.getDataById=function(id){
    for (var i=0;i<voice_model.data.list.length;i++){
        if (voice_model.data.list[i].id==id){
            return i;
        };
    }
    return null;
};