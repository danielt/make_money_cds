var loginInfoModel = new Starcor.S67.MVC.Model("loginInfo");

//获取登录用户信息
loginInfoModel.loginInfo = function(params) {
    if (!$.isEmptyObject(loginInfoModel.$data)) {
        loginInfoModel.disEvent('loginInfo', loginInfoModel.$data);
    } else {
        NBOSS.ajax(params, 'login_info', function(data) {
            if (data.code == "000000") {
                loginInfoModel.$data = data.data;

                loginInfoModel.disEvent('loginInfo', loginInfoModel.$data);
            } else {
                loginInfoModel.$data = {};

                NBOSS.tips.error("错误", data.reason, function() {
                    //TODO: 留着扩展

                });
            }
        });
    }
};