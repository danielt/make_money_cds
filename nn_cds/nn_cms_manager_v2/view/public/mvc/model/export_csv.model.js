/**
 * 统计model
 * 
 */
var export_csv_model = new Starcor.S67.MVC.Model("export_csv");
/**
 * 统计表，导出csv
 * @param params={data:[{data:要写入的数据}，{request_data:接口相关信息}...]}
 * 
 */
export_csv_model.query = function (params) {
    NBOSS.ajax(params, 'export_csv_file', function (data) {
        if (data.code == '1') {
            export_csv_model.data=data.data;
        	window.top.NBOSS.Alert('提示', '导出成功');
        } else {
            export_csv_model.data={};
            window.top.NBOSS.Alert('错误', data.reason);
        }
        export_csv_model.disEvent('query', export_csv_model.data);
    }, 'statistics/export_csv');
};
