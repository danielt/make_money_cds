/**
 * url查询model
 * Created by yxt on 2014/4/8.
 */
var url_query_model = new Starcor.S67.MVC.Model("url_query");
/**
 * @auth yxt
 * 获取页面类型列表
 * @param params 为空
 * 返回来的key为页面类型的id，name为页面类型的名称(用于显示)
 * 默认选中为详情页，
 * 当处于详情页的时候后面有一个输入框，可以输入影片信息，点击查询按钮查询出符合的列表，
 * 当处于列表页的时候后面输入框改为下拉选框，这个时候从服务器获取列表信息，面的查询按钮改为确定按钮，这个时候下面没有查询结果列表，
 * 当处于目录的时候，后面就只有一个确定按钮，因为目录是唯一的，
 * 当处于其他的时候，从服务器获取其他的列表，后面的查询按钮改为确定，下面的列表为空
 */
url_query_model.get_page_type = function (params) {
    NBOSS.ajax(params, 'get_page_type', function (data) {
        if (data.code == '1') {
            url_query_model.page_type = data.data;
        } else {
            url_query_model.page_type = {
            };
            window.top.NBOSS.Alert('错误', data.reason);
        }
        url_query_model.disEvent('get_page_type', url_query_model.page_type);
    }, 'weixin/url_query',true);
};
/**
 * @auth yxt
 * 获取子下拉选矿，只有页面类型为列表页、其他的时候才会调用
 * @param params={id:'前面处于选中状态的下拉选框的id'}
 * 返回来的id为页面类型的id，name为页面类型的名称(用于显示)
 */
url_query_model.query_child_select=function(params){
    NBOSS.ajax(params, 'query_child_select', function (data) {
        if (data.code == '1') {
            url_query_model.child_select = data.data;
        } else {
            url_query_model.child_select = {
            };
            window.top.NBOSS.Alert('错误', data.reason);
        }
        url_query_model.disEvent('query_child_select', url_query_model.child_select);
    }, 'weixin/url_query');
}

/**
 * @auth yxt
 * 查询影片
 * @param params={name:'输入框中输入的关键字'}
 * 返回来的id为页面类型的id，name为页面类型的名称(用于显示)
 */
url_query_model.query_video=function(params){
    NBOSS.ajax(params, 'query_video', function (data) {
        if (data.code == '1') {
            url_query_model.data = data.data;
        } else {
            url_query_model.data = {
            };
            window.top.NBOSS.Alert('错误', data.reason);
        }
        url_query_model.disEvent('query_video', url_query_model.data);
    }, 'weixin/url_query');
}