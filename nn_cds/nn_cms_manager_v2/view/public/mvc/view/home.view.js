var homeCategoryView = new Starcor.S67.MVC.View("homeMenu");

homeCategoryView.addEvent('homeMenu.homeMenu', function(data) {
    homePage.menuData = data;

    homePage.showHeadMenu();
});
// homeCategoryView.addEvent('homeMenu.get_precent', function(data) {
//     NBOSS.getTopDom('#laod_percent').text(data);
// });
var loginInfoView = new Starcor.S67.MVC.View("loginInfo");

loginInfoView.addEvent('login.logInfo', function(data) {
    homePage.showUserInfo(data.data);
});


var homePage = {
    menuData: {}
};
homePage.cookie_item='';
homePage.head_item='';
homePage.init = function() {
    homePage.rightcontent();
    NBOSS.closeDOM('#alertbox');
    NBOSS.frameID = 'alertframe';
    $('#alertlisten').click(function() {
        NBOSS.alert("权限提醒", "你没有访问次项权限！", NBOSS.optionssure);
    });

    $("#login_out").click(function() {
        homeCategoryView.call('login.logOut');
    });
    //获取后台登陆用户信息
    // loginInfoView.call('login.logInfo');
    homeCategoryView.call('homeMenu.homeMenu');
    $('html').keydown(function(){
        forbidden_ctrl_a()
    })
    $('html').keyup(function(){
        return_is_ctrl()
    })
};
homePage.rightcontent = function() {
    $('.subnav').find('a').click(function() {
        // if ($('#frameBox').length != 0) {
        //     $('#frameBox').remove();
        // }
        // var contentiframe = document.createElement("iframe");
        // contentiframe.src = $(this).attr("pos");
        // contentiframe.id = 'frameBox';
        // $('#myModal').before($(contentiframe));
        var contentiframe='';
        if ($('#frameBox').length == 0) {
            contentiframe = document.createElement("iframe");
            contentiframe.id = 'frameBox';
            $('#myModal').before($(contentiframe));
        }
        else{
            contentiframe=$('#frameBox')[0];
        }
        contentiframe.src = $(this).attr("pos");
    });
};

homePage.showHeadMenu = function() {
    for (var item in homePage.menuData) {
        var $tempHeadNav = $('<li>').appendTo($("#headnav"));
        var html='<div class="a_left"></div>';
        $tempHeadNav.append(html);
        (function(obj) {
            $('<a>', {
                "href": "javascript:void(0);"
            }).text(homePage.menuData[obj].name).appendTo($tempHeadNav).click(function() {
                
                var this_aindex=$(this).parent().prevAll("li").length;
                homePage.head_item=this_aindex;
                $("#leftnav").empty();
                $(".select").removeClass("select");
                $(this).parent().addClass("select");
                homePage.showCategory(homePage.menuData[obj].subMenuList, $("#leftnav"));
            });
        })(item);
        $tempHeadNav.append('<div class="a_right"></div>')
    }
    homePage.cookie_item=new get_cookie();
    if(homePage.cookie_item.head_menu_arr){
        $("#headnav").find('a').eq(homePage.cookie_item.head_menu_arr.pop()).click();
    }
    else{
        $("#headnav").find("a:first").click();
    }
};
homePage.showCategory = function(data, $contain) {
    //TODO: 处理首页展示栏目
    for (var i = 0; i < data.length; i++) {
        if (data[i].action == '1') {
            var $parentElement = $('<li>').appendTo($contain);

            $('<a>', {
                "href": "javascript:void(0);",
                "pos": data[i].template ? data[i].template : "html/example/usermanage.html"
            }).text(data[i].name).appendTo($parentElement).click(function(idx) {
                return function() {
                    NBOSS.is_e_back=false;
                    homePage.cookie_item=new get_cookie();
                    var this_aindex=0;
                    var is_link_a=false;
                    if(homePage.cookie_item.visite_aarr){
                        var self=this;
                        $('#leftnav').find("a[pos!='html/example/usermanage.html']").each(function(index){
                            if(this==self){
                                this_aindex=index;
                                is_link_a=true;
                                return false;
                            }
                            is_link_a=false;
                        })
                        if(is_link_a){
                            var this_visite_aarr=homePage.cookie_item.visite_aarr;
                            // this_visite_aarr.push(this_aindex)
                            // var visite_astr=this_visite_aarr.join(',')
                            set_cookie('visite_aarr',this_aindex);
                        }
                    }
                    else{
                        set_cookie('visite_aarr',this_aindex)
                    }
                    // if(homePage.cookie_item.head_menu_arr){
                    //     if(is_link_a){
                    //         var this_head_menu_arr=homePage.cookie_item.head_menu_arr;
                    //         this_head_menu_arr.push(homePage.head_item)
                    //         var head_menu_str=this_head_menu_arr.join(',')
                    //         set_cookie('head_menu_arr',head_menu_str);
                    //     }
                    // }
                    // else{
                    //     set_cookie('head_menu_arr',homePage.head_item)
                    // }
                    set_cookie('head_menu_arr',homePage.head_item)
                    homePage.saveCurrentModel(data[idx]);
                    homePage.showNavigation(data[idx].navigation);
                    $(this).parent().siblings().removeClass("leftselect");
                     $('.subnav').find('li').removeClass('leftselect');
                    $(this).parent().addClass("leftselect");
                    NBOSS.subpage_rights = data[idx].rights;
                    // if ($('#frameBox').length != 0) {
                    //     $('#frameBox').remove();
                    // }
                    $(this).parent().siblings().find(".subnav").slideUp(500);
                    if ($(this).next().hasClass("subnav")) {
                        //$(".subnav").hide();
                        $(this).next().slideDown(400);
                    }
                    var contentiframe='';
                    if ($('#frameBox').length == 0) {
                        contentiframe = document.createElement("iframe");
                        contentiframe.id = 'frameBox';
                        $('#myModal').before($(contentiframe));
                    }
                    else{
                        contentiframe=$('#frameBox')[0];
                    }
                    contentiframe.src = $(this).attr("pos");
                };
            }(i));

            if (data[i].subMenuList && data[i].subMenuList.length > 0) {
                var $subContain = $("<ul>", {
                    "class": "subnav"
                }).appendTo($parentElement);

                homePage.showCategory(data[i].subMenuList, $subContain);
            }

        } else if (data[i].action == '0') {
            if (data[i].subMenuList.length <= 0) {
                var $parentElement = $('<li>').appendTo($contain);

                $('<a>', {
                    "href": "javascript:void(0);",
                    "pos": data[i].template ? data[i].template : "html/example/usermanage.html"
                }).text(data[i].name).appendTo($parentElement).click(function(idx) {
                    //TODO: 根据需求添加操作
                    return function() {
                        homePage.saveCurrentModel(data[idx]);
                    };
                }(i));
            } else {
                var $parentElement = $('<li>').appendTo($contain);

                $('<a>', {
                    "href": "javascript:void(0);",
                    "pos": data[i].template ? data[i].template : "html/example/usermanage.html"
                }).text(data[i].name).appendTo($parentElement).click(function(idx) {
                    return function() {
                        homePage.saveCurrentModel(data[idx]);
                        $(this).parent().siblings().removeClass("leftselect");
                        $('.subnav').find('li').removeClass('leftselect');
                        $(this).parent().addClass("leftselect");
                            $(this).parent().siblings().find(".subnav").slideUp(500);
                        $(this).next().slideDown(400);
                    };
                }(i));

                var $subContain = $("<ul>", {
                    "class": "subnav"
                }).appendTo($parentElement);

                homePage.showCategory(data[i].subMenuList, $subContain);
            }
        }
    }
    if (!NBOSS.is_e_default) {
        return null
    };
    homePage.cookie_item=new get_cookie();
    if(homePage.cookie_item.visite_aarr&&homePage.cookie_item.head_menu_arr.pop()==homePage.head_item){
        var current_page=homePage.cookie_item.visite_aarr.pop()
        homePage.clickMenu($("#leftnav").find("a[pos!='html/example/usermanage.html']").eq(current_page));
        $("#leftnav").find("a[pos!='html/example/usermanage.html']").eq(current_page).click();
    }
    else{
        homePage.clickMenu($("#leftnav").find("a[pos!='html/example/usermanage.html']:first"));
        $("#leftnav").find("a[pos!='html/example/usermanage.html']:first").click();
    }
    
};

homePage.clickMenu = function($requireClickDom) {
    if ($requireClickDom.parent().parent().parent().is("li")) {
        if ($requireClickDom.parent().parent().prev().attr("pos") == 'html/example/usermanage.html') {
            $requireClickDom.parent().parent().prev().click();
            homePage.clickMenu($requireClickDom.parent().parent().prev());
        }
    }
};

homePage.saveCurrentModel = function(data) {
    NBOSS.userBehavior(data.model_id, data.id);
};

homePage.showNavigation = function(data) {
    $("#page_navigation").html(data.join(" / "));
};
/**
 * 显示后台登陆用户信息
 */
homePage.showUserInfo = function(data) {
    //TODO: 显示当前登录的用户，以及权限
    $("#login_info").html(data.name);
};
