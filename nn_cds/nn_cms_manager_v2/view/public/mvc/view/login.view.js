//创建视图对象
var loginView = new Starcor.S67.MVC.View("login");
//给admin模型的login方法添加回到事件
loginView.addEvent('login.check', function(data) {
    if (data.code == "000000") {//如果登陆成功，跳转到登陆页面
        window.location.href = NBOSS.BaseUrl() + '/view/home.html';
    } else {
        alert(data.reason);
    }
});

var loginpage = {};

loginpage.login = function() {
    var post = {
        "name": document.getElementById("username").value,
        "password": document.getElementById("userpwd").value,
    };
    //调用login模型login方法
    loginView.call('login.login', post);
};

$(document).ready(function() {
    //绑定登录点击事件
    $("#submit_btn").click(function() {
        loginpage.login();
    });
    $('html').bind('keydown',function(e){
        if(e.keyCode==13){
            $('#submit_btn').click();
        }
    });
});
