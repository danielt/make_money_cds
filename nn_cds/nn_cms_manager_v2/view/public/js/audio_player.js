var audio_player={
	playerID:'audio_player',
	player_url:''
}
audio_player.audio_play=function(player_url,callback){
	if(window.HTMLAudioElement){
		try{
			var oAudio=$('#'+this.playerID)[0];
			// oAudio.playbackRate=0;
			if (player_url&&this.player_url!=player_url){
				this.player_url=player_url;
				$('#'+this.playerID).attr("src",'../../../..'+player_url);
			}
			if (oAudio.paused) {
                oAudio.play();
            }
            // else{
            //     oAudio.pause();
            //     oAudio.currentTime=0;
            // }
		}
		catch (e) {
            // Fail silently but show in F12 developer tools console
            if(window.console && console.error("Error:" + e));
        }
	}
};
audio_player.audio_stop=function(callback){
	if(window.HTMLAudioElement){
		try{
			var oAudio=$('#'+this.playerID)[0];
                oAudio.pause();
                oAudio.currentTime=0;
		}
		catch (e) {
            // Fail silently but show in F12 developer tools console
            if(window.console && console.error("Error:" + e));
        }
	}
}