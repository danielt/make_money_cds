var home = {};
home.init = function() {
    $(".navtitle").click(function() {
        $(".navtitle").removeClass("leftselect");
        $(this).addClass("leftselect");
        $(".subnav").hide();
        $(this).next().slideDown("slow");
        $(this).next().find("a").click(function() {
            $(".subnav").find("a").removeClass("leftselect");
            $(this).addClass("leftselect");
        });
    });
    // 头部导航栏事件
    $(".headnav").children("li").each(function(index) {
        $(this).click(function() {
            $(".headnav").children().removeClass("select");
            $(this).addClass("select");
        });
    });
    home.rightcontent();
    NBOSS.closeDOM('#alertbox');
    NBOSS.frameID = 'alertframe';
    $("#close").click(function() {
        home.closeAlert();
    })
    $(".close").click(function() {
        home.closeAlert();
    })
    $('#alertlisten').click(function() {
        NBOSS.alert("权限提醒", "你没有访问次项权限！", NBOSS.options);
    })
//	NBOSS.tips.warning("受限","登录用户太多，请稍后登录");
}
home.closeAlert = function() {
    NBOSS.dialog.disEvent(NBOSS.dialog.DIALOG_CLOSE);
}
home.rightcontent = function() {
    $('.subnav').find('a').click(function() {
        if ($('#frameBox').length != 0) {
            $('#frameBox').remove();
        }
        var contentiframe = document.createElement("iframe");
        contentiframe.src = $(this).attr("pos");
        contentiframe.id = 'frameBox';
        $('#myModal').before($(contentiframe));
        NBOSS.reset();
    });
}

