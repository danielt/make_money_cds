var pageControl = {
    pageCount: 0,
    dataCount: 0,
    pageIndex: 0,
    pageShowNum: 10,
    $container: null,
    $pageShowArea: null,
    $pageJumpArea: null,
    pageShowNumFilterList: [10, 20, 30, 40, 50],
    callBack: function() {
    }
};


pageControl.init = function(option, callBack) {
    this.dataCount = option.dataCount;
    this.pageShowNum = option.pageShowNum > 1 ? option.pageShowNum : this.pageShowNum;
    this.pageCount = Math.ceil(this.dataCount / this.pageShowNum);
    this.pageIndex = option.pageIndex;
    this.$container = option.container;
    if (callBack) {
        this.callBack = callBack;
    }

    this.createEle();
    this.show();
};


pageControl.createEle = function() {
    var self = this;
    this.$container.empty();
    //页码展示区域。 
    this.$pageShowArea = $("<ul>", {
        "class": "pagination"
    }).appendTo(this.$container);

    var $firstPagebtn = $("<li>").appendTo(this.$pageShowArea);

    $("<a>", {
        "href": "javascript:void(0);"
    }).appendTo($firstPagebtn).html("&laquo;").click(function() {
        if (self.pageCount!=0) {
            pageControl.goPage(1);
        }
    });

    var $prePagebtn = $("<li>").appendTo(this.$pageShowArea);

    $("<a>", {
        "id": "pre_page_btn",
        "href": "javascript:void(0);"
    }).appendTo($prePagebtn).html("&lsaquo;").click(function() {
        if (self.pageIndex > 1) {
            pageControl.goPage(self.pageIndex - 1);
        }
    });

    var $currentPageBtn = $("<li>").appendTo(this.$pageShowArea);

    $("<a>", {
        "id": "page_show_ele",
        "href": "javascript:void(0);"
    }).appendTo($currentPageBtn).html(this.pageIndex);

    var $nextPagebtn = $("<li>").appendTo(this.$pageShowArea);

    $("<a>", {
        "id": "next_page_btn",
        "href": "javascript:void(0);"
    }).appendTo($nextPagebtn).html("&rsaquo;").click(function() {
        if (self.pageIndex < self.pageCount) {
            pageControl.goPage(Number(self.pageIndex) + 1);
        }
    });

    var $lastPagebtn = $("<li>").appendTo(this.$pageShowArea);

    $("<a>", {
        "href": "javascript:void(0);"
    }).appendTo($lastPagebtn).html("&raquo;").click(function() {
        if (self.pageCount!=0) {
            pageControl.goPage(self.pageCount);
        }
    });

    //通过输入页码，进行页面的跳转
    this.$pageJumpArea = $("<div>", {
        "class": "btn-group input-div",
    }).appendTo(this.$container);

    $("<input>", {
        "class": "form-control pageinput",
        "type": "text",
        "placeholder": this.pageIndex
    }).appendTo(this.$pageJumpArea);

    $("<button>", {
        "class": "btn btn-default pagebnt",
        "type": "button"
    }).text("Go!").appendTo(this.$pageJumpArea).click(function() {
        if (self.pageCount!=0) {
            pageControl.goPage($(this).prev().val() ? $(this).prev().val() : self.pageIndex);
        }
    });

    $("<span>", {
        "id": "show_page_count",
        "class": "pagefont pull_left"
    }).html("共" + "<strong>" + this.pageCount + "</strong>" + "页").appendTo(this.$container);

    $("<span>", {
        "id": "show_page_count",
        "class": "pagefont pull_left"
    }).text("每页显示").appendTo(this.$container);

    //插入每页显示多少条数据区域dom
    this.$pageShowNumArea = $("<div>", {
        "class": "btn-group input-div eachpagerow",
    }).appendTo(this.$container);

    $("<input>", {
        "class": "form-control pageinput",
        "type": "text",
        "placeholder": this.pageShowNum
    }).appendTo(this.$pageShowNumArea).val(this.pageShowNum).click(function() {
        $(this).parent().toggleClass("open");
        $(this).next().find("li").mouseup(function() {
            self.$pageShowNumArea.removeClass("open");
        });
    });
    $("<span>", {
        "id": "show_page_count",
        "class": "pagefont pull_left"
    }).text("行").appendTo(this.$pageShowNumArea);
    var $filterArea = $("<ul>", {
        "class": "dropdown-menu",
        "role": "menu",
        "aria-labelledby": "dropdownMenu3"
    }).appendTo(this.$pageShowNumArea);

    for (var i = 0; i < this.pageShowNumFilterList.length; i++) {
        (function(idx) {
            var $tempEle = $("<li>").appendTo($filterArea);

            $("<a>", {
                "href": "javascript:void(0);",
                "role": "menuitem",
                "tabindex": "-1"
            }).appendTo($tempEle).html(self.pageShowNumFilterList[idx]).click(function() {
                $(this).parent().parent().prev().val(self.pageShowNumFilterList[idx]);
                self.$pageShowNumArea.removeClass("open");

                self.pageShowNum = self.pageShowNumFilterList[idx];

                self.pageCount = Math.ceil(self.dataCount / self.pageShowNum);

                if (self.pageIndex > self.pageCount) {
                    self.pageIndex = self.pageCount;
                }
                self.goPage(self.pageIndex);
            });
        })(i);
    }

};


pageControl.show = function() {
    if(this.dataCount==0){
        this.$pageShowArea.find("a").parent().addClass("disabled");
    }
    if (this.pageIndex == 1||(this.pageCount==0)) {
        this.$pageShowArea.find("#pre_page_btn").parent().addClass("disabled");
    } else {
        this.$pageShowArea.find("#pre_page_btn").parent().removeClass("disabled");
    }

    if (this.pageIndex == this.pageCount||(this.pageCount==0)) {
        this.$pageShowArea.find("#next_page_btn").parent().addClass("disabled");
    } else {
        this.$pageShowArea.find("#next_page_btn").parent().removeClass("disabled");
    }

    this.$pageShowArea.find("#page_show_ele").text(this.pageIndex);
    this.$pageJumpArea.find("input").attr("placeholder", this.pageIndex);
};

pageControl.goPage = function(idx) {
    console.log(idx);

    if (idx > this.pageCount) {
        idx = this.pageCount;
    }

    this.pageIndex = idx;

    var option = {
        currentIndex: idx,
        pageShowNum: this.pageShowNum
    };

    this.callBack(option);
    this.show();
};



