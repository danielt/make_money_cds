// 分页下拉框事件
function tablebtnlisten(id) {
    // table工具栏事件
    $(id).each(function(index1) {
        $(this).find(".tablebtn-default").each(function(index) {
            $(this).click(function() {
                var checkboxlen = $('input[type|="checkbox"]').length;
                switch (index)
                {
                    case 0:
                        $('input[type|="checkbox"]').prop('checked', 'true');
                        break;
                    case 1:
                        for (var i = 0; i < checkboxlen; i++) {
                            if ($('input[type|="checkbox"]').eq(i).attr('checked')) {
                                $('input[type|="checkbox"]').eq(i).removeAttr('checked');
                            }
                            else {
                                $('input[type|="checkbox"]').eq(i).prop('checked', 'true');
                            }
                        }
                        break;
                    default:
                        break;
                }
            });
        });
    })
    // $('html').keydown(function(){
    //     forbidden_ctrl_a(true)
    // })
    // $('html').keyup(function(){
    //     return_is_ctrl()
    // })
    // window_reset();
}

function window_reset(){
    $('body').on("click","button",function(){
        var $this_contorl=$(this).next('.dropdown-menu');
        var change_height=$this_contorl.height();
        if(NBOSS.getTopDom('#frameBox').attr('height')<($(this).offset().top+$(window).scrollTop()+change_height+60)){
            NBOSS.getTopDom('#frameBox')[0].height=($(this).offset().top+$(window).scrollTop()+change_height+60);
        }
    })
    var url_arg=new Goto_Back.queryStringForUrl();
    window.top.Goto_Back.go_back(url_arg);
}

var is_ctrl=false;
function keydown_f(){   
        // if(event.keyCode==17){
        //     is_ctrl=true;
        // }
        // if(event.keyCode==65&&is_ctrl){
            // $(document).bind('body',function(){return false;});
            var checkboxlen = $('#frameBox').contents().find('input[type|="checkbox"]').length;
            for (var i = 0; i < checkboxlen; i++) {
                if ($('#frameBox').contents().find('input[type|="checkbox"]').eq(i).attr('checked')) {
                    $('#frameBox').contents().find('input[type|="checkbox"]').eq(i).removeAttr('checked');
                }
                else {
                    $('#frameBox').contents().find('input[type|="checkbox"]').eq(i).prop('checked', 'true');
                }
            }
        // }
}
function keydown_sub(){   
        // if(event.keyCode==17){
        //     is_ctrl=true;
        // }
        // if(event.keyCode==65&&is_ctrl){
            // $(document).bind('body',function(){return false;});
            var checkboxlen = $('input[type|="checkbox"]').length;
            for (var i = 0; i < checkboxlen; i++) {
                if ($('input[type|="checkbox"]').eq(i).parent().attr('checked')) {
                    $('input[type|="checkbox"]').eq(i).removeAttr('checked');
                }
                else {
                    $('input[type|="checkbox"]').eq(i).prop('checked', 'true');
                }
            }
        // }
}
function forbidden_ctrl_a(is_sub){
    console.log('键值'+event.keyCode);
    if(event.keyCode==17){
        is_ctrl=true;
    }
    if(event.keyCode==65&&is_ctrl){
        event.keyCode = 0;  
        event.returnValue = false;  
        if(is_sub){
            setTimeout(keydown_sub, 300);
        }
        else{
            setTimeout(keydown_f, 300);  
        }
        return false;  
    } 
    if($('#myModal2').css('display')!='none'&&event.keyCode==13){
        if($('#myModal2').find('button').length==1){
            $('#__alert_btn_sure').click();
            $('#__alert_btn_0').click();
        }
        else if($('#myModal2').find('button').length==2){
            $('#__alert_btn_1').click();
        }
    }
}
function return_is_ctrl(){
    if(event.keyCode==17){
        is_ctrl=false;
    }
}
function only_num(){
    $(".only_num").keyup(function(){
        this.value=this.value.replace(/[^\d]/g,'');
    })
    $(".only_num").blur(function(){
        exp=/[^\d]/g;
        if((exp.test(this.value))){
            $(this).parent().addClass("has-error");
            $(this).parent().find(".label-danger").html("只能为整数")
            this.value='';
        }
    })
}
//控制弹出框的方向向上
function pull_top() {
    $('.pull-top').css('top', -$('.dropdown-menu').outerHeight() - 6);
}
var inputlistener={

}
inputlistener.listen=function(id){
    var is_formok=false;
    id.find('input').next().removeClass("fail_show");
    id.find('input').next().html("");
    id.find('input').each(function(index){
        var $this_input=id.find('input').eq(index);
           if($this_input.val() == ""&&$this_input.hasClass('must_input')){
                $this_input.next().addClass("fail_show");
                $this_input.next().html("不能为空");
                is_formok=false;
                return false;
           }else{
                if($this_input.hasClass('max_len')){
                    var exp=$this_input.val().length;
                    var len=$this_input.attr('maxlen');
                    if(exp>len){
                        $this_input.next().addClass("fail_show");
                        $this_input.next().html("字符超过规定长度"+len);
                        is_formok=false;
                        return false;
                    }
                }
                else if($this_input.hasClass('max_len7')){
                    var exp=$this_input.val().length;
                    if(exp>7){
                        $this_input.next().addClass("fail_show");
                        $this_input.next().html("字符超过规定长度7");
                        is_formok=false;
                        return false;
                    }
                }
                else if($this_input.hasClass('number_letter')){
                    var exp=/[^a-z|^A-Z|^\d]/g;
                    if(exp.test($this_input.val())){
                        $this_input.parents('td').addClass("has-error");
                        $this_input.parents('td').find(".label-danger").html("只能为字母或者数字");
                        is_formok=false;
                        return false;
                    }
                }
                else if($this_input.hasClass('number_letter_icon')){
                    var exp=/[(a-z)(A-Z)(\d)(\ )(\~)(\!)(\@)(\#)(\$)(\%)(\^)(\&)(\*)(\()(\))(\-)(\_)(\+)(\=)(\[)(\])(\{)(\})(\|)(\\)(\;)(\:)(\')(\")(\,)(\.)(\/)(\<)(\>)(\?)(\)]+$/g;
                    if(!exp.test($this_input.val())){
                        $this_input.parents('td').addClass("has-error");
                        $this_input.parents('td').find(".label-danger").html("只能为字母，特殊字符或者数字");
                        is_formok=false;
                        return false;
                    }
                }
                else if($this_input.hasClass('number_letter_line')){
                    var exp=/[^a-z|^A-Z|^\d|^\-|^\,|^\[|^\]]/g;
                    if(exp.test($this_input.val())){
                        $this_input.parents('td').addClass("has-error");
                        $this_input.parents('td').find(".label-danger").html("只能为字母,横线,中括号,逗号,或者数字");
                        is_formok=false;
                        return false;
                    }
                }
                else if($this_input.hasClass('number_letter_dot')){
                    var exp=/[^\d|^.]/g;
                    if(exp.test($this_input.val())){
                        $this_input.parents('td').addClass("has-error");
                        $this_input.parents('td').find(".label-danger").html("只能为数字和“.”");
                        is_formok=false;
                        return false;
                    }
                }
                else if($this_input.hasClass('not_chanese')){
                    var exp=/[\u4E00-\u9FA5]/g;
                    if(exp.test($this_input.val())){
                        $this_input.parents('td').addClass("has-error");
                        $this_input.parents('td').find(".label-danger").html("不能含有中文字符");
                        is_formok=false;
                        return false;
                    }
                }
            }
            is_formok=true;
        });
    return is_formok
}

function quik_query_item(){
    var self=this;
    $('.quik_query').mouseover(function(){
        $('.quik_query').removeClass('quik_query_over');
        $(this).addClass('quik_query_over');
    })
    $('.quik_query').mouseout(function(){
        $(this).removeClass('quik_query_over');
    })
    $('.quik_query').mouseup(function(){
        // this.color: #28b779;
        var txt='';
        if (window.getSelection&&window.getSelection().toString()){  // mozilla FF
            txt = window.getSelection().toString();
           }
        else if (document.getSelection&&document.getSelection().toString()){
            txt = document.getSelection().toString();
            }
        else if (document.selection&&document.selection.createRange().text){  //IE
            txt = document.selection.createRange().text;
            }
        else{
            txt=$(this).attr('key_value')
        }
        $('#list').find('input').val('');
        $('#list select').each(function(){
            $(this).find('option').eq(0).prop('selected','true');
        })
        if($('#list select').length>0){
            $('#list select').select2();
        }
        var this_name=$(this).attr('pos');
        var $select_obj=$('#'+this_name);
        if($select_obj[0].tagName=='SELECT'){
            for(var i=0;i<$select_obj.find("option").length;i++){
                if($select_obj.find("option").eq(i).val()==txt){
                    $select_obj.find("option").eq(i).prop('selected','true')
                }
            }
            if($select_obj.length>0){
                $select_obj.select2();
            }
        }
        else{
            if(this_name=='id'){
                $('#device_id').val(txt);
            }
            else{
                $select_obj.val(txt);
            }
        }
        self.quik_val=txt;
        self.quik_name=this_name;
    });
}
function get_cookie(key){
    var strCookie=document.cookie; 
    //将多cookie切割为多个名/值对 
    var arrCookie=strCookie.split(";");  
    //遍历cookie数组，处理每个cookie对 
    for(var i=0;i<arrCookie.length;i++){ 
        var arr=arrCookie[i].split("="); 
        
        //找到名称为userId的cookie，并返回它的值
        if(arr.length>1) {
           this[arr[0].trim()]=arr[1];
           if(this[arr[0].trim()].split(",").length>0) {
            this[arr[0].trim()]=this[arr[0].trim()].split(",");
           }
        }
    } 
}
function set_cookie(key,value){
    document.cookie=key+"="+value;
}
function getCookie(name)//取cookies函数        
{
    var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));
     if(arr != null) return unescape(arr[2]); return null;

}
function delCookie(name)//删除cookie
{
    var exp = new Date();
    exp.setTime(exp.getTime() - 1);
    var cval=getCookie(name);
    if(cval!=null) document.cookie= name + "="+cval+";expires="+exp.toGMTString();
}
var Goto_Back={}
Goto_Back.go_back=function(url_arg){
    console.log("是否是后退"+NBOSS.is_e_back);
    console.log("是否是默认"+NBOSS.is_e_default);
    if(NBOSS.is_e_back==false){
        NBOSS.is_e_back=true;
        NBOSS.is_e_default=true;
        return null;
    }
    var this_head_menu=url_arg.headindex;
    var this_visite=url_arg.leftindex;
    console.log("上边"+this_head_menu);
    console.log("左边"+this_visite);
    set_cookie('visite_aarr',this_visite);
    set_cookie('head_menu_arr',this_head_menu);
    $(".select").removeClass("select");
    NBOSS.is_e_default=false;
    $('.headnav').find('a').eq(this_head_menu).click();
    var head_name=$('.headnav').find('li').eq(this_head_menu).text();
    $('.leftselect').removeClass('leftselect');
    $('.leftnav').find("a[pos!='html/example/usermanage.html']").eq(this_visite).addClass('leftselect');
    var left_name=$('.leftnav').find("a[pos!='html/example/usermanage.html']").eq(this_visite).text();
    $("#page_navigation").html(head_name+'/'+left_name);
    NBOSS.is_e_default=true;
}

function PreviewImage(fileObj,imgPreviewId,divPreviewId){  
        var allowExtention=".jpg,.bmp,.gif,.png";//允许上传文件的后缀名document.getElementById("hfAllowPicSuffix").value;  
        var extention=fileObj.value.substring(fileObj.value.lastIndexOf(".")+1).toLowerCase();              
        var browserVersion= window.navigator.userAgent.toUpperCase();  
        if(allowExtention.indexOf(extention)>-1){   
            if(fileObj.files){//HTML5实现预览，兼容chrome、火狐7+等  
                if(window.FileReader){  
                    var reader = new FileReader();   
                    reader.onload = function(e){  
                        document.getElementById(imgPreviewId).setAttribute("src",e.target.result);  
                    }    
                    reader.readAsDataURL(fileObj.files[0]);  
                }else if(browserVersion.indexOf("SAFARI")>-1){  
                    alert("不支持Safari6.0以下浏览器的图片预览!");  
                }  
            }else if (browserVersion.indexOf("MSIE")>-1){  
                if(browserVersion.indexOf("MSIE 6")>-1){//ie6  
                    document.getElementById(imgPreviewId).setAttribute("src",fileObj.value);  
                }else{//ie[7-9]  
                    fileObj.select();  
                    if(browserVersion.indexOf("MSIE 9")>-1)  
                        fileObj.blur();//不加上document.selection.createRange().text在ie9会拒绝访问  
                    var newPreview =document.getElementById(divPreviewId+"New");  
                    if(newPreview==null){  
                        newPreview =document.createElement("div");  
                        newPreview.setAttribute("id",divPreviewId+"New");  
                        newPreview.style.width = document.getElementById(imgPreviewId).width+"px";  
                        newPreview.style.height = document.getElementById(imgPreviewId).height+"px";  
                        newPreview.style.border="solid 1px #d2e2e2";  
                    }  
                    newPreview.style.filter="progid:DXImageTransform.Microsoft.AlphaImageLoader(sizingMethod='scale',src='" + document.selection.createRange().text + "')";                              
                    var tempDivPreview=document.getElementById(divPreviewId);  
                    tempDivPreview.parentNode.insertBefore(newPreview,tempDivPreview);  
                    tempDivPreview.style.display="none";                      
                }  
            }else if(browserVersion.indexOf("FIREFOX")>-1){//firefox  
                var firefoxVersion= parseFloat(browserVersion.toLowerCase().match(/firefox\/([\d.]+)/)[1]);  
                if(firefoxVersion<7){//firefox7以下版本  
                    document.getElementById(imgPreviewId).setAttribute("src",fileObj.files[0].getAsDataURL());  
                }else{//firefox7.0+                      
                    document.getElementById(imgPreviewId).setAttribute("src",window.URL.createObjectURL(fileObj.files[0]));  
                }  
            }else{  
                document.getElementById(imgPreviewId).setAttribute("src",fileObj.value);  
            }           
        }else{  
            alert("仅支持"+allowExtention+"为后缀名的文件!");  
            fileObj.value="";//清空选中文件  
            if(browserVersion.indexOf("MSIE")>-1){                          
                fileObj.select();  
                document.selection.clear();  
            }                  
            fileObj.outerHTML=fileObj.outerHTML;  
        }  
    }  