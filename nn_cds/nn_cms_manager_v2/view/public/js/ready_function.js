var ready_obj={

}
ready_obj.paly='';
ready_obj.open_dropmenu=function(){
	$('#table_list').on('click','.btn-group',function(){
		if($(this).hasClass('open')){
			$(this).removeClass('open');
		}
		else{
			$('.btn-group').removeClass('open');
			$(this).addClass('open');
		}
	})
}
ready_obj.open_select=function(){
	$('.select-group').live('click',function(){
		if($(this).hasClass('open')){
			$(this).removeClass('open');
		}
		else{
			$('.select-group').removeClass('open');
			$(this).addClass('open');
		}
	})
}
ready_obj.table_tr_over=function(){
	$('tbody').on('mouseover','tr',function(e){
		$(this).addClass('tr_over');
        e.stopPropagation();
	});
	$('tbody').on('mouseout','tr',function(e){
		$(this).removeClass('tr_over');
        e.stopPropagation();
	});
}
ready_obj.nav_change=function(){
	$('.nav_body').children().hide();
	$('.nav_body').children().eq(0).show();
	$('ul.nav_header').find('li').each(function(idx){
		$(this).click(function(){
			$('ul.nav_header').find('li').removeClass('select');
			$(this).addClass('select');
			$('.nav_body').children().hide();
			$('.nav_body').children().eq(idx).show();
		})
	});
}
ready_obj.triangles_change=function(){
	$('.custom_menu').on('click','.custom_menuli',function(){
		var $triangles=$(this).find('.triangles');
		$(this).toggleClass('open');
		if($triangles.hasClass('triangles_right')){
			$triangles.removeClass('triangles_right');
			$triangles.addClass('triangles_bottom');
		}
		else if($triangles.hasClass('triangles_bottom')){
			$triangles.removeClass('triangles_bottom');
			$triangles.addClass('triangles_right');
		}
	});
	$('.custom_menu').on('mouseover','li',function(e){
		$(this).children('.menu_opr').addClass('menu_opr_over');
		e.stopPropagation();
	});
	$('.custom_menu').on('mouseout','li',function(e){
		$(this).children('.menu_opr').removeClass('menu_opr_over');
		e.stopPropagation();
	});
	$('.custom_menu').on('click','li',function(e){
		$('.custom_menu li').removeClass('selected')
		$(this).addClass('selected');
		e.stopPropagation();
	})
}
ready_obj.audio_play=function(){
	$('.audioBox').live('click',function(){
		clearTimeout(ready_obj.paly);
		var self=this;
		$('.audioBox').not(this).removeClass('play');
		if($(this).hasClass('play')){
			audio_player.audio_stop();
			$(this).removeClass('play');
		}
		else{
			$(this).addClass('play');
			audio_player.audio_play($(this).attr('src_value'));
		}
		var time_len=Number($(this).attr('time_len'))*1000;
		ready_obj.paly=window.setTimeout(function(){
			$(self).removeClass('play');
			audio_player.audio_stop();
		},time_len)
	});
}
ready_obj.data_ready=function(){
	if($('.datepicker').length>0){
		$('.datepicker').each(function(){
			$(this).calendar();
		})
	}
}
var iframe_obj={};
iframe_obj.queryStringForUrl=function(){
        var name,value,i;
        var str=location.href;
        var num=str.indexOf("?");
        str=str.substr(num+1);
        var arrtmp=str.split("&");
        for(i=0;i<arrtmp.length;i++){
            num=arrtmp[i].indexOf("=");
            if(num>0){name=arrtmp[i].substring(0,num);value=arrtmp[i].substr(num+1);this[name]=value;}
        }
}
function showNavigation(){
    var url_arg=new iframe_obj.queryStringForUrl();
    var Navigation=[];
    if(url_arg.iscontrol&&$('#page_navigation').length==0){
       var hea_menu_name= window.top.NBOSS.getTopDom('.menubtn.select').find('a').text();
       Navigation.push(hea_menu_name);
       var left_menu=window.top.NBOSS.getTopDom('.left_frame').find('iframe').contents().find('a.select');
       left_menu_name1=left_menu.text();
       Navigation.push(left_menu_name1);
       if(left_menu.parent().find('.select').length>1){
          var left_menu_name2=left_menu.parent().find('.select a').text();
          Navigation.push(left_menu_name2);
       }
       var page_navigation=Navigation.join('/')
       var html='<div id="page_navigation">'+page_navigation+'</div>'
       $('body').prepend(html);
    }
}
$(document).ready(function() {
	ready_obj.open_dropmenu();
	ready_obj.table_tr_over();
	showNavigation();
	// ready_obj.data_ready();
	// ready_obj.nav_change();
});
