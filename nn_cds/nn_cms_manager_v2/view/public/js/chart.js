var myDate=new Date();

var _month = myDate.getMonth()+1;//返回值是 0（一月） 到 11（十二月） 之间的一个整数。
if(parseInt(_month) < 10){
	_month = "0"+ _month;
}
var _date = myDate.getDate();
if(parseInt(_date) < 10){
	_date = "0"+_date;
}
var _hours = myDate.getHours();
if(parseInt(_hours) < 10){
	_hours = "0"+_hours;
}
var _minutes = myDate.getMinutes();
if(parseInt(_minutes) < 10){
	_minutes = "0"+_minutes;
}
var _seconds=myDate.getSeconds();
if(parseInt(_seconds) < 10){
	_seconds = "0"+_seconds;
}

function data_to_week(dateStr){
	var weekDay = ["星期天", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
	//var dateStr = "2013-12-21";
	var myDate = new Date(Date.parse(dateStr.replace(/-/g, "/"))); 
	return (weekDay[myDate.getDay()]);
}

function GetDateStrgs(times) {
    if (times == "" || times == null) {
        return  "1997-01-01";
    } else {
        var request_begintime = (times).toString();
        if (request_begintime.length == 4) {
            return  request_begintime.slice(0, 4);
        } else if (request_begintime.length == 6) {
            return request_begintime.slice(0, 4) + '-' + request_begintime.slice(4, 6);
        } else if (request_begintime.length == 8) {
            return request_begintime.slice(0, 4) + '-' + request_begintime.slice(4, 6) + '-' + request_begintime.slice(6, 8);
        } else {
            return  "1997-01-01";
        }
    }
}

function checkSetId(charid)
{
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]")
    var rs = "";
    for (var i = 0; i < charid.length; i++) {
        rs = rs+charid.substr(i, 1).replace(pattern, '_');
    }
    return rs;

}
function GetDateStrgs_zh(times){
    var start_time_arry=times.split("-");
    if(start_time_arry.length==1){
        return start_time_arry[0];
    }else if(start_time_arry.length==2){
        return start_time_arry[0]+start_time_arry[1];
    }else if(start_time_arry.length==3){
        return start_time_arry[0]+start_time_arry[1]+start_time_arry[2];
    }else{
        return  "19970101";
    }

}


function GetDateStr(AddDayCount) 
{ 
	var dd = new Date(); 
	dd.setDate(dd.getDate()+AddDayCount);//获取AddDayCount天后的日期 
	var y = ( dd.getYear() < 1900 ) ? ( 1900 + dd.getYear() ) : dd.getYear(); 
	var m = dd.getMonth()+1;//获取当前月份的日期
    if(parseInt(m) < 10){
        m = "0"+ m;
    }
	var d = dd.getDate();
    if(parseInt( d) < 10){
        d = "0"+ d;
    }
	return y.toString()+m.toString()+d.toString();
} 
var show_chart_time = myDate.getFullYear()+"/"+_month+"/"+_date+" "+_hours+":"+_minutes+":"+_seconds;
var foot_title = "统计来源：CMS Copyright © Starcor "+show_chart_time; //底部标题全局变量，页面上面可以直接传这个参数，也可以直接传String字符串
var pie_preinstall_colors = ['#008081','#00be40','#bfc041','#ffff01','#ffbf41','#ff7f40','#ff3f40','#fe0000','#be0040','#81007f','#4100c0','#0000fe','#9955FF','#cc00ff','#7700ff ','#0000ff ','#00bbff ','#00ffff ','#00ff99 ','#77ff00 ','#bbff00','#00aa00','#bbbb00','#990099','#5555ff','#8c0044','#227700'];
var chart_preinstall_colors = ['#016bff','#349e00','#ff6400','#3a1d5f','#fd006b','#c78b00','#4100c0','#be0040', '#fe0000'];

/*获取一个随机颜色*/
function chart_get_color(str)
{
	var r = Math.floor(Math.random() * 255).toString(16);
	var g = Math.floor(Math.random() * 255).toString(16);
	var b = Math.floor(Math.random() * 255).toString(16);
	r = r.length == 1 ? "0" + r : r;
	g = g.length == 1 ? "0" + g : g;
	b = b.length == 1 ? "0" + b : b;
	var _over_color = "#" + r + g + b;
	if(str.indexOf(_over_color) != -1){
		chart_get_color(str)	
	}else{
		return _over_color;
	}
}

function  gettotlemin(num){
    var getmun_leng=num.toString();
    var getfirst="1";
    for(var i=0;i<getmun_leng.length-1;i++){
        getfirst+= "0";
    }
    var getnum=Math.floor(num/parseInt(getfirst));
    var getminnum=getnum*parseInt(getfirst);
    return getminnum;
}

function  gettotlmax(num){
    var getmun_leng=num.toString();
    var getfirst="1";
    var getnum=0;
    var getminnum=0;
    if(getmun_leng.length>2) {
        for (var i = 0; i < getmun_leng.length - 1; i++) {
            getfirst += "0";
        }
        var get_nofloornum = (num / parseInt(getfirst)).toString();
//    var get_onenum=parseInt(get_nofloornum.slice(0,1));
//    var get_basenum=parseInt(get_nofloornum.toString().slice(0,1))+0.5;
//    var get_new_num=get_onenum;
//    if(get_nofloornum.length!=1){
//        var get_twonum=(parseInt(get_nofloornum.slice(2,3))+1)/10;
//        get_new_num=get_new_num+get_twonum;
//    }

        //var getminnum=get_new_num*parseInt(getfirst);
         getminnum = Math.ceil(get_nofloornum) * parseInt(getfirst);
    }else if(getmun_leng.length<=1){
        getminnum = 10;
    }else if(getmun_leng.length==2){
        getminnum = 100;
    }
    return getminnum;
}
/*function  gettotlmax(num){
    var getmun_leng=num.toString();
    var getfirst="1";
    var getnum=0;
    for(var i=0;i<getmun_leng.length-1;i++){
        getfirst+= "0";
    }
    var get_nofloornum=(num/parseInt(getfirst)).toString();
    var get_onenum=parseInt(get_nofloornum.slice(0,1));
    var get_basenum=parseInt(get_nofloornum.toString().slice(0,1))+0.5;
    var get_new_num=get_onenum;
    var get_twonum=0;
    if(get_nofloornum.length!=1){
        if(get_nofloornum.length<=5){
            get_twonum=(parseInt(get_nofloornum.slice(2,3))+1)/10;
        } else if(get_nofloornum.length==6){
            get_twonum=(parseInt(get_nofloornum.slice(2,3))+1)/100;
        }else if(get_nofloornum.length==7){
            get_twonum=(parseInt(get_nofloornum.slice(3,4))+1)/1000;
        }
        else if(get_nofloornum.length==8){
            get_twonum=(parseInt(get_nofloornum.slice(4,5))+1)/10000;
        }
        else if(get_nofloornum.length==9){
            get_twonum=(parseInt(get_nofloornum.slice(6,8))+1)/100000;
        }
        else if(get_nofloornum.length==10){
            get_twonum=(parseInt(get_nofloornum.slice(8,9))+1)/1000000;
        }
        else if(get_nofloornum.length==11){
            get_twonum=(parseInt(get_nofloornum.slice(10,11))+1)/10000000;
        }
        else if(get_nofloornum.length==12){
            get_twonum=(parseInt(get_nofloornum.slice(11,12))+1)/100000000;
        }else {
            get_twonum=(parseInt(get_nofloornum.slice(12,13))+1)/1000000000;
        }
        get_new_num=get_new_num+get_twonum;
    }

    var getminnum=Math.ceil(get_new_num*parseInt(getfirst));

    return getminnum;
}*/
/*k线图*/
function show_chart_1(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, y_title, data, ption_dw){
	/*
	**********支持一条k线***************
	*chart_title 图表名称标题
	*chart_min_title 小标题
	*ption_time 每个节点鼠标悬停标题
	*right_down_title 右下角标题
	*x_title x轴底部标题
	*y_title y轴 标题
	*data 数据源
	*ption_dw 鼠标悬停单位
	
	var data = [
				{
					name : y_title,
					value: x_value,
					color:'#2f6cb3',
					line_width:2
				},
			 ];
	*/
	var chart = new iChart.LineBasic2D({
		render : dom_Obj,
		data: data,
		align:'center',
		animation: true,
		title : {
			text: chart_title,
			fontsize:24,
			color:'#23507c'
		},
		tip:{
			enable:true,
			shadow:true,
			listeners:{
			 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
			parseText:function(tip,name,value,text,i){
				return "<span style='color:#005268;font-size:12px;'>时间:"+ption_time[i]+"<br/>"+
				"</span><span style='color:#005268;font-size:12px;'>"+y_title+":"+value+ption_dw+"</span>";
				}
			}
		},
		subtitle : {
			text: chart_min_title,
			fontsize:14,
			color:'#4d4d4d'
		},
		footnote : {
			text : right_down_title,
			color : '#909090',
			fontsize : 11,
			padding : '0 38'
		},
		width : 1080,
		height : 435,
		border : false, //是否显示外边框
		shadow:false, //开启条形阴影
		shadow_color : '#ff00ff', //阴影颜色
		shadow_blur : 4,
		shadow_offsetx : 0,
		shadow_offsety : 2,
		background_color:'#ffffff', //配置背景填充颜色
		legend : {
			enable : true
		},
		crosshair:{
			enable:true,
			line_width:2,
			line_color:'#3f8695'
		},
		sub_option : {
//			smooth : true,//平滑曲线
			label:false,
			hollow:false,
			hollow_color: '#2f6cb3',
			hollow_inside:false,
			point_size:5
		},
		coordinate:{
			width:870,
			height:260,
			grid_color:'#9e9e9e',
			axis:{
				color:'#3d3d3d',
				width:[0,0,2,2]
			},
			scale:[{
				 position:'left',
				 gradient: true,
				 start_scale:0,
				 end_scale:100,
				 scale_size:2,
				 label : {color:'#3d3d3d',fontsize:13},
				 scale_color:'#9f9f9f'
			},{
				 position:'bottom',	
				 label : {color:'#3d3d3d',fontsize:13},
				 labels:x_title
			}]
		}
	});
	
	//开始画图
	chart.draw();
};


/*k线图*/
function show_chart_2(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, data, y_title){
	/*
	**********支持一条k线***************
	*chart_title 图表名称标题
	*chart_min_title 小标题
	*ption_time 每个节点鼠标悬停标题
	*right_down_title 右下角标题
	*x_title x轴底部标题
	*y_title y轴 标题
	*data 数据源
	*/
	var chart = new iChart.LineBasic2D({
		render : dom_Obj,
		data: data,
		align:'center',
		animation: true,
		title : {
			text: chart_title,
			fontsize:24,
			color:'#23507c'
		},
		tip:{
			enable:true,
			shadow:true,
			move_duration:400,
			border:{
				 enable:true,
				 radius : 5,
				 width:2,
				 color:'#3f8695'
			},
			listeners:{
			 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
			parseText:function(tip,name,value,text,i){
				return "<span style='color:#333;font-size:12px;'>"+name+":"+value+y_title+"</span>";
				}
			}
		},
		tipMocker:function(tips,i){
			var __temp = "<div style='font-weight:600'>时间:"+
					ption_time[i]+
					"</div>"+tips.join("<br/>");
			return __temp;
		},
		subtitle : {
			text: chart_min_title,
			fontsize:14,
			color:'#4d4d4d'
		},
		footnote : {
			text : right_down_title,
			color : '#909090',
			fontsize : 11,
			padding : '0 38'
		},
		width : 1080,
		height : 435,
		border : false, //是否显示外边框
		shadow:false, //开启条形阴影
		shadow_color : '#ff00ff', //阴影颜色
		shadow_blur : 4,
		shadow_offsetx : 0,
		shadow_offsety : 2,
		background_color:'#ffffff', //配置背景填充颜色
		legend : {
			enable : true,
			row:1,//设置在一行上显示，与column配合使用
			column : 'max',
			valign:'top',
			sign:'bar',
			background_color:null,//设置透明背景
			offsetx:-80,//设置x轴偏移，满足位置需要
			border : true
		},
		crosshair:{
			enable:true,
			line_color:'#3f8695',
			line_width:2
		},
		sub_option : {
//			smooth : true,//平滑曲线
			label:false,
			hollow:false,
			hollow_color: '#2f6cb3',
			hollow_inside:false,
			point_size:5
		},
		coordinate:{
			width:870,
			height:260,
			grid_color:'#9e9e9e',
			axis:{
				color:'#3d3d3d',
				width:[0,0,2,2]
			},
			scale:[{
				 position:'left',
				 gradient: true,
				 start_scale:0,
				 end_scale:100,
				 scale_size:2,
				 label : {color:'#3d3d3d',fontsize:13},
				 scale_color:'#9f9f9f'
			},{
				 position:'bottom',	
				 label : {color:'#3d3d3d',fontsize:13},
				 labels:x_title
			}]
		}
	});
	/*
	//利用自定义组件构造左侧说明文本
	chart.plugin(new iChart.Custom({
			drawFn:function(){
				//计算位置
				var coo = chart.getCoordinate(),
					x = coo.get('originx'),
					y = coo.get('originy');
				//在左上侧的位置，渲染一个单位的文字
				chart.target.textAlign('start')
				.textBaseline('bottom')
				.textFont('600 11px Verdana')
				.fillText(y_title,x-45,y-20,false,'#6d869f');
				
			}
	}));
	*/
	//开始画图
	chart.draw();
};


function show_chart_4(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, y_title, data, ption_dw){
	/*

	****************第一个是成功了，第二个是非法率**********************


	****** 这个K线图显示百分比，，，成功率使用
	**********支持一条k线***************
	*chart_title 图表名称标题
	*chart_min_title 小标题
	*ption_time 每个节点鼠标悬停标题
	*right_down_title 右下角标题
	*x_title x轴底部标题
	*y_title y轴 标题
	*data 数据源
	*ption_dw 鼠标悬停单位
	
	*/
	var chart = new iChart.LineBasic2D({
		render : dom_Obj,
		data: data,
		align:'center',
		animation: true,
		title : {
			text: chart_title,
			fontsize:24,
			color:'#23507c'
		},
		tip:{
			enable:true,
			shadow:true,
			listeners:{
			 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
			parseText:function(tip,name,value,text,i){
				var _html = "";
				var _p = "";
				if(name == "非法率"){
					_html = "<span style='color:#333;font-size:12px;'>非法比例:"+value+"%</span><br>";
					_html += "<span style='color:#333;font-size:12px;'>非法次数:"+data[1].fail_check_auth_count[i]+"</span><br>";
				}else if(name == "成功率"){
					_html = "<span style='color:#333;font-size:12px;'>成功比例:"+value+"%</span><br>";
					_html += "<span style='color:#333;font-size:12px;'>成功次数:"+data[0].succ_count[i]+"</span><br>";
					if(typeof(data[0].fail_percent) != "undefined"){
						_html += "<span style='color:#333;font-size:12px;'>失败比例:"+data[0].fail_percent[i]+"</span><br>";
					}
					_html += "<span style='color:#333;font-size:12px;'>失败次数:"+data[0].fail_count[i]+"</span>";
				}

				return _html;
				}
			}
		},
		tipMocker:function(tips,i){
			var __temp = "<div style='font-weight:600'>时间:"+
					ption_time[i]+
					"</div>"+tips.join("<br/>");
			return __temp;
		},
		subtitle : {
			text: chart_min_title,
			fontsize:14,
			color:'#4d4d4d'
		},
		footnote : {
			text : right_down_title,
			color : '#909090',
			fontsize : 11,
			padding : '0 38'
		},
		width : 1080,
		height : 410,
		border : false, //是否显示外边框
		shadow:false, //开启条形阴影
		shadow_color : '#ff00ff', //阴影颜色
		shadow_blur : 4,
		shadow_offsetx : 0,
		shadow_offsety : 2,
		background_color:'#ffffff', //配置背景填充颜色
		legend : {
			enable : true
		},
		crosshair:{
			enable:true,
			line_width:2,
			line_color:'#3f8695'
		},
		sub_option : {
//			smooth : true,//平滑曲线
			label:false,
			hollow:false,
			hollow_color: '#2f6cb3',
			hollow_inside:false,
			point_size:5
		},
		coordinate:{
			width:870,
			height:260,
			grid_color:'#9e9e9e',
			axis:{
				color:'#3d3d3d',
				width:[0,0,2,2]
			},
			scale:[{
				 position:'left',
				 gradient: true,
				 start_scale:0,
				 end_scale:100,
				 scale_size:2,
				 label : {color:'#3d3d3d',fontsize:13},
				 scale_color:'#9f9f9f'
			},{
				 position:'bottom',	
				 label : {color:'#3d3d3d',fontsize:13},
				 labels:x_title
			}]
		}
	});
	
	//开始画图
	chart.draw();
};


function show_chart_3(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, data){

	
};
/*流量图表*/
function show_area_1(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, data, ption_value_unit){
	/*
	**********支持多条面积图***************
	*chart_title 图表名称标题
	*chart_min_title 小标题
	*ption_time 每个节点鼠标悬停标题
	*right_down_title 右下角标题
	*x_title x轴底部标题
	*data 数据源
	*ption_value_unit 鼠标悬停值的单位
	*/
	
	 
	//创建x轴标签文本   
	var chart = new iChart.Area2D({
		render : dom_Obj,
		data: data,
		area_opacity : 0.1,
		animation: true,
		border : false, //是否显示外边框
		align:'center',
		title : {
			text: chart_title,
			fontsize:24,
			color:'#23507c'
		},
		subtitle : {
			text: chart_min_title,
			fontsize:14,
			color:'#4d4d4d'
		},
		footnote : {
			text : right_down_title,
			color : '#909090',
			fontsize : 11,
			padding : '0 38'
		},
		width : 1080,
		height : 435,
		background_color:'#ffffff',
		tip:{
			enable:true,
			shadow:true,
			move_duration:400,
			border:{
				 enable:true,
				 radius : 5,
				 width:2,
				 color:'#3f8695'
			},
			listeners:{
				 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
				parseText:function(tip,name,value,text,i){
					return name+":"+value+ption_value_unit;
				}
			}
		},
		legend : {
			enable : true
		},
		tipMocker:function(tips,i){
			var __temp = "<div style='font-weight:600'>时间:"+
					ption_time[i]+
					"</div>"+tips.join("<br/>");
			return __temp;
		},
		legend : {
			enable : true,
			row:1,//设置在一行上显示，与column配合使用
			column : 'max',
			valign:'top',
			sign:'bar',
			background_color:null,//设置透明背景
			offsetx:-80,//设置x轴偏移，满足位置需要
			border : true
		},
		crosshair:{
			enable:true,
			line_color:'#3f8695',
			line_width:2
		},
		sub_option : {
//			smooth : true,//平滑曲线
			label:false,
			hollow:false,
			hollow_inside:false,
			point_size:5
		},
		coordinate:{
			width:928,
			height:260,
			grid_color:'#9e9e9e',
			axis:{
				color:'#3d3d3d',
				width:[0,0,2,2]
			},
			scale:[{
				 position:'left',	
				 start_scale:0,
				 scale_size:2,
				 
				 scale_color:'#9e9e9e'
			},{
				 position:'bottom',	
				 labels:x_title
			}]
		}
	});
	
	
	
	//开始画图
	chart.draw();
}

function show_area_2(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, data, ption_value_unit){
	/*
	**********支持多条面积图***************
	*chart_title 图表名称标题
	*chart_min_title 小标题
	*ption_time 每个节点鼠标悬停标题
	*right_down_title 右下角标题
	*x_title x轴底部标题
	*data 数据源
	*ption_value_unit 鼠标悬停值的单位
	*/
	
	
	
	 
	//创建x轴标签文本   
	var chart = new iChart.Area2D({
		render : dom_Obj,
		data: data,
		area_opacity : 0.1,
		animation: true,
		border : false, //是否显示外边框
		align:'center',
		title : {
			text: chart_title,
			fontsize:24,
			color:'#23507c'
		},
		subtitle : {
			text: chart_min_title,
			fontsize:14,
			color:'#4d4d4d'
		},
		footnote : {
			text : right_down_title,
			color : '#909090',
			fontsize : 11,
			padding : '0 38'
		},
		width : 1080,
		height : 415,
		background_color:'#ffffff',
		tip:{
			enable:true,
			shadow:true,
			move_duration:400,
			border:{
				 enable:true,
				 radius : 5,
				 width:2,
				 color:'#3f8695'
			},
			listeners:{
				 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
				parseText:function(tip,name,value,text,i){
					return name+":"+value+ption_value_unit;
				}
			}
		},
		legend : {
			enable : true
		},
		tipMocker:function(tips,i){
			var __temp = "<div style='font-weight:600'>时间:"+
					ption_time[i]+
					"</div>"+tips.join("<br/>");
			return __temp;
		},
		legend : {
			enable : true,
			row:1,//设置在一行上显示，与column配合使用
			column : 'max',
			valign:'top',
			sign:'bar',
			background_color:null,//设置透明背景
			offsetx:-80,//设置x轴偏移，满足位置需要
			border : true
		},
		crosshair:{
			enable:true,
			line_color:'#3f8695',
			line_width:2
		},
		sub_option : {
//			smooth : true,//平滑曲线
			label:false,
			hollow:false,
			hollow_inside:false,
			point_size:5
		},
		coordinate:{
			width:928,
			height:260,
			grid_color:'#9e9e9e',
			axis:{
				color:'#3d3d3d',
				width:[0,0,2,2]
			},
			scale:[{
				 position:'left',	
				 start_scale:0,
				 scale_size:2,
				 
				 scale_color:'#9e9e9e'
			},{
				 position:'bottom',	
				 labels:x_title
			}]
		}
	});
	
	
	
	//开始画图
	chart.draw();
}


function show_bar_2(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, x_value,getmaxSale, y_title){
    var getmaxnum=Math.ceil(gettotlmax(getmaxSale));
    var chart = new iChart.ColumnMulti2D({

        render : dom_Obj,
        data : x_value,
        border : false, //是否显示外边框
        animation: true,
        labels:x_title,
        title : {
            text : chart_title,
            color : '#23507c',
            fontsize: 24
        },
        subtitle : {
            text : chart_min_title,
            fontsize : 14,
            color : '#4d4d4d'
        },
        footnote : {
            text : right_down_title,
            color : '#909090',
            fontsize : 11,
            padding : '0 38'
        },
        width : 1020,
        height : 430,
        label : {
            fontsize:11,
            color : '#666666'
        },
        tip:{
            enable:true,
            listeners:{
                //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
                parseText:function(tip,name,value,text,i){
				 var f = new String(value);
                            f = f.split("").reverse().join("").replace(/(\d{3})/g,"$1,").split("").reverse();
                            if(f[0]==','){
                                f.shift();
                            }
                            f = f.join("");
                   return "数量："+f;

                }
            }
        },
        shadow : true,
        shadow_blur : 2,
        shadow_color : '#aaaaaa',
        shadow_offsetx : 1,
        shadow_offsety : 0,
        column_width : 62,
        legend:{
            enable:true,
            valign:'top',
            background_color : null,
            border : {
                enable : false
            }
        },
        sub_option : {
            label : false,
            border : {
                width : 2,
                color : '#ffffff'
            },
            listeners : {
                parseText : function(r, t) {
                    return t;
                }
                /**
                 * r:iChart.Sector2D对象
                 * e:eventObject对象
                 * m:额外参数
                 */
//                click:function(r,e,m){
//
//                        var index = dom_Obj.split("_")[1];
//                        show_pie_2(dom_Obj, chart_title, foot_title, x_value, "", function () {
//                        });
//                        //show_pie_2(dom_Obj,'',foot_title,data.全部,"用户");
//                }
            }
        },
        coordinate:{
            background_color : null,
            grid_color : '#c0c0c0',
            width : 890,
            height:260,
            axis : {
                color : '#c0d0e0',
                width : [0, 0, 1, 0]
            },
            scale:[{
                position:'left',
               // start_scale:0,
                end_scale:getmaxnum,
               // scale_space:20
            }]

        }
    });
    chart.draw();
}

function show_bar_1(dom_Obj, chart_title, chart_min_title, ption_time, right_down_title, x_title, x_value, x_value_count, y_title,isclick){
    /*
     **********柱状图，支持单组数据************
     *chart_title 图表名称标题
     *chart_min_title 小标题
     *ption_time 每个节点鼠标悬停标题
     *right_down_title 右下角标题
     *x_title x轴底部标题
     *x_value x轴值，数组
     *x_value_count x数据总数
     *y_title y轴 标题
     */

   // var _totle_count=0;
    var _max_num=0;
    var min_num=x_value[0].value;
    var getminnum=gettotlemin(min_num);
    var _max_click=0
   for(var i=0;i<x_value.length;i++) {
       if (_max_click < parseInt(x_value[i].value)) {
           _max_click = parseInt(x_value[i].value);
       }
   }
    var getmaxnum=Math.ceil(gettotlmax(_max_click));
    if(getminnum==getmaxnum||getminnum.toString().length!=getmaxnum.toString().length){
        getminnum=0;
    }
    var chart = new iChart.Column2D({
        render : dom_Obj,
        data : x_value,
        border : false, //是否显示外边框
        animation: true,
        title : {
            text : chart_title,
            color : '#23507c',
            fontsize: 24
        },
        subtitle : {
            text : chart_min_title,
            fontsize : 14,
            color : '#4d4d4d'
        },
        footnote : {
            text : right_down_title,
            color : '#909090',
            fontsize : 11,
            padding : '0 38'
        },
        width : 1040,
        height : 430,
        label : {
            fontsize:11,
            textAlign:'right',
            textBaseline:'middle',
            rotate:0,
            color : '#666666',
            offsetx: 20
        },
        tip:{
            enable:true,
            listeners:{
                //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
                parseText:function(tip,name,value,text,i){
				 var f = new String(value);
                            f = f.split("").reverse().join("").replace(/(\d{3})/g,"$1,").split("").reverse();
                            if(f[0]==','){
                                f.shift();
                            }
                            f = f.join("");
                   // return name+":" + value;// + "<br>百分比:"+ (Math.floor(value / x_value_count * 100)+1) + "%";
            if(ption_time[i]==""||ption_time[i]==null){
                return "名称："+name+"<br>数量："+f;
            }else{
                return "时间："+ption_time[i]+"<br>数量："+f;
            }

                }
            }
        },

        shadow : true,
        shadow_blur : 2,
        shadow_color : '#aaaaaa',
        shadow_offsetx : 1,
        shadow_offsety : 0,
        column_width : 62,
        sub_option : {
            label : false,
            border : {
                width : 2,
                color : '#ffffff'
            },
            listeners : {
                parseText : function(r, t) {
                    return t;
                },
                /**
                 * r:iChart.Sector2D对象
                 * e:eventObject对象
                 * m:额外参数
                 */
//                click:function(r,e,m) {
//
//                    var index = dom_Obj.split("_")[1];
//                    show_pie_2(dom_Obj, chart_title, foot_title, x_value, "", function () {
//                    });
//
//                }
            }
        },
        coordinate : {
            background_color : null,
            grid_color : '#c0c0c0',
            width : 900,
            height:260,
            axis : {
                color : '#c0d0e0',
                width : [0, 0, 1, 0]
            },
            scale : [{
                //end_scale: 10,//最大刻度值
               // scale_share: 5,//刻度数量
                position : 'left',
               start_scale :getminnum,//起始刻度值
                end_scale:getmaxnum,//最大刻度值
                scale_size:2,
                scale_enable : false,
                label : {
                    fontsize:11,
                    color : '#666666'
                },
                listeners:{
                    parseText:function(t,x,y){
                        return {text:t}
                    }
                }
            }]
        }
    });

    //利用自定义组件构造左侧说明文本
    chart.plugin(new iChart.Custom({
        drawFn:function(){
            //计算位置
            var coo = chart.getCoordinate(),
                x = coo.get('originx'),
                y = coo.get('originy');
            //在左上侧的位置，渲染一个单位的文字
            chart.target.textAlign('start')
                .textBaseline('bottom')
                .textFont('600 11px Verdana')
                .fillText(y_title,x-45,y-20,false,'#6d869f');

        }
    }));

    chart.draw();
}

/*饼状图1*/

function show_pie_1(dom_Obj, chart_title, chart_min_title, right_down_title, pic_data, ption_title) {
    /*
     **********饼状图，支持单组数据************
     *chart_title 图表名称标题
     *right_down_title 右下角标题
     *pic_data数据源
     */

    var chart = new iChart.Pie2D({
        render: dom_Obj,
        data: pic_data,
        animation: true,
        border: false,
        title: {
            text: chart_title,
            color: '#3e576f'
        },
        subtitle: {
            text: chart_min_title,
            fontsize: 12,
            color: '#4d4d4d'
        },
        legend: {
            enable: true
        },
        tip: {
            enable: true,
            listeners: {
                //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
                parseText: function (tip, name, value, text, i) {
                    return ption_title + ":" + pic_data[i].count;// + "<br>百分比:" + pic_data[i].value + "%";
                }
            }
        },
        footnote: {
            text: right_down_title,
            color: '#909090',
            fontsize: 11,
            padding: '0 38'
        },
        sub_option: {
            label: {
                background_color: null,
                sign: false,//设置禁用label的小图标
                padding: '0 4',
                border: {
                    enable: false,
                    color: '#666666'
                },
                fontsize: 11,
                fontweight: 600,
                color: '#4572a7'
            },
            listeners: {
                parseText: function (r, t) {
                    return t;
                }
                /**
                 * r:iChart.Sector2D对象
                 * e:eventObject对象
                 * m:额外参数
                 */

            },
            border: {
                width: 2,
                color: '#ffffff'
            }
        },
        shadow: true,
        shadow_blur: 6,
        shadow_color: '#aaaaaa',
        shadow_offsetx: 0,
        shadow_offsety: 0,
        background_color: '#fefefe',
        offsetx: -60,//设置向x轴负方向偏移位置60px
        offset_angle: -120,//逆时针偏移120度
        showpercent: true,
        decimalsnum: 2,
        width: 1000,
        height: 500,
        radius: 120
    });
    //利用自定义组件构造右侧说明文本
    chart.plugin(new iChart.Custom({
        drawFn: function () {
            //计算位置
            var y = chart.get('originy'),
                w = chart.get('width');

            //在右侧的位置，渲染说明文字
            chart.target.textAlign('start')
                .textBaseline('middle')
                .textFont('600 16px Verdana');
        }
    }));

    chart.draw();
};

//饼状图2
function show_pie_2(dom_Obj, chart_title, right_down_title, pic_data, ption_title,callback,danwei){
	/*
	**********小 饼状图，支持单组数据************
	*chart_title 图表名称标题
	*right_down_title 右下角标题
	*pic_data数据源
	*/
    var heights=550;
   /* if(pic_data.length>14&&pic_data.length<20){
        heights=1000;
    }else if(pic_data.length>20&&pic_data.length<40){
        heights=1500;
    }*/
	var chart = new iChart.Pie2D({
		render : dom_Obj,
		data: pic_data,
		animation: true,
		title : {
			text : chart_title,
			color : '#3e576f'

		},
		border:{
			enable:false
		},
		legend : {
			enable : true,
           // align:'left',
            border:false,
            padding:0
          //  background_color : null//透明背景
		},
		//animation_duration:700,//700ms完成动画
		tip:{
			enable:true,
			listeners:{
				 //tip:提示框对象、name:数据名称、value:数据值、text:当前文本、i:数据点的索引
				/*parseText:function(tip,name,value,text,i){
					return ption_title + ":"+pic_data[i].count;// + "<br>百分比:" + pic_data[i].value + "%";
				}*/
				parseText:function(tip,name,value,text){
						//return name+":"+(value/this.get('total') * 100).toFixed(2)+ "%<br/>点击进入"+chart_title+"详情";
						return name+":"+(value/this.get('total') * 100).toFixed(2)+ "%<br/>";
						
				}
			}
		},

		footnote : {
			text : right_down_title,
			color : '#909090',
			fontsize : 11,
			padding : '0 0 0 60',
            textAlign:'left'
		},
		sub_option : {
			listeners:{
					/**
					 * r:iChart.Sector2D对象
					 * e:eventObject对象
					 * m:额外参数
					 */
					 //饼形图描述
					parseText : function(r, t,e) {
							//return t;
                            var f = new String(r.get('value'));
                            f = f.split("").reverse().join("").replace(/(\d{3})/g,"$1,").split("").reverse();
                            if(f[0]==','){
                                f.shift();
                            }
                            f = f.join("");
                        if(danwei==""||danwei==null){
                            return r.get('name')+':'+f+'部';
                        }else{
                            return r.get('name')+':'+f+'人';
                        }

					},
					click:function(r,e){
//                            var index = dom_Obj.split("_")[1];
//                            show_bar_1(dom_Obj, chart_title, '', '', foot_title, '', pic_data, null, ption_title,"yes");
					}
			},
			label : {
				background_color:null,
				sign:false,//设置禁用label的小图标
				padding:'0 4',
				border:{
					enable:false,
					color:'#666666'
				},
				fontsize:11,
				fontweight:600,
				color : '#4572a7'
			},
			border : {
				width : 2,
				color : '#ffffff'
			}
		},
		shadow : true,
		shadow_blur : 6,
		shadow_color : '#aaaaaa',
		shadow_offsetx : 0,
		shadow_offsety : 0,
		background_color:'#fefefe',
		offsetx:-80,//设置向x轴负方向偏移位置60px
		offset_angle:-120,//逆时针偏移120度
		showpercent:false,
		decimalsnum:2,
		width : 990,
		height : heights,
		radius:120
	});
	//利用自定义组件构造右侧说明文本
	chart.plugin(new iChart.Custom({
			drawFn:function(){
				//计算位置
				var y = chart.get('originy'),
					w = chart.get('width');
				
				//在右侧的位置，渲染说明文字
				chart.target.textAlign('start')
				.textBaseline('middle')
				.textFont('600 16px Verdana');
				//_data.length=0
			}
		
	}));
	
	chart.draw();
	if(callback){		
	       callback();return null;
	 }
};