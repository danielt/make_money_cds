var resources_view=new Starcor.S67.MVC.View("resources_view");
resources_view.addEvent("news.$DataChange",function(data){
    if(data.list!=null&&data.request_data!=null){
        pageControl.init({
            "dataCount":data.total,
            "pageShowNum":data.request_data.pager.page_size,
            "pageIndex":data.request_data.pager.page_num,
            "container":$(".pagelist")
        },news_func.changePage);
    }
    news_func.create_list(data);
});
resources_view.addEvent("voice.$DataChange",function(data){
    if(data.list!=null&&data.request_data!=null){
        pageControl.init({
            "dataCount":data.total,
            "pageShowNum":data.request_data.pager.page_size,
            "pageIndex":data.request_data.pager.page_num,
            "container":$(".pagelist")
        },voice_func.changePage);
    }
    voice_func.create_list(data);
});
resources_view.addEvent("news.get_news_content_list",function(data){
    $("#edit_btn").removeClass('hidden');
    $("#save").addClass('hidden');
    news_func.create_massge_list(data);
    news_func.set_default_input();
});
resources_view.addEvent("news.edit",function(data){
    resources_view.call("news.get_news_list",{pager:{page_size:news_func.pageShowNum,page_num:news_func.pageIndex}});
});
resources_view.addEvent("news.del_news",function(data){
    resources_view.call("news.get_news_list",{pager:{page_size:news_func.pageShowNum,page_num:news_func.pageIndex}});
});
resources_view.addEvent("news.add",function(data){
    resources_view.call("news.get_news_list",{pager:{page_size:news_func.pageShowNum,page_num:news_func.pageIndex}});
});
resources_view.addEvent("voice.del_voice",function(data){
    resources_view.call("voice.get_voice_list",{pager:{page_size:voice_func.pageShowNum,page_num:voice_func.pageIndex}});
    // window.top.NBOSS.tips.
});
var news_func={};
news_func.pageShowNum=10;
news_func.pageIndex=1;
news_func.edi_id='';
news_func.current_position=0;
news_func.base_url='../../../..'
var voice_func={};
voice_func.pageShowNum=10;
voice_func.pageIndex=1;
voice_func.edit_id='';
NBOSS.Complete = function(){ 
    resources_view.call("news.get_news_list",{pager:{page_size:news_func.pageShowNum,page_num:1}});
    $(".btn_add").click(function(){
        $("#save").removeClass('hidden');
        $("#edit_btn").addClass('hidden');
      	news_func.reset_add();
    });
    $('.btn_delete').click(function(){
        news_func.delete_more();
    });
    $("#words").click(function(){
        resources_view.call("news.get_news_list",{pager:{page_size:news_func.pageShowNum,page_num:1}});
    });
    $(".appmsg_add").click(function(){
    	news_func.add_appmsg();
    });
    $('#save').click(function(){
        news_func.add_submit();  
    });
    $('#edit_btn').click(function(){
        news_func.edit_submit();  
    });
    $('.msg_list_info').find('input').blur(function(){
        news_func.set_data();
    });
    $("#go_back").click(function(){
        $('#pic_note_list').show();
        $("#pic_note_edit").hide();
    });
    $(".appmsg").on('click','.js_del',function(){
        var pre_len=$(this).parents('.appmsg_item').prevAll().length;
        var $pre_obj=$(this).parents('.appmsg_item').prev();
        var $next_obj=$(this).parents('.appmsg_item').next();
        var del_len=$(".appmsg_content").children().length;
        $(this).parents('.appmsg_item').remove();
        if(pre_len+1==del_len){
            // $pre_obj.find('.appmsg_edit_mask .js_edit').click();
            news_func.set_other_input($pre_obj.find('.appmsg_edit_mask .js_edit')[0]);
        }
        else{
            // $next_obj.find('.appmsg_edit_mask .js_edit').click();
            news_func.set_other_input($next_obj.find('.appmsg_edit_mask .js_edit')[0]);
        }   
    });
    $("#msg_list_title").keyup(function(){
        var msg_title=$("#msg_list_title").val();
        $('.js_appmsg_item').eq(news_func.current_position).find(".appmsg_title>a").html(msg_title);
    });
    $("#appmsgItem0").find(".appmsg_edit_mask>a").click(function(){
        if(!inputlistener.listen($('.msg_list_info'))){
            return
        }
        news_func.set_default_input();
    })
    $("#voices").click(function(){
        resources_view.call("voice.get_voice_list",{pager:{page_size:voice_func.pageShowNum,page_num:1}});
    });
    $('#voice_list').on('click','.edit_voice',function(){

    });
    $('#voice_list').on('blur','.frm_input',function(){
        voice_func.edit_submit(this);
        $(this).parents('.audio_item').removeClass('editing');
    });
    $('#voice_list').on('click','.enter_gray',function(){
        voice_func.edit_submit(this);
        $(this).parents('.audio_item').removeClass('editing');
    });
    $("#image_del").click(function(){
        $('#uploadify').val('');
        $('#uploadify').attr('relative_img','');
        news_func.set_data();
        // $('.js_appmsg_item').eq(news_func.current_position).find(".appmsg_edit_mask .js_edit").click();
        news_func.set_other_input($('.js_appmsg_item').eq(news_func.current_position).find(".appmsg_edit_mask .js_edit")[0]);
    });
    $("#select_url").click(function(){
        window.top.NBOSS.dialog.open('html/url_list/url_list.html',"查询URL",function(alert_obj){
            var menu_url=alert_obj.find('#movie_list').val();
            var $check_video=alert_obj.find("input[name='detail']:checked");
            var detail_url=$check_video.val(); 
            var img_url=$check_video.attr('imgurl'); 
            var relative_image_address=$check_video.attr('relative_image_address');
            var video_name=$check_video.attr('videoname'); 
            if(menu_url){
                $('#msg_url').val(menu_url);
                $('#msg_list_title').val('');
                $('#uploadify').val('');
                $('#uploadify').attr('relative_img','');
                news_func.set_data();
                news_func.msg_app_info($('.js_appmsg_item')[news_func.current_position].data);
                return true;
            }
            if(detail_url){
                $('#msg_url').val(detail_url);
                $('#msg_list_title').val(video_name);
                $('#uploadify').val(img_url);
                $('#uploadify').attr('relative_img',relative_image_address);
                news_func.set_data();
                news_func.msg_app_info($('.js_appmsg_item')[news_func.current_position].data);
                return true;
            }
            else{
                window.top.NBOSS.Alert('提示', '请先选择详情地址');
                return false;
            }
        });
    });
    
    $(function() {
      $("#uploadify").uploadify({
          'buttonClass'   : 'upload_gray',
          'fileTypeExts'   : '*.jpg;*.bmp;*.png;*.gif',     
          'swf'           : '../../public/js/uploadify.swf',
          'uploader'      : '../../../admin.php?m=weixin/news&a=upload',
          'onUploadSuccess' : function(fileObj,data,is) {
              var rdata=eval('('+data+')');
              if(rdata.code==1){
                    $('#' + fileObj.id).find('.data').html(' - 完成');
                    $('#uploadify').val(rdata.data.image_address);
                    $('#uploadify').attr('relative_img',rdata.data.relative_image_address);
                    news_func.set_data();
                    var base_url=news_func.base_url;
                        if(rdata.data.image_address.indexOf('http')!=-1){
                            base_url='';
                        }
                        var terval=0;
                       news_func.timer=window.setInterval(function(){
                            $('#image').attr("src",base_url+rdata.data.image_address);    
                            terval++;
                            if(terval==10){
                                clearInterval(news_func.timer);
                            }               
                            $('#image').unbind().load(function(){
                                $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').attr('src',base_url+rdata.data.image_address)
                                $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').addClass('default');
                                $('.js_appmsg_item').eq(news_func.current_position).find('i.appmsg_thumb').removeClass('default');
                                $('.image_outer').removeClass('hidden');
                                window.top.NBOSS.reset(); 

                                clearInterval(news_func.timer);
                                $('#image').unbind();
                            })
                        },1000);   
              }
              else{
                    $('#' + fileObj.id).find('.data').html('<i class="frm_msg upload_fail">'+rdata.reason+'</i>');
              }
          }
        });
    });
    $(function() {
          $("#uploadify_voice").uploadify({
              fileSizeLimit : '256KB',
              fileTypeExts  : '*.mp3',
              height        : 30,
              swf           : '../../public/js/uploadify.swf',
              uploader      : '../../../admin.php?m=weixin/voice&a=upload',
              width         : 60,
              'onUploadSuccess' : function(fileObj,data,response) {
                var rdata=eval('('+data+')');
                if(rdata.code==1){
                    $('#' + fileObj.id).find('.data').html(' - 完成');
                    resources_view.call("voice.get_voice_list",{pager:{page_size:voice_func.pageShowNum,page_num:1}});
                }
                else{
                    $('#' + fileObj.id).find('.data').html('<i class="frm_msg upload_fail">'+rdata.reason+'</i>');
                }
              },
              'onUploadError' : function(fileObj, errorCode, errorMsg, errorString) {
                   $('#' + fileObj.id).find('.data').html('<i class="frm_msg upload_fail"> - 上传异常</i>');
              }, 
          });
        });
    ready_obj.audio_play();
    ready_obj.nav_change();
    showNavigation();
    tablebtnlisten(".tool_btns");
	window.top.NBOSS.reset();		
};
news_func.set_default_input=function(){
    $('.must_input').next().removeClass("fail_show");
    news_func.current_position=0;
    $(".msg_list_info").css("margin-top","0px");
    news_func.msg_app_info($("#appmsgItem0")[0].data);
    window.top.NBOSS.reset(); 
}
news_func.set_other_input=function(obj){
    $('.must_input').next().removeClass("fail_show");
    var current_position=$(obj).parents('.js_appmsg_item').prevAll().length
    if(current_position==0){
        $(".msg_list_info").css("margin-top","0px");
    }
    else{
        $(".msg_list_info").css("margin-top",''+(100*(current_position-1)+185)+'px');
    }
    news_func.current_position=current_position;
    news_func.msg_app_info($(".js_appmsg_item")[current_position].data);
    window.top.NBOSS.reset();
}
news_func.msg_app_info=function(data_param){
    if(data_param){
        $('#msg_url').val(data_param.url);
        var image_url=data_param.image_address;
        
        if(image_url){
            var base_url=news_func.base_url;
            if(image_url.indexOf('http')!=-1){
                base_url='';
            }
            $('#uploadify').val(image_url);
            $('#uploadify').attr('relative_img',data_param.relative_image_address);
            $('#image').attr("src",base_url+image_url);
            $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').attr('src',base_url+image_url)
            $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').addClass('default');
            $('.js_appmsg_item').eq(news_func.current_position).find('i.appmsg_thumb').removeClass('default');
        }
        else{
            $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').attr('src','')
            $('.js_appmsg_item').eq(news_func.current_position).find('i.appmsg_thumb').addClass('default');
            $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').removeClass('default');
            $('.msg_list_info').find("img").attr('src','');
            // $('.msg_list_info').find('input').val('');
            $(".image_outer").addClass('hidden');
        }
        $('#msg_list_title').val(data_param.title);
        $('.js_appmsg_item').eq(news_func.current_position).find('.appmsg_title a').text(data_param.title);
        $('.image_outer').removeClass('hidden');
        $('#msg_list_author').val(data_param.author);
    }
    else{
        $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').attr('src','')
        $('.js_appmsg_item').eq(news_func.current_position).find('i.appmsg_thumb').addClass('default');
        $('.js_appmsg_item').eq(news_func.current_position).find('img.appmsg_thumb').removeClass('default');
        $('.js_appmsg_item').eq(news_func.current_position).find('.appmsg_title a').text('标题');
        $('.msg_list_info').find("img").attr('src','');
        $('.msg_list_info').find('input').val('');
        $(".image_outer").addClass('hidden');
    }
}
news_func.init_add=function(){
    $('#pic_note_list').hide();
    $("#pic_note_edit").show();
    $("#pic_note_edit").find('.appmsg_item').remove();
    $("#pic_note_edit").find('.js_appmsg_item')[0].data=null;
    $("#global_name").val('')
}
news_func.reset_add=function(id){
    news_func.init_add();
    news_func.set_default_input();
    window.top.NBOSS.reset();
}
news_func.add_appmsg=function(){
    var appcontant='<img src="" class="js_appmsg_thumb appmsg_thumb"><i class="appmsg_thumb default lang">缩略图</i>';
    appcontant+='<h4 class="appmsg_title"><a target="_blank" href="javascript:void(0);" onclick="return false;"class="lang">标题</a></h4>';
    appcontant+='<div class="appmsg_edit_mask"><a href="javascript:void(0);" class="icon14_common edit_gray js_edit">编辑</a><a href="javascript:void(0);" class="icon14_common del_gray js_del">删除</a></div>'
    var i=$(".appmsg_content").children().length;
    var $appmsg_content=$(".appmsg_content");
        if(i<10){
          $("<div>",{
            "class":"appmsg_item js_appmsg_item clearfloat",
          }).html(appcontant).appendTo($appmsg_content).find(".appmsg_edit_mask .js_edit").click(function(){
                if(!inputlistener.listen($('.msg_list_info'))){
                    return
                }
                news_func.set_other_input(this);
          });
          window.top.NBOSS.reset();
      }
      else{
          window.top.NBOSS.Alert('提示','最多只能添加十个图文信息');
      }  
}
news_func.set_data=function(){
    var msg_url=$('#msg_url').val();
    var image_url=$('#uploadify').val();
    var re_image_url=$('#uploadify').attr('relative_img');
    var msg_list_title=$('#msg_list_title').val();
    var msg_list_author=$('#msg_list_author').val();
    var data_param={
        'url':msg_url,
        'title':msg_list_title,
        'author':msg_list_author,
        'image_address':image_url,
        'relative_image_address':re_image_url
    }
    $('.js_appmsg_item')[news_func.current_position].data=data_param;
}
news_func.add_submit=function(){
    var all_data=[];
    var global_name=$("#global_name").val();
    $('.js_appmsg_item').each(function(){
      all_data.push(this.data)
    })
    var param={
      'global_name':global_name,
      'data':all_data
    }
    if(!inputlistener.listen($('.msg_list_info'))){
        return
    }
    if(!inputlistener.listen($('#globle_outer'))){
        return
    }
    resources_view.call("news.add",param);
}
news_func.create_list=function(data){
    $('#pic_note_edit').hide();
    $("#pic_note_list").show();
    $("#table_list tbody").html('');
    var model_list_html='';
    if(data.list!=null){
        for (var i=0;i<data.list.length;i++){
            var html_str=news_func.create_list_tr(data.list[i],i);
            model_list_html+=html_str;
        }
    }
    $("#table_list tbody").html(model_list_html);
    window.top.NBOSS.reset();
};

news_func.create_list_tr=function(item,i){
    var tr_class='';
    if(i%2!=0){
      tr_class='tr_odd'
    }
    if(i<9){
      i='0'+(i+1);
    }
    else{
        i++;
    }
    var html='<tr class="'+tr_class+'"><td width="10"><input type="checkbox" value="'+item.id+'" /></td>';
    html+='<td>'+i+'</div></td>';
    html+='<td>'+item.content_total+'</div></td>';
    html+='<td>'+item.name+'</div></td>';
    html+='<td>'+item.create_time+'</div></td>';
    html+='<td><div class="btn-group">';
    html+='<a href="javascript:void(0)" class="action_btn lang">操作'
    html+='</a>';
    html+='<ul class="dropdown-menu pull-right" role="menu">';
    html+='<li><a href="javascript:news_func.edit(\''+item.id+'\',\''+item.name+'\');" class="lang">编辑</a></li>';
    html+='<li><a href="javascript:news_func.del_massege(\''+item.id+'\');" class="lang">删除</a></li>';
    html+='</ul>';
    html+='</div></td>';
    return html;
};
news_func.edit=function(id,name){
    news_func.edi_id=id;
    $('#global_name').val(name);
    resources_view.call("news.get_news_content_list",{message_id:id});
}
news_func.create_massge_list=function(data){
    $('#pic_note_list').hide();
    $("#pic_note_edit").show();
    $("#pic_note_edit").find('.appmsg_item').remove();
    if(data.list){
        var item=data.list;
        news_func.create_massge_app_first(item[0]);
        for(var i=1;i<item.length;i++){
            news_func.create_massge_app(item[i],i);
        }
    }
    window.top.NBOSS.reset();
}
news_func.create_massge_app=function(item,i){
    var base_url=news_func.base_url;
    if(item.image_address.indexOf('http')!=-1){
        base_url='';
    }
    if(item.image_address){
        var appcontant='<img src="'+base_url+item.image_address+'" class="js_appmsg_thumb appmsg_thumb default">';
        appcontant+='<i class="appmsg_thumb lang">缩略图</i>';
    }
    else{
        var appcontant='<img src="" class="js_appmsg_thumb appmsg_thumb">';
        appcontant+='<i class="appmsg_thumb default lang">缩略图</i>';
    }
    
    appcontant+='<h4 class="appmsg_title"><a target="_blank" href="javascript:void(0);" onclick="return false;">'+item.title+'</a></h4>';
    appcontant+='<div class="appmsg_edit_mask">';
    appcontant+='<a href="javascript:void(0);" class="icon14_common edit_gray js_edit">编辑</a>';
    appcontant+='<a href="javascript:void(0);" class="icon14_common del_gray js_del">删除</a>'
    appcontant+='</div>';
    var $appmsg_content=$(".appmsg_content");
    $("<div>",{
          "class":"appmsg_item js_appmsg_item clearfloat",
      }).html(appcontant).appendTo($appmsg_content).find(".appmsg_edit_mask>a").click(function(){
            if(!inputlistener.listen($('.msg_list_info'))){
                return
            }
            news_func.set_other_input(this);
      });
      news_func.init_data(item,i);
      window.top.NBOSS.reset();
}
news_func.create_massge_app_first=function(item){
    var $appmsgItem0=$('#appmsgItem0');
    $appmsgItem0.find('.appmsg_title a').text(item.title);
    if(item.image_address==''){
        $appmsgItem0.find('img.appmsg_thumb').removeClass('default');
        $appmsgItem0.find('i.appmsg_thumb').addClass('default');
    }
    else{
        var base_url=news_func.base_url;
        if(item.image_address.indexOf('http')!=-1){
            base_url='';
        }
        $appmsgItem0.find('i.appmsg_thumb').removeClass('default');
        $appmsgItem0.find('img.appmsg_thumb').addClass('default');
        $appmsgItem0.find('img')[0].src=base_url+item.image_address;
    }
    news_func.init_data(item,0);
    window.top.NBOSS.reset();
}
news_func.init_data=function(item,i){
  var data_param={
          'url':item.url,
          'title':item.title,
          'author':item.author,
          'image_address':item.image_address,
          'relative_image_address':item.relative_image_address
      }
      $(".js_appmsg_item")[i].data=data_param;
}
news_func.edit_submit=function(){
    var all_data=[];
    var global_name=$("#global_name").val();
    $('.js_appmsg_item').each(function(){
      all_data.push(this.data);
    })
    var param={
      'id':news_func.edi_id,
      'global_name':global_name,
      'data':all_data
    }
    if(!inputlistener.listen($('.msg_list_info'))){
        return
    }
    if(!inputlistener.listen($('#globle_outer'))){
        return
    }
    resources_view.call("news.edit",param);
}
news_func.del_massege=function(id){
    var ids=[];
    ids[0]=id
    if(ids.length==0){
        return ;
    }
    var tr_len=$("#table_list tbody input").length;
    if(ids.length==tr_len){
        news_func.pageIndex--;
    }
    resources_view.call('news.del_news',{id:ids});
}
news_func.delete_more=function(){
    var ids=[];
    var $checks=$("#table_list tbody input[type=checkbox]");
    for (var i=0;i<$checks.length;i++){
      if ($checks.eq(i).attr('checked')){
        ids.push($checks.eq(i).val());
      }
    }
    if(ids.length==0){
        return ;
    }
    var tr_len=$("#table_list tbody input").length;
    if(ids.length==tr_len){
        news_func.pageIndex--;
    }
  resources_view.call('news.del_news',{id:ids});
}
news_func.changePage = function(data){
    news_func.pageShowNum = data.pageShowNum;
    news_func.pageIndex=data.currentIndex;
    var params={
      'pager':{
        'page_size':news_func.pageShowNum,
        'page_num':data.currentIndex
      }
    };
    resources_view.call("news.get_news_list", params);
};
voice_func.create_list=function(data){
    $("#voice_list").html('');
    var model_list_html='';
    if(data.list!=null){
      for (var i=0;i<data.list.length;i++){
        var html_str=voice_func.create_list_li(data.list[i]);
        model_list_html+=html_str;
      }
    }
    $("#voice_list").html(model_list_html);
    window.top.NBOSS.reset();
}
voice_func.create_list_li=function(item){
    var html='<li class="audio_item dialog_audio_list js_audio_list line">';
    html+='<div class="pics_infor clearfloat"><div class="pics_titile pull_left">';
    html+='<h3 class="pics_titile_text" data-id="'+item.id+'" >'+item.name+'</h3>';
    html+='<span class="frm_input_box append"><input type="text" class="frm_input" value="'+item.name+'"/>';
    html+='<a class="js_rename frm_input_append icon16_common enter_gray" href="javascript:void(0);">确定</a></span></div>'
    html+='<div class="pics_size pull-left">'+item.media_size+'kb</div><div class="pics_opre pull-left hidden">';
    html+='<a href="javascript:void(0)" class="edit_voice pull-left" onclick="voice_func.edit_voice(this,\''+item.id+'\')"><i class="icon18_common edit_blue"></i></a>';
    html+='<a href="javascript:voice_func.del_voice(\''+item.id+'\')" class="del_voice pull-left"><i class="icon18_common del_blue"></i></a>';
    html+='</div></div>';
    html+='<div class="audioBox voice-audio" src_value="'+item.media_address+'" time_len="'+item.media_leng+'">';
    html+='<div class="audioContent "><span class="audioTxt lang">点击播放</span><span class="iconAudio"></span>';
    html+='<b>'+item.media_leng+'"</b></div></div>'
    html+='</li>'
    return html;
}
voice_func.edit_voice=function(obj,id){
    voice_func.edit_id=id;
    var input_obj=$(obj).parents('.audio_item').find('input')[0];
    $(obj).parents('.audio_item').addClass('editing');
    input_obj.focus();
}
voice_func.edit_submit=function(obj){
    var name=$(obj).val();
    var param={
      'name':name,
      'id':voice_func.edit_id
    }
    resources_view.call("voice.edit_voice",param);
}
voice_func.del_voice=function(id){
    var ids=[];
    ids[0]=id;
    var param={
      'id':ids
    }
    var tr_len=$("#voice_list .audio_item").length;
    if(ids.length==tr_len){
        voice_func.pageIndex--;
    }
    resources_view.call("voice.del_voice",param);
}
voice_func.changePage = function(data){
    voice_func.pageShowNum = data.pageShowNum;
    voice_func.pageIndex=data.currentIndex;
    var params={
      'pager':{
        'page_size':voice_func.pageShowNum,
        'page_num':data.currentIndex
      }
    };
    resources_view.call("voice.get_voice_list", params);
};