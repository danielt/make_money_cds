var movie_pointView=new Starcor.S67.MVC.View("PointView");
movie_pointView.addEvent("movie_point.get_list",function(data){
  point.create_list(data);
});

movie_pointView.addEvent("movie_point.add",function(data){
   
	$("#pic_note_edit").addClass('hidden');
	$("#pic_note_list").removeClass('hidden');
	var params=point.get_urlmsg();
	movie_pointView.call("movie_point.get_list",params)
});
movie_pointView.addEvent("movie_point.del_list",function(data){
 	var param=point.get_urlmsg();
	movie_pointView.call("movie_point.get_list",param)
});
movie_pointView.addEvent("movie_point.del_image",function(data){
	$('#uploadify').val('');
});
movie_pointView.addEvent("movie_point.edit_list",function(data){
	$("#save").removeClass('hidden');
    $("#edit_btn").addClass('hidden')
	$("#pic_note_edit").addClass('hidden');
	$("#pic_note_list").removeClass('hidden')
	var param=point.get_urlmsg();
	movie_pointView.call("movie_point.get_list",param)
});
var point={};
point.base_url='../../../../'
point.metadata={};
point.page_size=0;
NBOSS.Complete = function(){
	url=window.location.href;
	movie_id=urlControlObj.getItem('id',url);
	movie_index=urlControlObj.getItem('index',url);
	movie_title=urlControlObj.getItem('video_name',url);
	$("#head_title").text(decodeURIComponent(movie_title));
		$("#point_start,#point_end").blur(function(){
			if(this.value!=this.value.replace(/[^0-9:：]+/,'')){
					alert("请输入数字")	
			}
			this.value=this.value.replace(/[^0-9:：]+/,'');
	
		})
 $("#point_end").blur(function(){
 	var end_times=parseFloat($("#point_end").val());
 	var start_times=parseFloat($("#point_start").val());
 	if(end_times<start_times){
 		alert("结束时间应大于开始时间");
 			$("#point_end").val('');
 	}
 
 })
$(function() {
      $("#uploadify").uploadify({
          'buttonClass'   : 'upload_gray',
          'fileTypeExts'   : '*.jpg;*.bmp;*.png;*.gif',     
          'swf'           : '../../public/js/uploadify.swf',
          'uploader'      : '../../../admin.php?m=seekpoint/seekpoint&a=upload_seekpoint&video_id='+movie_id,
          'onUploadSuccess' : function(file,data,is) {
              var rdata=eval('('+data+')');
              if(rdata.code==1){
                  $('#' + file.id).find('.data').html(' - 完成');
                  $('#uploadify').val(rdata.data.image_url);
               setTimeout(function(){
                  $('#image').attr("src",rdata.data.ftp_url);
                  $('.js_appmsg_item').find('img.appmsg_thumb').attr('src',rdata.data.ftp_url)
              },1000)
                  $('.js_appmsg_item').find('img.appmsg_thumb').attr('src',"http://1img.imgo.tv/preview/internettv/prev/starcor/seekpoint/db6caa47f8c43a5d87709e4fdd174d9b/53733cf3031bb.png");
                  $('.js_appmsg_item').find('img.appmsg_thumb').addClass('default');
                  $('.js_appmsg_item').find('i.appmsg_thumb').removeClass('default');
                  $('.image_outer').removeClass('hidden');
                  window.top.NBOSS.reset('frameBox_sub'); 
              }
              else{
                  $('#' + file.id).find('.data').html('<i class="frm_msg upload_fail">'+rdata.reason+'</i>');
              }
          }
       });
});
$(".btn_delete").click(function(){
	    var arr=[];
		var del_lines=$("input[type='checkbox']:checked")
		var l=$("#pic_note_list").find("input[type='checkbox']:checked").length
		for(var i=0;i<l;i++){
            arr.push(del_lines.eq(i).val());
        }
        var id=arr.join(",")
        if(id!=''){
			var params={
				'id':id,
				'video_id':'',
				'video_index':''
			}
			 movie_pointView.call("movie_point.del_list",params);
			 }
			  window.top.NBOSS.reset('frameBox_sub'); 
	})

	var params=point.get_urlmsg();
	movie_pointView.call("movie_point.get_list",params)
	$(".btn_add").click(function(){
		$("#save").removeClass('hidden');
    	$("#edit_btn").addClass('hidden')
		$("#pic_note_list").addClass('hidden');
		$("#pic_note_edit").removeClass('hidden');
		 window.top.NBOSS.reset('frameBox_sub'); 
	});
	$("#point_name").keyup(function(){
		var name=$("#point_name").val();
		$(".appmsg_title").find("a").html(name);
		});
	$("#go_back").click(function(){
		$("#image_del").addClass('hidden');
		$("#pic_note_edit").addClass('hidden');
		$("#pic_note_list").removeClass('hidden');
		$('#uploadify').val('');
	    $(".inner").find("input").val('');
	    $('.js_appmsg_item').find('img.appmsg_thumb').attr('src','');
	    $('.js_appmsg_item').find('img.appmsg_thumb').removeAttr("src");
		$('#image').removeAttr("src");
		$('#image').attr('src','');
		$(".appmsg_title>a").html("打点信息");
		 window.top.NBOSS.reset('frameBox_sub'); 
	})

	$("#save").click(function(){
		var params=point.set_data();
		movie_pointView.call("movie_point.add",params)	;
		window.top.NBOSS.reset('frameBox_sub'); 
	});
	$('.btn_back').click(function(){
		history.go(-1);
	});
	tablebtnlisten(".tool_btns");
	showNavigation();
}
point.get_urlmsg=function(){
	var url=window.location.href;
	var movie_id=urlControlObj.getItem('id',url);
	var movie_index=urlControlObj.getItem('index',url);
	var data_param={
    	'video_id':movie_id,
    	'video_index':movie_index,
    }
    return data_param;
}
point.set_data=function(){
	var url=window.location.href;
	var movie_id=urlControlObj.getItem('id',url);
	var movie_index=urlControlObj.getItem('index',url);
    var fragment_type=$('#point_fragment_type').val();
    var name=$('#point_name').val();
    var begin_time=$('#point_start').val();
    var end_time=$('#point_end').val();
    var image=$('#uploadify').val();
    var data_param={
    	'video_id':movie_id,
    	'video_index':movie_index,
        'fragment_type':fragment_type,
        'name':name,
        'begin_time':begin_time,
        'end_time':end_time,
        'image':image
    }
    return data_param;
}
 point.create_list=function(data){
 	$("#image_del").addClass('hidden');
    $('#uploadify').val('');
    $(".inner").find("input").val('');
    $("#table_list tbody").html('');
    $('.js_appmsg_item').find('img.appmsg_thumb').attr('src','');
    $('.js_appmsg_item').find('img.appmsg_thumb').removeAttr("src");
	$('#image').removeAttr("src");
	$('#image').attr('src','');
	$(".appmsg_title>a").html("打点信息");
	var len=Math.max(data.list.length,10);
	point.page_size=data.list.length;
    var model_list_html='';
    if(data.list!=null){
        for (var i=0;i<len;i++){
        	if(point.page_size<=i){
        		var html_str=point.create_list_tr(data.list[point.page_size],i);
        	}
            else{
            	var html_str=point.create_list_tr(data.list[i],i);
            }
            model_list_html+=html_str;
        }
    }
    $("#table_list tbody").html(model_list_html);
    window.top.NBOSS.reset('frameBox_sub');
 }
point.create_list_tr=function(item,i){
	var tr_class='';
    if(i%2!=0){
      tr_class='tr_odd'
    }
	if(point.page_size<=i){
		html='<tr class="'+tr_class+'"><td width="10"></td>';
		html+='<td></td>';
		html+='<td></td>';
		html+='<td></td>';
		html+='<td></td>';
		html+='<td></td>';
		html+='<td></td>';
		html+='<td></td>';
		html+='<td></td></tr>';
		return html;
	}
	point.metadata[i]=item;
	// var josnsty=JSON.stringify(item);
	 
    // if(i<9){
    //   i='0'+(i+1);
    // }
	html='<tr class="'+tr_class+'"><td width="10"><input type="checkbox" value="'+item.id+'"></td>';
	html+='<td>'+item.video_id+'</td>';
	html+='<td>'+(Number(item.video_index)+1)+'</td>';
	if(item.fragment_type==1){
		html+='<td>'+'片头'+'</td>';
	}
	else if(item.fragment_type==0){
		html+='<td>'+'片中'+'</td>';
	}
	else if(item.fragment_type==2){
		html+='<td>'+'片尾'+'</td>';
	}
    else if(item.fragment_type==3){
        html+='<td>'+'互动'+'</td>';
    }
	html+='<td>'+item.name+'</td>';
  if(item.image){	html+='<td><a href="javascript:pic_view(\''+item.image+'\')">查看</a></td>';}else{
  	html+='<td>无图片</td>';
  }

	html+='<td>'+item.begin+'</td>';
	html+='<td >'+item.end+'</td>';
	html+='<td><div class="btn-group">';
	html+='<a href="javascript:void(0)" class="action_btn lang">操作</a>';
	html+='<ul class="dropdown-menu pull-right" role="menu">';
	html+='<li onClick=point.edit('+i+')><a href="javascript:void(0)"; class="lang">编辑</a></li>';
	html+='<li><a href="javascript:point.del(\''+item.id+'\',\''+item.video_id+'\',\''+item.video_index+'\')" class="lang">删除</a></li></ul></div></td></tr>';
	return html;
}
pic_view=function(url){
	 window.top.NBOSS.dialog.open('html/point/pic_view.html?pic_url='+url,"图片查看",function(){
	 	return true;
	 })
}
point.edit=function(i){ 
	params=point.metadata[i];
	$("#pic_note_list").addClass('hidden');
	$("#pic_note_edit").removeClass('hidden');
	$('#point_fragment_type').val(params.fragment_type)
	$('#point_name').val(params.name);
   	$('#point_start').val(params.begin);
    $('#point_end').val(params.end);
    $("#save").addClass('hidden');
    $(".appmsg_title>a").html(params.name);
    $("#edit_btn").removeClass('hidden')
    $("#image_del").removeClass('hidden')
    if(params.image!=''){
    	$(".image_outer").removeClass("hidden");
    	$('#image').attr("src",params.image);
    	$('.js_appmsg_item').find('img.appmsg_thumb').attr('src',params.image)
        $('.js_appmsg_item').find('img.appmsg_thumb').addClass('default');
        $('.js_appmsg_item').find('i.appmsg_thumb').removeClass('default');
    }
    $("#edit_btn").unbind().click(function(){
    	var data=point.set_data();
    	   data.id=params.id
    	movie_pointView.call("movie_point.edit_list",data);

 	});
 		 $("#image_del").unbind().click(function(){
		 $('.js_appmsg_item').find('img.appmsg_thumb').removeAttr("src");
		 $('#image').removeAttr("src");
		 movie_pointView.call("movie_point.del_image",{id:params.id})
	})
 		 window.top.NBOSS.reset('frameBox_sub');
}	
point.del=function(id,video_id,video_index){
	var params={
		'id':id,
		'video_id':video_id,
		'video_index':video_index
	}
    movie_pointView.call("movie_point.del_list",params);
}
