var urlControlObj = {
    url_obj : null,
    
};
/**
 * 获取url中的指定参数的值， 这里需要将需要设定的键值和目标url地址传入
 * @param item为obj。以json对象的形式传入，如果原url中存在这个键值，则替换，如果不存在则在url上面进行追加。 url: 目标url地址。如果url传入则将item放入url中，如果没有则使用默认的url进行处理
 * @returns 返回修改后的url地址。
 * **/
urlControlObj.getItem = function(item, url){
    if( this.url_obj ){
        return this.url_obj.params[item];
    }else{
        var tempObj = this.urlToObj(url);
        
        return tempObj.params[item];
    }
};

/**
 * 设置url中的指定参数的值， 这里需要将需要设定的键值和目标url地址传入
 * @param item为obj。以json对象的形式传入，如果原url中存在这个键值，则替换，如果不存在则在url上面进行追加。 url: 目标url地址。如果url传入则将item放入url中，如果没有则使用默认的url进行处理
 * @returns 返回修改后的url地址。
 * **/
urlControlObj.setItem = function(obj, url){
    if( url ){
        var tempObj = this.urlToObj(url);
        
        for(var par in obj){
            tempObj.params[par] = obj[par];
        }
        
        return this.urlObjToStr(tempObj);
    }
    
    if( this.old_url ){
        
        for(var par in obj){
            this.url_obj[par] = obj[par];
        }
        
        return this.urlObjToStr(this.url_obj);
    }
};

/**
 * 替换url中的参数，该方面是将目标url上面的一些参数替换为当前页面打开时传递的参数。主要用于模板缓存时页面生成的url地址固定，使参数不正确的问题。
 * @param url: 目标url地址。 arr需要替换的参数. arr 需要替换的参数数组。传入参数为需要替换的键
 * @returns 返回替换后的目标url。
 * **/
urlControlObj.replaceItem = function(url, arr){
    var tempUrl = "";
    
    if( this.url_obj ){
        this.urlToObj(url);
    }
    
    var __urlObj = this.url_obj;
    var __urlParamObj = __urlObj.params;
    
    var __locaUrlObj = this.urlToObj(window.location.href);
    var __locaUrlParamObj = __locaUrlObj.params;
    
    if(__urlParamObj && __urlParamObj != {}){
        tempUrl = __urlObj.baseUrl+"?";
    }else{
        tempUrl = url;
        return tempUrl;
    }
    
    
    if(arr && typeof(arr) == "object" && arr instanceof Array){
        var len = arr.length;
        for(var i=0; i<len; i++){
            if(__urlParamObj[arr[i]] && __locaUrlParamObj[arr[i]]){
                __urlParamObj[arr[i]] = __locaUrlParamObj[arr[i]];
            }
        }
    }
    
    return urlControlObj.urlObjToStr(__urlParamObj);
};

/**
 * 将固定格式的url对象转换成字符串
 *@param obj url对象包含2个见，1、baseUrl 2、params
 * **/
urlControlObj.urlObjToStr = function(obj){
    var tempUrl = "";
    
    if( !obj ){
        obj = this.url_obj;
    }
    
    if( !obj.baseUrl ) return "";
    
    if( obj.params == {} ) {
        return obj.baseUrl;
    }
    
    tempUrl = obj.baseUrl + "?";
    
    for(var item in obj){
        tempUrl += (item +"="+obj.params[item])+"&";
    }
    
    tempUrl = tempUrl.substring(0, tempUrl.length-1);
    
    return tempUrl;
};

/**
 * url字符串转换为json对象
 * @param url需要转换的url地址
 * @returns 返回一个obj。obj包含2个键值 1、baseUrl 2、params
 * **/
urlControlObj.urlToObj = function(url){
    var obj = {
        "baseUrl":"",
        "params" : {}
    };
    
    if(typeof(url) != "string"){
        return ;
    }
    
    if( url.indexOf("?") != -1 ){
        var params = url.split('?');
        
        obj.baseUrl = params[0];
        
        if( params[1] ){
            var pars=params[1].split('&');  
            for (var num in pars){
                if (typeof pars[num] =='string'){
                    var par=pars[num].split('=');
                    obj.params[par[0]]=par[1];    
                }
            }
        }else{
            obj.params = {};
        }
    }else{
        obj.baseUrl = url;
        obj.params = {};
    }
    
    this.url_obj = obj;
    return obj;
};